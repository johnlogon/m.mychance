<?php
	require_once('system/start.php');
	$siteurl = ossn_site_url();
	if(isset($_POST['searchString'])){
		$searchString = $_POST['searchString'];
		$friends = ossn_loggedin_user()->getFriends();
		$data = array();
		foreach($friends as $friend){
			$name = $friend->first_name." ". $friend->last_name;
			if (stripos($name, $searchString) !== false) {
				$data[] = $friend;
			}
		}
		echo json_encode($data);
	}
?>