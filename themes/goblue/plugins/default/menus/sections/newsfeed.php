<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$menus = $params['menu'];
?>
<div class="sidebar-menu-nav">
          <div class="sidebar-menu">
                 <ul id="menu-content" class="menu-content collapse out">
<?php                        
foreach ($menus as $key => $menu) {
    $section = ossn_print($key);
	$expend = '';
	$icon = "fa-angle-right";
	if($key == 'links'){
		$expend = 'in';
		$icon = "fa-newspaper-o";
	}
	if($key == 'groups'){
		$icon = "fa-users";
	}
	$hash = md5($key);
	
    ?>
     <li data-toggle="collapse" data-target="#<?php echo $hash;?>" class="collapsed active">
        	<a href="#"><i class="fa <?php echo $icon;?> fa-lg"></i><?php echo $section;?><span class="arrow"></span></a>
     </li>
    <ul class="sub-menu collapse <?php echo $expend;?>" id="<?php echo $hash;?>"> 
    <?php
    foreach ($menu as $text => $data) {
        $menu = str_replace(':', '-', $text);
        $icon = $data[1];
        if (!is_array($data[2])) {
            $data[2] = array();
        }
        $args = ossn_args($data[2]);
		if($text!=="Messages")
        	echo "<li><a {$args} href='{$data[0]}'>{$text}</a></li>";
    }
	echo "</ul>";
}
?>

         </ul>
    </div>
</div>


<script>
	$(document).on('click', '.post-control-like', function(){
		var msg = 'You have new notification from Mychance';
		var id = $(this).attr('id');
		var postid ='';
		var test = ($('#'+id).attr('onclick'));
		if(test.substring(0,13)=="Ossn.PostLike"){
			msg = "Someone has liked your post";
			postid = test.substring(test.length-4, test.length-2);
			$.ajax({
				url: "http://139.59.6.174:70/html/push/pushnotification.php?postId="+postid+"&message="+msg,
				type: "GET",
				success: function(response){
					console.log("success: "+response);			
				},
				error: function(xhr, error, errorThrown){
					console.log(xhr.responseText);
				}
			});
		}
	});
	$(document).on('keydown', '.comment-box', function(event){
		if(event.keyCode==13){
			var id = $(this).attr('id');
			postid = id.substring(12, id.length);
			var msg = 'Someone has commented on your post';
			if(postid!=''){
				$.ajax({
					url: "http://139.59.6.174:70/html/push/pushnotification.php?postId="+postid+"&message="+msg,
					type: "GET",
					success: function(response){
						console.log("success: "+response);			
					},
					error: function(xhr, error, errorThrown){
						console.log(xhr.responseText);
					}
				});
			}
		}
	});
	$(document).on('click', '.ossn-like-comment', function(){
		var ownerid = $(this).attr('data-owner-id');
		console.log(ownerid);
		var data_type = $(this).attr('data-type');
		var msg = 'Someone has liked your comment';
		if(ownerid!='' && data_type=='Like'){
			$.ajax({
				url: "http://139.59.6.174:70/html/push/pushnotification_Comment_Like.php?ownerId="+ownerid+"&message="+msg,
				type: "GET",
				success: function(response){
					console.log("success: "+response);			
				},
				error: function(xhr, error, errorThrown){
					console.log(xhr.responseText);
				}
			});
		}
	});
	$(document).on('click', '#push-identifier', function(event){
		event.preventDefault();
		$(this).text("Sending..");
		$(this).css('text-decoration', 'none');
		var href = $(this).attr('href');
		var ownerid = $(this).attr('data-push-id');
		var msg = 'You have a new friend request';
		if(ownerid!=''){
			$.ajax({
				url: "http://139.59.6.174:70/html/push/pushnotification_Comment_Like.php?ownerId="+ownerid+"&message="+msg,
				type: "GET",
				success: function(response){
					//console.log("success: "+response);			
				},
				error: function(xhr, error, errorThrown){
					//console.log(xhr.responseText);
				},
				complete: function(){
					location.href = href;
				}
			});
		}
		else
			location.href = href;
	});
</script>