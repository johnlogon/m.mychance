<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$params['controls'] = (isset($params['controls'])) ? $params['controls'] : '';
?>
	<?php 
		$myguid = ossn_loggedin_user()->guid;
		$yourguid = $_SESSION['yourguid'];
		if($params['title']=='Friends' && $_SESSION['yourguid']!=$myguid ){
		$myfriends = ossn_loggedin_user()->getFriends();
		$yourfriends = ossn_user_by_guid($yourguid)->getFriends();
	
	?>
        <div class="col-md-11">
		<div class="ossn-layout-module">
			<div class="module-title">
            
				<div class="title"><?php echo "Mutual Friends";?></div>
				<div class="controls">
				</div>
			</div>
			<div class="module-contents">
            <?php 
				foreach ($myfriends as $mutual) {
					if (in_array($mutual, $yourfriends)) {
			?>
							<div class="row ossn-users-list-item">
                               <div class="col-md-2 col-sm-2 hidden-xs">
                                  <img src="<?php echo ossn_site_url("avatar/{$mutual->username}/large"); ?>" width="100" height="100">
                               </div>
                               <div class="col-md-10 col-sm-10 col-xs-12">
                                  <div class="uinfo">
                                     <a class="userlink" href="<?php echo ossn_site_url(); ?>u/<?php echo $mutual->username ?>"><?php echo $mutual->first_name ." ". $mutual->last_name ?></a>        	   		
                                  </div>
                               </div>
                            </div>

			<?php
					}
				}
			 ?>
			</div>
		</div>
	</div>
		
  <?php }  
  ?>
	<div class="col-md-11">
		<div class="ossn-layout-module">
			<div class="module-title">
            
				<div class="title"><?php echo $params['title']; ?></div>
				<div class="controls">
					<?php echo $params['controls']; ?>
				</div>
			</div>
			<div class="module-contents">
				<?php echo $params['content']; ?>
			</div>
		</div>
	</div>