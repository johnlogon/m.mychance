<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$params['controls'] = (isset($params['controls'])) ? $params['controls'] : '';

?>
<?php
	$myguid = ossn_loggedin_user()->guid;
	$yourguid = $_SESSION['yourguid'];
	if($params['title']=='Friends' && $_SESSION['yourguid']!=$myguid ){
		$myfriends = ossn_loggedin_user()->getFriends();
		$yourfriends = ossn_user_by_guid($yourguid)->getFriends();
?>
        <div class="container">  
          <ul class="nav nav-tabs" style="width:92%">
            <li class="active">
            	<a data-toggle="tab" href="#home">
                	<div class="module-title">
                        <div class="title">All Friends</div>
                        <div class="controls">
                            <?php echo $params['controls']; ?>
                        </div>
                    </div>
                </a>
            </li>
            <li>
            	<a data-toggle="tab" href="#menu1">
                	<div class="module-title">
                        <div class="title">Mutual Friends</div>
                        <div class="controls"></div>
                    </div>
                </a>
            </li>    
          </ul>
        
          <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
            	<div class="col-md-11">
                     <div class="ossn-layout-module" style="max-height: 500px;overflow-y: scroll;">
                        <div class="module-contents" >
                            <?php echo $params['content']; ?>
                        </div> 
                    </div>
                </div>
            </div>
            <div id="menu1" class="tab-pane fade">
            	<div class="col-md-11">
                     <div class="ossn-layout-module" style="max-height: 500px;overflow-y: scroll;">
                        <div class="module-contents">
                           <?php 
						   		$flag = false;
								foreach ($myfriends as $mutual) {
									if (in_array($mutual, $yourfriends)) {
										$flag = true;
							?>
											<div class="row ossn-users-list-item">
											   <div class="col-md-2 col-sm-2 hidden-xs">
												  <img src="<?php echo ossn_site_url("avatar/{$mutual->username}/large"); ?>" width="100" height="100">
											   </div>
											   <div class="col-md-10 col-sm-10 col-xs-12">
												  <div class="uinfo">
													 <a class="userlink" href="<?php echo ossn_site_url(); ?>u/<?php echo $mutual->username ?>"><?php echo $mutual->first_name ." ". $mutual->last_name ?></a>        	   		
												  </div>
											   </div>
											</div>
				
							<?php
									}
								}
								if(!$flag){
									echo "<div>Nothing to show. </div>";
								}
							 ?>
                        </div> 
                    </div>
                </div>   
            </div>    
          </div>
        </div>
<?php
	}
	else{
		?>
        <div class="col-md-11">
            <div class="ossn-layout-module">
                <div class="module-title">
                    <div class="title"><?php echo $params['title']; ?></div>
                    <div class="controls">
                        <?php echo $params['controls']; ?>
                    </div>
                </div>
                <div class="module-contents">
                    <?php echo $params['content']; ?>
                </div>
            </div>
        </div>
		
		<?php
	}
?>