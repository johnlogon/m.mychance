<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
	if(isset($_SESSION['redirect_to'])){
		$goto = $_SESSION['redirect_to'];
		if($goto == "send-message"){
			unset($_SESSION['redirect_to']);
			$forward = "Location: ". ossn_site_url('send-message');
			header($forward);
			exit();
		}
	}
 
//unused pagebar skeleton when ads are disabled #628 
if(ossn_is_hook('newsfeed', "sidebar:right")) {
	$newsfeed_right = ossn_call_hook('newsfeed', "sidebar:right", NULL, array());
	$sidebar = implode('', $newsfeed_right);
	$isempty = trim($sidebar);
}  
?>
<div class="container">
	<div class="row">
    	<div style="
            position: fixed;
            width: 100%;
            height: 25px;
            background: white;
            top: 0;
            z-index: 500000;
        "></div>
       	<?php echo ossn_plugin_view('theme/page/elements/system_messages'); ?>    
		<div class="ossn-layout-newsfeed asd">
			<div class="col-md-7" style="padding-left:0px;padding-right:0px;">
				<div class="newsfeed-middle">
					<?php echo $params['content']; ?>
				</div>
			</div>
			<div class="col-md-4" style="padding-left:0px;padding-right:0px;">
            	<?php if(!empty($isempty)){ ?>
				<div class="newsfeed-right">
					<?php
						echo $sidebar;
						?>                            
				</div>
                <?php } ?>
			</div>
		</div>
        <div id="comment-full-view" style="display:none;width:100%"></div>
	</div>
	<?php echo ossn_plugin_view('theme/page/elements/footer');?>
</div>