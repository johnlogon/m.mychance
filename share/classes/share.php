<?php
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);
	class Share{
		public function connect(){
			$conn = new mysqli(MY_HOST,MY_USER, MY_PWD, MY_DB);
			if ($conn->connect_errno) {
				$this->logger($conn->connect_error);
			}
			return $conn;
		}
		public function logger($log){
			$file = $_SERVER['DOCUMENT_ROOT']."/share/log.txt";
			$logFile = fopen($file, 'a');
			$time = date('d-m-Y h:i:s A', time());
			$time= $this->getTimeStamp();
			$log.="       time:".$time."\n\n";
			fwrite($logFile, $log);
			fclose($logFile);
		}
		private function setTimeZone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		private function getTimeStamp(){
			$this->setTimeZone();
			$time = time();
			return $time;
		}
		public function sharePost($post_guid){
			$OssnWall = new OssnWall;
			$postOriginal = $OssnWall->GetPost($post_guid);
			$item = ossn_wallpost_to_item($postOriginal);
			return $this->createPostEntry($item);
			//return $item;
		}
		private function createPostEntry($item){
			$mysqli = $this->connect();
			$mysqli->set_charset("utf8");
			$sql = "INSERT INTO ossn_object (
					  owner_guid,
					  type,
					  time_created,
					  title,
					  description,
					  subtype,
					  post_owner_guid,
					  post_guid,
					  media,
					  thumbnail
					) 
					VALUES
					  (
						?,
						?,
						?,
						?,
						?,
						?,
						?,
						?,
						?,
						?
					  )";
			$loggedin_user = ossn_loggedin_user()->guid;
			$stmt = $mysqli->prepare($sql);
			$time = time();
			$description = '{"post":"'.$item['text'].'"}';
			$post_owner_guid = $item['post']->owner_guid;
			if(isset($_POST['group']))
				$post_owner_guid = $item['user']->guid;
			$post_guid = $item['post']->guid;
			$type = 'user';
			$title = '';
			$subtype = 'wallshare';
			$image = $item['image'] == null ? '' : $item['image'];
			$thumbnail = $item['post']->thumbnail;
			if($item['post']->subtype=="wall_video"){
				$subtype = "wallsharevideo";
				$image = $item['post']->media;
			}
			$stmt->bind_param('isisssiiss', $loggedin_user, $type, $time, $title, $description, $subtype, $post_owner_guid, $post_guid, $image, $thumbnail);
			if(!$stmt->execute())
				return false;
			$params['owner_guid'] = $stmt->insert_id;
			$params['time_created'] = $time;
			$params2 = $this->setFirstOssnEntityParams($params);
			$params = array_merge($params, $params2);
			$first = $this->createEntityEntry($params);
			$params['subtype'] = 'access';
			$second = $this->createEntityEntry($params);
			
			$metadata['guid'] = $first;
			$metadata['value'] = $loggedin_user;
			$this->createEntityMetaDataEntry($metadata);
			
			$metadata['guid'] = $second;
			$metadata['value'] = 3;
			$this->createEntityMetaDataEntry($metadata);
			return true;
		}
		private function createEntityEntry($params){
			$mysqli = $this->connect();
			$sql = "INSERT INTO ossn_entities (
					  owner_guid,
					  type,
					  subtype,
					  time_created,
					  time_updated,
					  permission,
					  active
					) 
					VALUES
					  (
						?,
						?,
						?,
						?,
						?,
						?,
						?
					  )";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('issiiii', $params['owner_guid'], $params['type'], 
										  $params['subtype'], $params['time_created'], 
										  $params['time_updated'], $params['permission'], 
										  $params['active']);
			if($stmt->execute())
				return $stmt->insert_id;
			else
				throw new Exception($mysqli->error);
			return 0;
		}
		private function createEntityMetaDataEntry($params){
			$mysqli = $this->connect();
			$sql = "INSERT INTO ossn_entities_metadata (
					  id,
					  guid,
					  value
					) 
					VALUES
					  (
						?,
						?,
						?
					  )";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('iis', $params['id'], $params['guid'], 
										  $params['value']);
			if($stmt->execute())
				return $stmt->insert_id;
			return 0;
		}
		private function setFirstOssnEntityParams($params){
			$params['type'] = 'object';
			$params['subtype'] = 'poster_guid';
			$params['time_updated'] = 0;
			$params['permission'] = 2;
			$params['active'] = 1;
			return $params;
		}
	}
?>