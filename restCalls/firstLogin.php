<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{
		require_once("init.php");
		require_once("../system/start.php");
		if(isset($_GET)){
			//$isValidUser = false;
			$username = $_GET['userName'];
			$password = $_GET['password'];
			$oldToken = '';
			$newToken = '';
			$data = array();
			
			if(isset($_GET['oldToken']))
				$oldToken = $_GET['oldToken'];
			if(isset($_GET['newToken'])){
				$newToken = $_GET['newToken'];
				$_SESSION['DEVICE_ID'] = $newToken;
			}	
			$sql = "SELECT * FROM ossn_users WHERE username='".$username."'";
			$result = mysqli_query($db, $sql);
			if(!$result){
				throw new Exception("Exception in login.php: ". mysqli_error($db));
			}
			if((mysqli_num_rows($result))>0){
				$row = mysqli_fetch_array($result, MYSQLI_ASSOC);
				$expected_password = $row['password'];
				$salt = $row['salt'];
				$guid = $row['guid'];
				$password_received = md5($password.$salt);
				if($password_received==$expected_password){
					$data['is_activated'] = 1;
					if($row['ph_activation']!=""){
						$data['is_activated'] = 0;
						$data['id'] = $row['guid'];
						echo json_encode($data);
						exit;
					}
					$data['fullName'] = $row['first_name'] . " " . $row['last_name']; 
					$data['id'] = $row['guid'];
					$data['type'] = $row['type'];
					if($oldToken!=''){
						$sql = "DELETE FROM ossn_deviceids_socialmedia WHERE device_id='".$oldToken."' AND userid= $guid";
						if(!mysqli_query($db, $sql))
							error_write("Error deleting record from table ossn_deviceids_socialmedia in login.php: ". mysqli_error($db));
					}
					if($newToken!=''){
						$sql = "SELECT id FROM ossn_deviceids_socialmedia WHERE device_id='$newToken'";
						$r = mysqli_query($db, $sql);
						if(!mysqli_num_rows($r)>0){
							$sql = "INSERT INTO ossn_deviceids_socialmedia (userid, username, device_id) VALUES ($guid, '$username', '".$newToken."')";
							if(!mysqli_query($db, $sql))
								error_write("Error adding new record in ossn_deviceids_socialmedia in login.php: ". mysqli_error($db));
						}
												
					}
					//$isValidUser = true;
					$groups_user = ossn_get_user_groups(ossn_user_by_username($username));
					$groups = array();
					if($groups_user) {
							foreach($groups_user as $group) {
									$icon = ossn_site_url('components/OssnGroups/images/group.png');
									$groups[] =array(
											'text' => $group->title,
											'url' => ossn_group_url($group->guid),
											'guid' =>$group->guid,
											'section' => 'groups',
											'icon' => $icon
									);
									unset($icon);
							}
					}
					$data['groups'] = $groups;
				}				
			}
			echo json_encode($data);
		}
	}
	catch(Exception $e){
		error_write($e->getMessage());
	}
?>