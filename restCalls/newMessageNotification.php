<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{
		require_once("db_connect.php");
		require_once("error_log.php");
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$data = array();
			$sql = "SELECT message, message_from, COUNT(*) AS `num` FROM ossn_messages WHERE message_to=$id AND viewed='0' GROUP BY message_from";
			$result = mysqli_query($db, $sql);
			if(!$result){
				error_write(mysqli_error($db));
				die();
			}
			$msgCount = 0;
			$data['senders'] = array();
			$data['message'] = array();
			$i = 0;
			while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				$msgCount += $row['num'];
				$data['senders'][$i] = $row['message_from'];
				$msg =  $row['message'];
				if(strlen($msg)>=20){
					$msg = substr($msg, 0, 20)."...";
				}
				$data['message'][$i++] = $msg;
			}
			$data['count'] = $msgCount;
			echo json_encode($data);
		}
		else{
			error_write("Invalid POST request in newMessageNotification.php");
		}
		
	}
	catch(Exception $e){
		error_write("Exception in newMessageNotification.php: ". $e->getMessage());
	}

?>