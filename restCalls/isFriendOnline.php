<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{
		require_once("error_log.php");
		require_once("db_connect.php");
		if(isset($_POST['id'])){
			$userid = $_POST['id'];
			$sql = "SELECT relation_from FROM ossn_relationships WHERE relation_from IN(SELECT relation_to FROM ossn_relationships WHERE 
					relation_from='$userid') AND relation_to='$userid'";
			$result = mysqli_query($db, $sql);
			if(!$result){
				echo mysqli_error($db);
				error_write("Error fetching friends in isFriendOnline.php: ". mysqli_error($db));
				die();
			}
			$friends = array();
			$r = 0;
			$c = 0;
			while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
				$friends[$r]=array();
				$guid = $row['relation_from'];
				$sql = "SELECT last_activity from ossn_users WHERE guid=$guid";
				$isOnilne = false;
				if($result1=mysqli_query($db, $sql)){
					$row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
					if($row1['last_activity']>(time()-10))
						$isOnilne = true;
				}
				else{
					error_write("Error fetching data from ossn_users in isFriendOnline.php". mysqli_error($db));
				}
				$friends[$r]['id'] = $guid;
				$friends[$r]['online'] = $isOnilne;
				$r++;
			}
			echo json_encode($friends);
		}
		else{
			error_write("Invalid POST request: Parameter 'id' not found in isFriendOnline.php");
		}
	}
	catch(Exception $e){
		error_write("Exception in isFriendOnline.php: ". $e->getMessage());
	}

?>