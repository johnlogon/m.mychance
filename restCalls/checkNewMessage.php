<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{
		require_once("db_connect.php");
		require_once("error_log.php");
		$ret=array();
		if(isset($_POST['message_from']) && isset($_POST['message_to'])){
			$message_from = $_POST['message_from'];
			$message_to = $_POST['message_to'];
			$sql="SELECT id, message FROM ossn_messages WHERE message_from='$message_from' AND message_to='$message_to' AND viewed='0' ORDER BY id DESC";
				$result=mysqli_query($db,$sql);
				if(!$result){
					error_write("Error in checkNewMessages: " . mysqli_error());
					die();
				}
				$i = 0;
				while($row=mysqli_fetch_array($result,MYSQLI_ASSOC)){
					$ret[$i] = $row['message'];
					$sql = "UPDATE ossn_messages SET viewed='1' WHERE id=". $row['id'];
					mysqli_query($db, $sql);
					$i++;
				}
				
		}
		else{
			error_write("Invalid post request in checkNewMessage.php, Required parameter not found");
		}
		echo json_encode($ret);
	}
	catch(Exception $e){
		error_write($e->getMessage());
	}

?>