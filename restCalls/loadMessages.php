<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{
		require_once("db_connect.php");
		require_once("error_log.php");
		$html = "";
		if(isset($_POST)){
			if(!isset($_POST['myId']) || !isset($_POST['friendId'])){
				throw new Exception("Parameter myId or friendId not found");
			}
			$myId = $_POST['myId'];
			$friendId = $_POST['friendId'];
			$sql = "SELECT * FROM (SELECT * FROM ossn_messages WHERE (message_to=$myId AND message_from=$friendId) OR 
					(message_to=$friendId AND message_from=$myId) ORDER BY id DESC LIMIT 20 OFFSET 0)t ORDER BY id ASC";
			if(!$result = mysqli_query($db, $sql)){
				throw new Exception(mysqli_error($db));
			}
			else{
				$data = array();
				$i = 0;
				$row1 = mysqli_fetch_array(mysqli_query($db, "SELECT username FROM ossn_users WHERE guid=".$friendId), MYSQLI_ASSOC);
				$data[$i++] = $row1['username'];
				while($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){
					$data[$i] = array();
					$data[$i]["message_from"] = $row['message_from'];
					$data[$i++]["message"] = $row['message'];
					$sql = "UPDATE ossn_messages SET viewed='1' WHERE id=". $row['id'];
					mysqli_query($db, $sql);
				}				
			}
			echo json_encode($data);
		}
		else{
			error_write("Invalid POST request in loadMessages.php");
		}
	}
	catch(Exception $e){
		error_write("Exception in loadMessages.php: "+ $e->getMessage());
	}

?>