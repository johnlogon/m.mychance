<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{
		require_once("db_connect.php");
		require_once("error_log.php");
		if(isset($_POST['id'])){
			$userid = $_POST['id'];
			$time=time();
			$sql="UPDATE ossn_users SET last_activity=$time WHERE guid=$userid";
			if(!mysqli_query($db, $sql))
				error_write("Error updating table ossn_users in updateActivity.php: " . mysqli_error($db));
			else
				echo true;
		}
		else{
			error_write("Invalid POST request, Parameter 'id' not found in updateActivity.php");
		}
	}
	catch(Exception $e){
		error_write("Exception in updateActivity.php: " . $e->getMessage());
	}

?>