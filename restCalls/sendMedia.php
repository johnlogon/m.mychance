<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	require_once('db_connect.php');
	require_once('error_log.php');		
	if(isset($_POST)){
		$messagefrom=$_POST['message_from'];
		$fromuserfullname=$_POST['message_from_name'];
		$messageto=$_POST['message_to'];
		$touserfullname = $_POST['message_to_name'];
		if (!($_FILES["file"]["error"] > 0)){
				/** IF PHOTO/VIDEO/AUDIO IS SUBMITTED **/
			 
				$allowedExts = array("webm", "mp4", "WEBM", "MP4", 
									 "gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF",
									 "mp3", "wav", "MP3", "WAV");
				$photoExts = array("gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
				$videoExts = array("mpeg", "avi", "mp4", "MPEG", "AVI", "MP4");
				$audioExts = array("mp3", "wav", "MP3", "WAV");
				$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

				if ((($_FILES["file"]["type"] == "video/mp4")
					|| ($_FILES["file"]["type"] == "video/webm")
					|| ($_FILES["file"]["type"] == "image/gif")
     	 			|| ($_FILES["file"]["type"] == "image/jpeg")
      	 			|| ($_FILES["file"]["type"] == "image/jpg")
      	 			|| ($_FILES["file"]["type"] == "image/pjpeg")
      	 			|| ($_FILES["file"]["type"] == "image/x-png")
      	 			|| ($_FILES["file"]["type"] == "image/png")
					|| ($_FILES["file"]["type"] == "audio/mp3")
					|| ($_FILES["file"]["type"] == "audio/mpeg")
					|| ($_FILES["file"]["type"] == "audio/wav"))
					&& in_array($extension, $allowedExts)){
						if(in_array($extension, $photoExts)){
							$dir=$messagephoto;
							$type='photo';
						}
						else if(in_array($extension, $videoExts)){
							$dir=$messagevideo;
							$type='video';
						}
						else if(in_array($extension, $audioExts)){
							$dir=$messageaudio;
							$type='audio';
						}
						
  						if ($_FILES["file"]["error"] > 0){
    						echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    					}
  						else{
							$time=time();
							$temp = explode(".", $_FILES["file"]["name"]);
							$newfilename = round(microtime(true)) . '.' . end($temp);
      						move_uploaded_file($_FILES["file"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].$dir .  $newfilename);
							$link="http://".$_SERVER['SERVER_NAME'] . $dir .  $newfilename;
							$link1= $dir .  $newfilename;
							$message='';
							if($type=='photo'){
								$message = "<img id=message-photo src=$link width=100px height=100px>";
							}
							else if($type=='video'){
								$message = "<video class=message
												preload=none controls>
													<source type=video/mp4 src=".$link.">
													<source type=video/webm src=".$link.">
											 </video>";
							}
							else if($type=='audio'){
								$message = "<audio controls class=message>
												<source src=".$link." type=audio/mp3>
												<source src=".$link." type=audio/ogg>
												<source src=".$link." type=audio/mpeg>
												<source src=".$link." type=audio/wav>
												Your browser does not support Audio Playback.
											 </audio>";
								
							}
							$sql="INSERT INTO ossn_messages(message_from, message_to, message, viewed, time, fromuserfullname, touserfullname,
							 message_type, link) VALUES (".$messagefrom.",".$messageto.", '".$message."', '0', '".$time."', '".$fromuserfullname."',
							 '".$touserfullname."', '".$type."', '".$link1."')";
							 
							if(!mysqli_query($db,$sql)){
								error_write(mysqli_error($db));
								die();
							}
							else{
								echo $message;
							}
    					}
  				}
				else{
 					error_write("Invalid file");
  				}
		 	}
	
}
?>