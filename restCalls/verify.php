<?php

	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	require_once('application/start.php');
	
	if(!isset($_POST['code']) || empty($_POST['code'])){
		echo false;
		exit;
	}
	
	$code = $_POST['code'];
	$guid = $_POST['guid'];
	
	$user = new OssnUser();
	echo $user->ValidatePhoneNumber($code, $guid);
?>