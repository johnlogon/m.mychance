<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	require_once("../system/start.php");
	require_once("error_log.php");
	try{
		if(isset($_POST['id'])){
			$userid = $_POST['id'];
			$data = array();
			
			$notification = new OssnNotifications;
			$count_notif = $notification->countNotification($userid);
			if(!$count_notif)
				$data['notification'] = 0;
			else
				$data['notification'] = $count_notif;
			
			if(class_exists('OssnMessages')){
				$messages = new OssnMessages;
				$count_messages = $messages->countUNREAD($userid);
				$data['message'] = $count_messages;
			}
			
			$friends = ossn_user_by_guid($userid)->getFriendRequests();
			if (count($friends) > 0 && !empty($friends)) {
				$friends_c = count($friends);
				$data['friends'] = $friends_c;
			}
			echo json_encode($data);
		}
	}
	catch(Exception $e){
		error_write($e->getMessage());
	}
?>