<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{	
		require_once("db_connect.php");
		require_once("error_log.php");
		if(isset($_POST['id'])){
			$id = $_POST['id'];
			$sql="SELECT DISTINCT(message_from) FROM ossn_messages WHERE message_to = $id UNION SELECT DISTINCT(message_to) FROM ossn_messages WHERE message_from = $id";
			$res=mysqli_query($db,$sql)or die(mysqli_error());
			$to=0;
			$chats = array();
			$i = 0;
			$userid = '';
			date_default_timezone_set('Asia/Kolkata');
			while($arr=mysqli_fetch_array($res)){	
				$to=$arr[0];
				$sql="SELECT MAX(id) FROM ossn_messages WHERE message_from=$id AND message_to='$to' 
						OR message_from='$to' AND message_to=$id";
				$r=mysqli_query($db,$sql);
				if(!$r){
					error_write(mysqli_error($db));
					die();
				}
				$arr1=mysqli_fetch_array($r);
				if($arr1[0]!=null && $arr1[0]!=''){
					$sql = "SELECT * FROM ossn_users WHERE guid = $to";
					$result = mysqli_query($db, $sql);
					while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
						$chats[$i] = array();
						$chats[$i]['name'] = $row['first_name'] . " " . $row['last_name'];
						$chats[$i]['username'] = $row['username'];
						$chats[$i]['userid'] = $row['guid'];
						$result1 = mysqli_query($db, "SELECT message, message_type, time FROM ossn_messages WHERE id = ".$arr1[0]);
						if(!$result1){
							echo mysqli_error($db);
							error_write(mysqli_error($db));
						}
						$row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
						if($row1['message_type']=='' || $row1['message_type']==NULL){
							$msg =  $row1['message'];
							if(strlen($msg)>=20){
								$msg = substr($msg, 0, 20)."...";
							}
							$chats[$i]['message'] =$msg;
						}
						else
							$chats[$i]['message'] = "<i class='fa fa-paperclip'></i>  Attachment";
						$chats[$i]['time'] =date("h:i:s A", $row1['time']);
						$i++;
						
					}
				}
				
			}
			echo json_encode($chats);
		}
		else{
			error_write("Invalid POST request in fetchRecentChats.php: required parameter 'id' not present. ");
		}
	}
	catch(Exception $e){
		error_write("Exception in fetching recent messages: ". $e->getMessage());
	}
		
?>