<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	require_once('../system/start.php');
	
	$userId = $_POST['userId'];
	$user = new OssnUser;
	$user->guid = $userId;
	
	$friends['friends'] = $user->getFriendRequests();
	$friends_count      = count($friends['friends']);
	if($friends_count > 0 && !empty($friends['friends'])) {
		$data = ossn_plugin_view('notifications/pages/notification/friends', $friends);
		echo $data;
	} 
	else {
		echo '<div class="ossn-no-notification">' . ossn_print('ossn:notification:no:notification') . '</div>';
	}


?>