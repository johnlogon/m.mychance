<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	try{
		require_once("db_connect.php");
		require_once("error_log.php");
		$friends = array();
		$friendsoffline = array();
		if(isset($_POST['id'])){
			$userid = $_POST['id'];
			date_default_timezone_set('Asia/Kolkata');
			/*********** FETCHING FRIENDS ***********/
			$sql = "SELECT * FROM ossn_relationships WHERE relation_from IN(SELECT relation_to FROM ossn_relationships WHERE relation_from=$userid)
		  			AND relation_to=$userid";
			$result = mysqli_query($db, $sql);
			if(!$result){
				error_write("Error fetching friends in home.php: ". mysqli_error($db));
				die();
			}
			$rows = 0;
			$col = 0;
			$rows1 = 0;
			$col1 = 0;
			while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
				$id = $row['relation_from'];
				$name = '';
				$isOnline = "0";
				$dp = '';
				$lastMessage = '';
				$time = '';
				$sql = "SELECT username, first_name, last_name, last_activity FROM ossn_users WHERE guid=$id";
				if(!$result1=mysqli_query($db, $sql)){
					error_write("Error fetching user data in home.php: ". mysqli_error($db));
					die();
				}
				$row1 = mysqli_fetch_array($result1, MYSQLI_ASSOC);
				if($row1['last_activity']>(time()-10)){
					$isOnline = "1";
					$friends[$rows][$col] = $id;
					$friends[$rows][++$col] =  $row1['first_name']. " " . $row1['last_name'];
					$friends[$rows][++$col] = $isOnline;
					$friends[$rows][++$col] = $row1['username'];
					$col = 0; 
					$rows++;
				}
				else{
					$isOnline = "0";
					$friendsoffline[$rows1][$col1] = $id;
					$friendsoffline[$rows1][++$col1] =  $row1['first_name']. " " . $row1['last_name'];
					$friendsoffline[$rows1][++$col1] = $isOnline;
					$friendsoffline[$rows1][++$col1] = $row1['username'];
					$col1 = 0; 
					$rows1++;
				}			
			}
			$friends = array_merge($friends, $friendsoffline);
			$rows = $rows+$rows1;
			$friends[$rows++][0] = "endofFriends";
			
			$sql = "SELECT first_name, last_name FROM ossn_users WHERE guid=$userid";
			if($result2=mysqli_query($db, $sql)){
				$row1 = mysqli_fetch_array($result2, MYSQLI_ASSOC);
				$friends[$rows][$col] = $row1['first_name'] ." ". $row1['last_name'];
				$friends[$rows][++$col] = $userid;
			}
			else{
				error_write("SQL error in home.php: " . mysqli_error($db));
				die();
			}
		}
		else{
			error_write("Invalide POST request: Parameter 'id' not found in home.php");
		}
		echo json_encode($friends);
	}
	catch(Exception $e){
		error_write($e->getMessage());
	}

?>