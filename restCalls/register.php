<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	require_once('../system/start.php');
	require_once('db_connect.php');
	
	if(isset($_POST)){
		$user['username'] = $_POST['username'];
		$user['firstname'] = $_POST['firstname'];
		$user['lastname'] = $_POST['lastname'];
		$user['email'] = $_POST['email'];
		$user['password'] = $_POST['password'];
		$user['dob'] = $_POST['dob'];
		$user['gender'] = $_POST['gender'];
		$user['mobile_number'] = $_POST['mobile_number'];
		
		$add = new OssnUser;
		$add->username = $user['username'];
		$add->first_name = $user['firstname'];
		$add->last_name = $user['lastname'];
		$add->email = $user['email'];
		$add->password = $user['password'];
		$add->sendactiviation = true;
		$add->gender = $user['gender'];
		$add->mobile_number = $user['mobile_number'];
		//$add->dob = date_format($user['dob'],"d/m/Y");
		
		if (!$add->isUsername($user['username'])) {
			$em['success'] = 0;
			$em['message'] =  ossn_print('username:error');
			echo json_encode($em);
			exit;
		}
		if($add->isOssnUsername()){
			$em['success'] = 0;
			$em['message'] = ossn_print('username:inuse');
			echo json_encode($em);
			exit;	
		}
		if($add->isOssnEmail()){
			$em['success'] = 0;
			$em['message'] = ossn_print('email:inuse');
			echo json_encode($em);
			exit;	
		}
		//check if email is valid email 
		if(!$add->isEmail()){
			$em['success'] = 0;
			$em['message'] =  ossn_print('email:invalid');
			echo json_encode($em);
			exit;		
		}
		if ($add->addUser()) {
			$user_db = $add->getUser();
			$sql = "INSERT INTO ossn_entities (owner_guid, type, subtype, time_created, time_updated, permission, active)
					VALUES (".$user_db->guid.", 'user', 'birthdate', ".time().", 0, 2, 1 )";
			if(mysqli_query($db, $sql)){
				$id = mysqli_insert_id($db);
				$date = date_create($user['dob']);
				$sql = "INSERT INTO ossn_entities_metadata (guid, value) VALUES($id, '".date_format($date,"d/m/Y")."')";
				mysqli_query($db, $sql);
			}
			$sql = "INSERT INTO ossn_entities (owner_guid, type, subtype, time_created, time_updated, permission, active)
					VALUES (".$user_db->guid.", 'user', 'mobile_number', ".time().", 0, 2, 1 )";
			if(mysqli_query($db, $sql)){
				$id = mysqli_insert_id($db);
				$sql = "INSERT INTO ossn_entities_metadata (guid, value) VALUES($id, '".$user['mobile_number']."')";
				mysqli_query($db, $sql);
			}
			$em['success'] = 1;
			$em['message'] = ossn_print('account:created:email');
			$em['guid'] = $user_db->guid;
			echo json_encode($em);
			exit;
		} else {
			$em['success'] = 0;
			$em['message'] = ossn_print('account:create:error:admin');
			echo json_encode($em);
			exit;
		}
	}
	
?>