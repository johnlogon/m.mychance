<?php
	/******** ERROR LOG ******/
	require_once("constants.php");
	function createLogFile(){
		global $file_name;
		$error_file = fopen("log.txt", 'a') or die("Cant create/open error log");
		return $error_file;
	}
	
	function getTimeAndDate(){
		$date = new DateTime("now", new DateTimeZone('Asia/Kolkata') );
		return $date->format('Y-m-d H:i:s');
	}
	
	function error_write($error_message){
		$file = createLogFile();
		$date_time = getTimeAndDate();
		$formatted_error_message =  $date_time . "\r\n" . $error_message . "\r\n\r\n\r\n";
		fwrite($file, $formatted_error_message);
		fclose($file);
		
	}
?>