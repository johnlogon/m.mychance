<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	
	require_once("../system/start.php");
	
	if(isset($_POST)){
		$action = $_POST['action'];
		$userId = $_POST['userId'];
		$friendId = $_POST['friendId'];
		
		if($action=='add'){
			if (ossn_add_friend($userId, $friendId)) {
				if (!ossn_is_xhr()) {
					echo 1;
				}
				else
					echo 0;
			} 
		}
		else{
			if (ossn_remove_friend($userId, $friendId)) {
				if (!ossn_is_xhr()) {
					echo 0;
				}
				if (ossn_is_xhr()) {
					echo 1;
				}
			}
		}
	}

?>