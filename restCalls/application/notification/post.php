<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$title = ossn_print('post:view');
$wall  = new OssnWall;
$post  = input('guid');
$post  = $wall->GetPost($post);
if(empty($post->guid) || empty($post)) {
		echo ossn_error_page();
}
$params['post'] = $post;

$contents = array(
		'content' => ossn_plugin_view('wall/pages/view', $params)
);
$content  = ossn_set_page_layout('newsfeed', $contents);
echo $content;
?>