<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');
$title    = 'Notifications';
$contents = array(
		'content' => ossn_plugin_view('notifications/pages/all')
);
$content  = ossn_set_page_layout('media', $contents);
echo $content;
?>