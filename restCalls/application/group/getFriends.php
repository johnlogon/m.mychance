<?php
	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: * ");
	header('Access-Control-Allow-Methods: *');
	
	session_id($_POST['session_id']);
	session_start();
	
	require_once('../start.php');
	
	$user = ossn_loggedin_user();
	
	$friends = $user->getFriends();
	echo json_encode($friends);
?>