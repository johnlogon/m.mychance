<?php
	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: * ");
	header('Access-Control-Allow-Methods: *');
	
	session_id($_POST['session_id']);
	session_start();
	
	require_once('../start.php');
	
	$group_id = $_POST['group_id'];
	
	$friends = json_decode($_POST['friends']);
	$obj = new OssnGroup;
	foreach($friends as $friend){
		echo $obj->inviteFriend($group_id, $friend);
	}
?>