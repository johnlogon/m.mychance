<?php
	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: * ");
	header('Access-Control-Allow-Methods: *');
	
	session_id($_POST['session_id']);
	session_start();
	
	require_once('../start.php');
	
	$share_guid = $_POST['share_guid'];
	$group_guid = $_POST['group_guid'];
	$group = new OssnGroup;
	echo json_encode($group->sharePost($share_guid, $group_guid));
?>