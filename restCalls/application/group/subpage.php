<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$subpage = $_POST['subpage'];
$pages[1] = $subpage;
$guid = $_POST['guid'];
$pages[0] = $guid;

if(empty($pages[0])) {
	echo ossn_error_page();
	die();
}
if(!empty($pages[0]) && !empty($pages[0])) {
	if(isset($pages[1])) {
			$params['subpage'] = $pages[1];
	} else {
			$params['subpage'] = '';
	}
	
	$group = ossn_get_group_by_guid($pages[0]);
	if(empty($group->guid)) {
			echo ossn_error_page();
			die();
	}
	ossn_set_page_owner_guid($group->guid);
	/*ossn_trigger_callback('page', 'load:group');*/
	
	
	$params['group']     = $group;
	$title               = $group->title;
	$view                = ossn_plugin_view('groups/pages/profile', $params);
	$contents['content'] = ossn_group_layout($view);
	$content             = ossn_set_page_layout('contents', $contents);
	echo $content;
}

?>