<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$guid = $_POST['guid'];
$group = ossn_get_group_by_guid($guid);
if(empty($group->guid)) {
	echo ossn_error_page();
	die();
}
ossn_set_page_owner_guid($group->guid);
ossn_trigger_callback('page', 'load:group');


$params['group']     = $group;
$title               = $group->title;
$view                = ossn_plugin_view('groups/pages/profile', $params);
$contents['content'] = ossn_group_layout($view);
$content             = ossn_set_page_layout('contents', $contents);
echo $content;
?>