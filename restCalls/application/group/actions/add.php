<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$data = array();
$add = new OssnGroup;
$params['owner_guid'] = ossn_loggedin_user()->guid;
$params['name'] = input('groupname');
$params['description'] = input('description');
$params['privacy'] = input('privacy');
if ($add->createGroup($params)) {
	$data['text'] = 'created';
	$data['name'] = $params['name'];
	$data['type'] = 'groupAdd';
	$data['guid'] = $add->getGuid();
	echo json_encode($data);
} else {
  	$data['err']  = 'error';
	echo json_encode($data);
}