<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$name = input('groupname');
$desc = input('groupdesc');
$memb = input('membership');

$data = array();

$group = ossn_get_group_by_guid(input('group'));
if ($group->owner_guid !== ossn_loggedin_user()->guid && !ossn_isAdminLoggedin()) {
   $data['err'] = "error";
   echo json_encode($data);
   die();
}

$edit = new OssnGroup;
$access = array(
    OSSN_PUBLIC,
    OSSN_PRIVATE
);

if (in_array($memb, $access)) {
    $edit->data = new stdClass;
    $edit->data->membership = $memb;
}

if ($edit->updateGroup($name, $desc, $group->guid)) {
	/**** EDIT SUCCESS, RETURN TEMPLATE ***/
	$pages[1] = 'edit';
	$pages[0] = $group->guid;
	
	if(empty($pages[0])) {
		echo ossn_error_page();
		die();
	}
	if(!empty($pages[0]) && !empty($pages[0])) {
		if(isset($pages[1])) {
				$params['subpage'] = $pages[1];
		} else {
				$params['subpage'] = '';
		}
		
		$group = ossn_get_group_by_guid($pages[0]);
		if(empty($group->guid)) {
				echo ossn_error_page();
				die();
		}
		ossn_set_page_owner_guid($group->guid);
		/*ossn_trigger_callback('page', 'load:group');*/
		
		
		$params['group']     = $group;
		$title               = $group->title;
		$view                = ossn_plugin_view('groups/pages/profile', $params);
		$contents['content'] = ossn_group_layout($view);
		$content             = ossn_set_page_layout('contents', $contents);
		$data['text'] = $content;
		$data['type'] = 'editGroup';
		echo json_encode($data);
		die();
	}
} else {
   $data['err'] = "error";
   echo json_encode($data);
   die();
}
