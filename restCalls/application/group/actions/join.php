<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$add = new OssnGroup;
$group = input('group');

if (empty($group)) {
    echo 0;
	die();
}
if ($add->sendRequest(ossn_loggedin_user()->guid, $group)) {
    echo 1;
	die();
} else {
    echo 0;
}