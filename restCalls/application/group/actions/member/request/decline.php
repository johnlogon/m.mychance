<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../../../start.php');

$add = new OssnGroup;
$group = input('gGuid');
$user = input('uGuid');

if (ossn_get_group_by_guid($group)->owner_guid !== ossn_loggedin_user()->guid && !ossn_isAdminLoggedin()) {
   echo 0;
   die();
}
if ($add->deleteMember($user, $group)) {
   echo 1;
} else {
   echo 1;
}
