<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$OssnWall = new OssnWall;

$OssnWall->poster_guid = ossn_loggedin_user()->guid;

$owner = input('wallowner');
if (isset($owner) && !empty($owner)) {
    $OssnWall->owner_guid = $owner;
}

$OssnWall->type = 'group';

$post = input('post');
$friends = false;
$location = input('location');
if ($OssnWall->Post($post, $friends, $location, OSSN_PRIVATE)) {
	$guid = $OssnWall->getObjectId();
	$get  = $OssnWall->GetPost($guid);
	if($get) {
			$get = ossn_wallpost_to_item($get);
			$data = ossn_wall_view_template($get);
			echo $data;
	}
		
} 
else {
	echo 0;
}
