<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$guid = input('guid');
$group = ossn_get_group_by_guid($guid);

if(($group->owner_guid === ossn_loggedin_user()->guid) || ossn_isAdminLoggedin()){
	if ($group->deleteGroup($group->guid)) {
		/* EDIT SUCCESS, RETURN TEMPLATE */
		$data['html'] = '<div class="col-md-7" style="padding-left:0px;padding-right:0px;">
                               <div class="newsfeed-middle">
                                  <div class="ossn-wall-container">
                                     <form action="#" id="ossn-wall-form" class="ossn-form" method="post"  enctype="multipart/form-data">
                                        <fieldset>
                                           <div class="tabs-input">
                                              <div class="wall-tabs" style="display:none">
                                                 <div class="item">
                                                    <div class="ossn-wall-status"></div>
                                                    <div class="text">Post</div>
                                                 </div>
                                              </div>
                                              <textarea placeholder="My minds" name="post" id="post"></textarea>
                                              <div id="ossn-wall-friend" style="display:none;">
                                                 <input type="text" placeholder="Tag Friends" name="friends"
                                                    id="ossn-wall-friend-input"/>
                                              </div>
                                              <div id="ossn-wall-location" style="display:none;">
                                                 <input type="text" placeholder="Enter Location" name="location"
                                                    id="ossn-wall-location-input"/>
                                              </div>
                                              <div id="ossn-wall-photo" style="display:none;">
                                                 <input type="file" id="ossn_photo" name="ossn_photo"/>
                                              </div>
                                           </div>
                                           <div class="controls">
                                              <li class="ossn-wall-friend">
                                                 <i class="fa fa-users"></i>
                                              </li>
                                              <li class="ossn-wall-location">
                                                 <i class="fa fa-map-marker"></i>
                                              </li>
                                              <li class="ossn-wall-photo">
                                                 <i class="fa fa-picture-o"></i>
                                              </li>
                                              <div style="float:right;">
                                                 <div class="ossn-loading ossn-hidden"></div>
                                                 <input type="hidden" value="" name="session_id" id="session_id"  / >
                                                 <input class="btn btn-primary ossn-wall-post" id="wall-post-submit" 
                                                    type="submit" value="Mike"/>
                                              </div>
                                              <li class="ossn-wall-privacy">
                                                 <div class="ossn-wall-privacy-lock"></div>
                                                 <span><i class="fa fa-lock"></i><span class="hidden-xs">Privacy</span></span>
                                              </li>
                                           </div>
                                           
                                           <input type="hidden" value="" name="wallowner"/>
                                           <input type="hidden" name="privacy" id="ossn-wall-privacy" value="2"/>
                                        </fieldset>
                                     </form>
                                  </div>
                                  <div class="user-activity">
                                     '. ossn_plugin_view('wall/siteactivity').'
                                  </div>
                               </div>
                            </div>';
		echo $data['html'];
		die(); 
	} 
}
echo 0;