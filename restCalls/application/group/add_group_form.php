<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$params = array(
		'action' => '#',
		'data-action'=>  'restCalls/application/group/actions/add.php',
		'component' => 'OssnGroups',
		'class' => 'ossn-form'
);
$form   = ossn_view_form('add', $params, false);
echo ossn_plugin_view('output/ossnbox', array(
		'title' => ossn_print('add:group'),
		'contents' => $form,
		'callback' => '#ossn-group-submit'
));
?>