<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$block = new OssnBlock;
$user = input('user');
$user = ossn_user_by_guid($user);

//Admin profiles should be unblockable by 'normal' members 
if(!$user || $user->isAdmin()){
	echo json_encode(array(
		'done' => 0,
		'text' => ossn_print('user:block:error'),
	));
	die();
}
if ($block->addBlock(ossn_loggedin_user()->guid, $user->guid)) {
	echo json_encode(array(
		'done' => 1,
		'text' => ossn_print('user:blocked'),
	));
} else {
   echo json_encode(array(
		'done' => 0,
		'text' => ossn_print('user:block:error'),
	));
}