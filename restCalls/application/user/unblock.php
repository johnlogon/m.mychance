<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$block = new OssnBlock;
$user = input('user');

if ($block->removeBlock(ossn_loggedin_user()->guid, $user)) {
	echo json_encode(array(
		'done' => 1,
		'text' => ossn_print('user:unblocked'),
	));
} else {
	echo json_encode(array(
		'done' => 0,
		'text' => ossn_print('user:unblock:error'),
	));
}