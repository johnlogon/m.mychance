<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$poke = new OssnPoke;
$user = input('user');
if ($poke->addPoke(ossn_loggedin_user()->guid, $user)) {
    $user = ossn_user_by_guid($user);
	echo json_encode(array(
		'done' => 1,
		'text' => ossn_print('user:poked'),
	));
} else {
    echo json_encode(array(
		'done' => 0,
	));
}