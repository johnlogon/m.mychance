<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}

require_once('../../../start.php');


if(input('type')=='view'){

	$title = ossn_print('photos');
	$photo['photo'] = input('guid');
	
	$view            = new OssnPhotos;
	$image           = $view->GetPhoto($photo['photo']);
	$photo['entity'] = $image;
	
	//redirect user to home page if image is empty
	if(empty($image)) {
			echo 01;
			die();
	}
	//throw 404 page if there is no album access
	$albumget = ossn_albums();
	$owner    = $albumget->GetAlbum($image->owner_guid)->album;
	if($owner->access == 3) {
			if(!ossn_validate_access_friends($owner->owner_guid)) {
					echo 02;
					die();
			}
	}
	$contents          = array(
			'title' => ossn_print('photos'),
			'content' => ossn_plugin_view('photos/pages/photo/view', $photo),
	);
	//set page layout
	$content = ossn_set_page_layout('media', $contents);
	echo $content;
}
else if(input('type')=='profile'){
	
	$title          = ossn_print('photos');
	$photo['photo'] = input('guid');
	$type           = input('type');
	
	$view            = new OssnPhotos;
	$image           = $view->GetPhoto($photo['photo']);
	$photo['entity'] = $image;
	
	//redirect user if photo is empty
	if(empty($image->value)) {
			redirect();
	}
	$contents          = array(
			'title' => 'Photos',
			'content' => ossn_plugin_view('photos/pages/profile/photos/view', $photo),
	);
	//set page layout
	$content  = ossn_set_page_layout('media', $contents);
	echo $content  ;

}
else if(input('type')=='covers'){
	$title          = ossn_print('cover:view');
	$photo['photo'] = input('guid');
	$type           = input('type');
	
	$image          = ossn_get_entity($photo['photo']);
	$photo['entity']= $image;
	
	//redirect user if photo is empty
	if(empty($image->value)) {
			echo 03;
	}
	$contents          = array(
			'title' => 'Photos',
			'content' => ossn_plugin_view('photos/pages/profile/covers/view', $photo),
	);
	//set page layout
	$content = ossn_set_page_layout('media', $contents);

	echo $content;
}
else
	echo 04;

?>