<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}

require_once('../../../start.php');

$flag = 1;
$photoid = input('id');

$delete = ossn_photos();
$delete->photoid = $photoid;

$photo = $delete->GetPhoto($delete->photoid);

switch(input('type')){
	case 'view':
			$owner = ossn_albums();
			$owner = $owner->GetAlbum($photo->owner_guid);
			
			if (($owner->album->owner_guid == ossn_loggedin_user()->guid) || ossn_isAdminLoggedin()) {
				if ($delete->deleteAlbumPhoto()) {
					
					
					$title = ossn_print('photos');
			
			$user['album'] = input('guid');
			
			$albumget      = ossn_albums();
			$owner         = $albumget->GetAlbum($user['album'])->album;
			
			if(empty($owner)) {
				
			}
			
			//throw 404 page if there is no album access
			if($owner->access == 3) {
					if(!ossn_validate_access_friends($owner->owner_guid)) {
							echo ossn_error_page();
					}
			}
			//shows add photos if owner is loggedin user
			if(ossn_loggedin_user()->guid == $owner->owner_guid) {
					$addphotos     = array(
							'text' => ossn_print('add:photos'),
							'href' => 'javascript:void(0);',
							'id' => 'ossn-add-photos',
							'data-url' => '?album=' . $album[1],
							'class' => 'button-grey'
					);
					$delete_action = ossn_site_url("action/ossn/album/delete?guid=".input('guid'), true);
					$delete_album  = array(
							'text' => ossn_print('delete:album'),
							'href' => $delete_action,
							'data-guid' => input('guid'),
							'class' => 'button-grey delete-album'
					);
					$control       = ossn_plugin_view('output/url', $addphotos);
					$control .= ossn_plugin_view('output/url', $delete_album);
			} else {
					$control = false;
			}
			//Missing back button to photos #570
			$owner = ossn_user_by_guid($owner->owner_guid);
			$back         = array(
					'text' => ossn_print('back'),
					'href' => ossn_site_url("u/{$owner->username}/photos"),
					'class' => 'button-grey back-button'
			);
			$control           .= ossn_plugin_view('output/url', $back);									
			//set photos in module
			$contents          = array(
					'title' => ossn_print('photos'),
					'content' => ossn_plugin_view('photos/pages/albums', $user),
					'controls' => $control,
					'module_width' => '850px'
			);
			//set page layout
			$module['content'] = ossn_set_page_layout('module', $contents);
			$content           = ossn_set_page_layout('contents', $module);
			echo $content;
					
					
					
					
				} else {
					 $flag=0;
				}
			} else {
				$flag=0;
			}
	break;
	
	case 'profile':
	if(($photo->owner_guid == ossn_loggedin_user()->guid) || ossn_isAdminLoggedin()) {
			if($delete->deleteProfilePhoto()) {
				$user['user']                  = ossn_user_by_guid($photo->owner_guid);
				$user['user']->data->icon_time = time();
				$user['user']->save();
				
				
				$title = ossn_print('profile:photos');
				
			
			$user['user'] = ossn_user_by_guid(input('guid'));
			if(empty($user['user']->guid)) {
					echo ossn_error_page();
					die();
			}
			//Missing back button to photos #570
			$back         = array(
					'text' => ossn_print('back'),
					'href' => ossn_site_url("u/{$user['user']->username}/photos"),
					'class' => 'button-grey back-button'
			);
			$control           = ossn_plugin_view('output/url', $back);								
			//view profile photos in module layout
			$contents          = array(
					'title' => ossn_print('photos'),
					'content' => ossn_plugin_view('photos/pages/profile/photos/all', $user),
					'controls' => $control,
					'module_width' => '850px'
			);
			$module['content'] = ossn_set_page_layout('module', $contents);
			//set page layout
			$content           = ossn_set_page_layout('contents', $module);
			echo $content  ;
				
				
				
				
				
			} else {
					$flag = 0;
			}
	} else {
			$flag = 0;
	}
	break;
	case 'covers':
	if(($photo->owner_guid == ossn_loggedin_user()->guid) || ossn_isAdminLoggedin()) {
			if($delete->deleteProfileCoverPhoto()) {
				$user['user']                  = ossn_user_by_guid($photo->owner_guid);
				$user['user']->data->cover_time = time();
				$user['user']->save();
				
				
				$title = ossn_print('profile:covers');
										
			$user['user'] = ossn_user_by_guid(input('guid'));
			if(empty($user['user']->guid)) {
					echo ossn_error_page();
					die();
			}
			//Missing back button to photos #570
			$back         = array(
					'text' => ossn_print('back'),
					'href' => ossn_site_url("u/{$user['user']->username}/photos"),
					'class' => 'button-grey back-button'
			);
			$control           = ossn_plugin_view('output/url', $back);										
			//view profile photos in module layout
			$contents          = array(
					'title' => ossn_print('covers'),
					'content' => ossn_plugin_view('photos/pages/profile/covers/all', $user),
					'controls' => $control,
					'module_width' => '850px'
			);
			$module['content'] = ossn_set_page_layout('module', $contents);
			//set page layout
			$content           = ossn_set_page_layout('contents', $module);
			echo $content;
				
				
				
			} else {
				$flag = 0;
			}
	} else {
			$flag = 0;
	}

	break;
}