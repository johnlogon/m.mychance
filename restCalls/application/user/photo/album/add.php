<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}

require_once('../../../start.php');


$add = new OssnAlbums;
$user['user'] = ossn_loggedin_user();
$data = array();
if ($add->CreateAlbum($user['user']->guid, input('title'), input('privacy'))) {
	//show add album 
	$addalbum = array(
			'text' => ossn_print('add:album'),
			'href' => 'javascript:void(0);',
			'id' => 'ossn-add-album',
			'class' => 'button-grey'
	);
	$control  = ossn_plugin_view('output/url', $addalbum);

	$friends = ossn_plugin_view('photos/pages/photos', $user);
	$data['text'] = ossn_set_page_layout('module', array(
			'title' => ossn_print('photo:albums'),
			'content' => $friends,
			'controls' => $control
	));
	$data['type'] = 'album_add';
	echo json_encode($data);
} else {
   $data['err'] = 0;
   echo json_encode($data);
}