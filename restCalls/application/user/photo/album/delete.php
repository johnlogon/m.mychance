<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}

require_once('../../../start.php');

$guid = input('guid');
$album = new OssnAlbums;
if($album->deleteAlbum($guid)){
	
	/** PREPARING TEMPLATE FOR ALBUM VIEW **/
	
	if(isset($_POST['username'])){
	$params['user'] = ossn_user_by_username($_POST['username']);
	}
	
	$block = new OssnBlock;
	$user = $params['user'];
	$cover = new OssnProfile;
	
	$coverp = $cover->coverParameters($user->guid);
	$cover = $cover->getCoverURL($user);
	
	if(!empty($coverp[0])){
		$cover_top = "top:{$coverp[0]};";
	}
	if(!empty($coverp[1])){
		$cover_left = "left:{$coverp[1]};";
	}
	if (ossn_isLoggedIn()) {
		$class = 'ossn-profile';
	} else {
		$class = 'ossn-profile ossn-profile-tn';
	}
	?>
	<div class="ossn-profile container">
		<div class="row">
			<div class="col-md-11">
				<div class="<?php echo $class; ?>">
					<div class="top-container">
						<div id="container" class="profile-cover">
							<?php if (ossn_loggedin_user()->guid == $user->guid) { ?>
							<div class="profile-cover-controls">
								<a href="javascript:void(0);" onclick="Ossn.Clk('.coverfile');" class='btn-action change-cover'>
									<?php echo ossn_print( 'change:cover'); ?>
								</a>
								<a href="javascript:void(0);" id="reposition-cover" class='btn-action reposition-cover'>
									<?php echo ossn_print( 'reposition:cover'); ?>
								</a>
							</div>
							<form id="upload-cover" style="display:none;" method="post" enctype="multipart/form-data">
								<input type="file" name="coverphoto" class="coverfile" onchange="Ossn.Clk('#upload-cover .upload');" />
								<?php echo ossn_plugin_view( 'input/security_token'); ?>
								<input type="submit" class="upload" />
							</form>
							<?php } ?>
							<img id="draggable" class="profile-cover-img" src="<?php echo $cover; ?>" style='<?php echo $cover_top; ?><?php echo $cover_left; ?>' />
						</div>
						<div class="profile-photo">
							<?php if (ossn_loggedin_user()->guid == $user->guid) { ?>
							<div class="upload-photo" style="display:none;cursor:pointer;">
								<span onclick="Ossn.Clk('.pfile');"><?php echo ossn_print('change:photo'); ?></span>
	
								<form id="upload-photo" style="display:none;" method="post" enctype="multipart/form-data">
									<input type="file" name="userphoto" class="pfile" onchange="Ossn.Clk('#upload-photo .upload');" />
									<?php echo ossn_plugin_view( 'input/security_token'); ?>
									<input type="submit" class="upload" />
								</form>
							</div>
							<?php } 
							$viewer= '' ; 
							if (ossn_isLoggedIn() && get_profile_photo_guid($user->guid)) { 
									$viewer = 'onclick="Ossn.Viewer(\'photos/viewer?user=' . $user->username . '\');"';
							}
							?>
							<img src="<?php echo $user->iconURL()->larger; ?>" height="170" width="170" <?php echo $viewer; ?> />
						</div>
						<div class="user-fullname"><?php echo $user->fullname; ?></div>
						<?php echo ossn_plugin_view('profile/role', array('user' => $user)); ?>
						<div id='profile-hr-menu' class="profile-hr-menu">
							<ul>
								<li><a class="menu-user-timeline-timeline owner-link" href="<?php echo $params['user']->profileURL() ?>">Timeline</a></li>
								<li><a class="menu-user-timeline-friends" data-guid="<?php echo $params['user']->guid ?>" 
									href="<?php echo $params['user']->profileURL('/friends') ?>">Friends</a></li>
								<li><a class="menu-user-timeline-photos" data-guid="<?php echo $params['user']->guid ?>" 
									href="<?php echo $params['user']->profileURL('/photos') ?>">Photos</a></li></ul>
						</div>
	
						<div id="profile-menu" class="profile-menu">
							<?php if (ossn_isLoggedIn()) { if (ossn_loggedin_user()->guid == $user->guid) { ?>
							<a href="<?php echo $user->profileURL('/edit'); ?>" class='btn-action'>
								<?php echo ossn_print( 'update:info'); ?>
							</a>
							<?php } ?>
							<?php if (ossn_loggedin_user()->guid !== $user->guid) { if (!ossn_user_is_friend(ossn_loggedin_user()->guid, $user->guid)) { if (ossn_user()->requestExists(ossn_loggedin_user()->guid, $user->guid)) { ?>
							<a href="<?php echo ossn_site_url("action/friend/remove?cancel=true&user={$user->guid}", true); ?>" class='btn-action'>
									<?php echo ossn_print('cancel:request'); ?>
								</a>
							<?php } else { ?>
							<a href="<?php echo ossn_site_url("action/friend/add?user={$user->guid}", true); ?>" class='btn-action friend-add-a' data-push-id="<?php echo $user->guid ?>" id="push-identifier" data-action='add'>
									<?php echo ossn_print('add:friend'); ?>
							 </a>
							<?php } } else { ?>
							<a href="<?php echo ossn_site_url("action/friend/remove?user={$user->guid}", true); ?>"  class='btn-action friend-add-a' data-action='remove'>
								<?php echo ossn_print('remove:friend'); ?>
							</a>
							<?php } ?>
							<!--<a href="<?php /*?><?php echo ossn_site_url("messages/message/{$user->username}"); ?><?php */?>" id="profile-message" data-guid='<?php /*?><?php echo $user->guid; ?><?php */?>' class='btn-action'>-->
							<?php /*?><?php echo ossn_print('message'); ?><?php */?><!--</a>-->
							<div class="ossn-profile-extra-menu dropdown">
								<?php /*echo ossn_view_menu( 'profile_extramenu', 'profile/menus/extra');*/
									if(ossn_isLoggedin() ) {?>
										<div class="ossn-profile-extra-menu dropdown">
										   <div class="">
											  <a role="button" data-toggle="dropdown" class="btn-action" data-target="#" aria-expanded="false">
											  <i class="fa fa-sort-desc"></i></a>
											  <ul class="dropdown-menu multi-level" role="menu" 
												 aria-labelledby="dropdownMenu">
												 <li><a class="profile-menu-extra-poke" href="
													<?php echo ossn_site_url("action/poke/user?user=".$params['user']->guid); ?>">Poke</a></li>
													<?php
														$href='';
														$html ='';
														if($block->isBlocked(ossn_loggedin_user(), $params['user'])){
															$href=ossn_site_url("action/unblock/user?user=".$params['user']->guid);
															$html='Unblock';
														}
														else{
															$href=ossn_site_url("action/block/user?user=".$params['user']->guid);
															$html='Block';
														}
													?>
												 <li><a class="profile-menu-extra-block" href="<?php echo $href; ?>"><?php echo $html; ?></a>
													</li>
											  </ul>
										   </div>
										</div>
									<?php
									}
								 ?>
							</div>
							<?php } }?>
						</div>
						<div id="cover-menu" class="profile-menu">
							<a href="javascript:void(0);" onclick="Ossn.repositionCOVER();" class='btn-action'>
								<?php echo ossn_print('save:position'); ?>
							</a>
						</div>
					</div>
	
				</div>   
			  </div>   
		</div>
		<?php
			$_SESSION['yourguid'] = $user->guid;
			/*$myfriends = ossn_loggedin_user()->getFriends();
			$yourfriends = $user->getFriends();*/
		?>
		<div class="row ossn-profile-bottom">
			<?php
				$control      = false;
				//show add album if loggedin user is owner
				if(ossn_loggedin_user()->guid == $params['user']->guid) {
						$addalbum = array(
								'text' => ossn_print('add:album'),
								'href' => 'javascript:void(0);',
								'id' => 'ossn-add-album',
								'class' => 'button-grey'
						);
						$control  = ossn_plugin_view('output/url', $addalbum);
				}
				$friends = ossn_plugin_view('photos/pages/photos', $params);
				echo ossn_set_page_layout('module', array(
						'title' => ossn_print('photo:albums'),
						'content' => $friends,
						'controls' => $control
				));
			
			?>
			
		</div>
	</div>
	<script type="text/javascript">
	
		
	</script>
    <?php
       
} else {
	echo 0;        	
}
