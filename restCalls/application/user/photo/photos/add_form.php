<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

require_once("../../../start.php");

if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}
else if(isset($_GET['session_id'])){
	session_id($_GET['session_id']);
	session_start();
}

echo ossn_plugin_view('output/ossnbox', array(
	'title' => ossn_print('add:photos'),
	'contents' => ossn_plugin_view('photos/pages/photos/add'),
	'callback' => '#ossn-photos-submit'
));

?>