<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

require_once("../../../start.php");

if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}
else if(isset($_GET['session_id'])){
	session_id($_GET['session_id']);
	session_start();
}

$add = new OssnPhotos;
$data = array();
if ($add->AddPhoto(input('album'), 'ossnphoto', input('privacy'))) {
	//ADD SUCCESS; RETURN VIEW TEMPLATE
	
	$title = ossn_print('photos');
	
	$user['album'] = input('album');
	
	$albumget      = ossn_albums();
	$owner         = $albumget->GetAlbum($user['album'])->album;
	
	if(empty($owner)) {
		
	}
	
	//throw 404 page if there is no album access
	if($owner->access == 3) {
			if(!ossn_validate_access_friends($owner->owner_guid)) {
					echo ossn_error_page();
			}
	}
	//shows add photos if owner is loggedin user
	if(ossn_loggedin_user()->guid == $owner->owner_guid) {
			$addphotos     = array(
					'text' => ossn_print('add:photos'),
					'href' => 'javascript:void(0);',
					'id' => 'ossn-add-photos',
					'data-url' => '?album=' . $album[1],
					'class' => 'button-grey'
			);
			$delete_action = ossn_site_url("action/ossn/album/delete?guid={$album[1]}", true);
			$delete_album  = array(
					'text' => ossn_print('delete:album'),
					'href' => $delete_action,
					'class' => 'button-grey ossn-make-sure'
			);
			$control       = ossn_plugin_view('output/url', $addphotos);
			$control .= ossn_plugin_view('output/url', $delete_album);
	} else {
			$control = false;
	}
	//Missing back button to photos #570
	$owner = ossn_user_by_guid($owner->owner_guid);
	$back         = array(
			'text' => ossn_print('back'),
			'href' => ossn_site_url("u/{$owner->username}/photos"),
			'class' => 'button-grey back-button'
	);
	$control           .= ossn_plugin_view('output/url', $back);									
	//set photos in module
	$contents          = array(
			'title' => ossn_print('photos'),
			'content' => ossn_plugin_view('photos/pages/albums', $user),
			'controls' => $control,
			'module_width' => '850px'
	);
	//set page layout
	$module['content'] = ossn_set_page_layout('module', $contents);
	$content           = ossn_set_page_layout('contents', $module);
	$data['type'] = 'photo';
	$data['text'] = $content;
	echo json_encode($data);
   
} else {
	$data['err'] = "error";
    echo json_encode($data);
}