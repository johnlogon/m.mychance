<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$user['user'] = ossn_user_by_guid(input('guid'));
$_SESSION['yourguid'] = $user['user']->guid;
$friends      = ossn_plugin_view('profile/pages/friends', $user);
echo ossn_set_page_layout('module', array(
		'title' => ossn_print('friends'),
		'content' => $friends
));