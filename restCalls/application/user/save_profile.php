<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

require_once("../start.php");


session_id($_POST['session_id']);
session_start();
$data = array();

$entity = ossn_user_by_username(input('username'));
$user['user'] = $entity;
if(!$entity){
	$data['err'] = "error";
	$data['type'] = 'profileSave';
	echo json_encode($data);
	die();
}
$user['firstname'] = input('firstname');
$user['lastname'] = input('lastname');
$user['email'] = input('email');
$user['gender'] = input('gender');
$user['username'] = input('username');



$fields = ossn_user_fields_names();
foreach($fields['required'] as $field){
	$user[$field] = input($field);
}
if (!empty($user)) {
    foreach ($user as $field => $value) {
        if (empty($value)) {
			$data['errpro'] = ossn_print('fields:require');
			$data['type'] = 'profileSave';
			$data['type'] = 'profileSave';
			echo json_encode($data);
			die();
        }
    }
}
$password = input('password');

$OssnUser = new OssnUser;
$OssnUser->password = $password;
$OssnUser->email = $user['email'];

$OssnDatabase = new OssnDatabase;
$user_get = ossn_user_by_username(input('username'));
if ($user_get->guid !== ossn_loggedin_user()->guid) {
   $data['err'] = "error";
   $data['type'] = 'profileSave';
   echo json_encode($data);
   die();
}

$params['table'] = 'ossn_users';
$params['wheres'] = array("guid='{$user_get->guid}'");

$params['names'] = array(
    'first_name',
    'last_name',
    'email'
);
$params['values'] = array(
    $user['firstname'],
    $user['lastname'],
    $user['email']
);
//check if email is not in user
if($entity->email !== input('email')){
  if($OssnUser->isOssnEmail()){
    $data['errpro'] =ossn_print('email:inuse');
	$data['type'] = 'profileSave';
   	echo json_encode($data);
	die();
  }
}
//check if email is valid email 
if(!$OssnUser->isEmail()){
	$data['errpro'] = ossn_print('email:invalid');
	$data['type'] = 'profileSave';
   	echo json_encode($data);
	die();
}
//check if password then change password
if (!empty($password)) {
    if (!$OssnUser->isPassword()) {
		$data['errpro'] = ossn_print('password:error');
		$data['type'] = 'profileSave';
   		echo json_encode($data);
		die();
    }
    $salt = $OssnUser->generateSalt();
    $password = $OssnUser->generate_password($password, $salt);
    $params['names'] = array(
        'first_name',
        'last_name',
        'email',
        'password',
        'salt'
    );
    $params['values'] = array(
        $user['firstname'],
        $user['lastname'],
        $user['email'],
        $password,
        $salt
    );
}
$language = input('language');
$success  = ossn_print('user:updated');
if(!empty($language) && in_array($language, ossn_get_available_languages())){
	$lang = $language;
} else {
	$lang = 'en';
}
//save
if ($OssnDatabase->update($params)) {
    //update entities
	$user_get->data = new stdClass;
    $guid = $user_get->guid;
    if (!empty($guid)) {
		foreach($fields as $items){
				foreach($items as $field){
						$user_get->data->{$field} = $user[$field];
				}
		}
		$user_get->data->language = $lang;
        $user_get->save();
    }
	
	/* TEMPLATE */
	$user['user'] = ossn_user_by_username(input('username'));
	$params = array(
			'action' => '#',
			'component' => 'OssnProfile',
			'class' => 'ossn-edit-form',
			'params' => $user,
			'data-action' => 'restCalls/application/user/save_profile.php'
	);
	$form   = ossn_view_form('edit', $params, false);
	$data['text'] =  ossn_set_page_layout('module', array(
			'title' => ossn_print('edit'),
			'content' => $form
	));
	$data['type'] = 'profileSave';
    echo json_encode($data);
    die();
}
 
