<?php
	try{
		require_once('session.php');
		$obj = new MCTDAOFetchData;
		$cats = $obj->getAllCategories();
		$i = 0;
		foreach($cats as $cat){
			$data[$i]['cat'] = $cat['category'];
			$data[$i]['id'] = $cat['id'];
			$data[$i++]['count'] = $obj->getAdsCountByCategory($cat['id']);
		}
		echo json_encode($data);
	}
	catch(Exception $e){
		$obj->logger($e->getMessage());
		header('location:'.ossn_site_url('mtrade/error.php'));
		exit;
	}
?>