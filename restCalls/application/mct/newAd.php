<?php
	try{
		require_once('session.php');
		$obj = new MCTDAOFetchData;
		$user = ossn_loggedin_user();
		$data = array();
		if(isset($_POST['ad_id']) && $_POST['ad_id']!=''){
			if(!ctype_digit($_POST['ad_id']))
				throw new Exception('Invalid Id');
			$data['ad'] = $obj->getAdsById($_POST['ad_id']);
			if(empty($data['ad']))
				throw new Exception('Ad not found');
			if($data['ad']['posted_by']!=$user->guid)
				throw new Exception('Unauthorized');
		}
		else
			$data['ad'] = array();
		$data['categories'] = $obj->getAllCategories();
		echo json_encode($data);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>