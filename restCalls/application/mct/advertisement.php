<?php
	try{
		require_once('session.php');
		$obj = new MCTDAOFetchData;
		$settings = new UserDao;
		if(!isset($_POST['ads_id']) || $_POST['ads_id']==''){
			$obj->logger('1');
			throw new Exception('Oops! Something went wrong');
		}
		else if(!ctype_digit($_POST['ads_id'])){
			$obj->logger('2');
			throw new Exception('Invalid ID');
		}
		else if($_POST['ads_id']==''){
			$obj->logger('3');
			throw new Exception('Invalid ID');
		}
		$ad = $obj->getAdsById($_POST['ads_id']);
		if(empty($ad)){
			$obj->logger('4');
			throw new Exception('Ad not found');
		}
		$data['ad'] = $ad;
		$data['posted'] = ossn_user_by_guid($ad['posted_by']);
		$data['settings'] = $settings->getUserSettings($data['posted']->guid);
		if($data['settings']['show_email']==0)
			$data['posted']->email = "";
		if($data['settings']['show_phone']==0)
			$data['settings']['phone'] = "";
		$data['status'] = 200;
		echo json_encode($data);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>