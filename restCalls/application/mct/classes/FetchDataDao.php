<?php
	/*
		Author  : Suhail;
		Created : 26-Nov-2016;
	*/
	
	class MCTDAOFetchData{
		public function connect(){
			$conn = new mysqli(MY_HOST,MY_USER, MY_PWD, MY_DB);
			if ($conn->connect_errno) {
				$this->logger($conn->connect_error);
			}
			return $conn;
		}
		public function logger($log){
			$file = $_SERVER['DOCUMENT_ROOT']."/mtrade/log.txt";
			$logFile = fopen($file, 'a');
			$time = date('d-m-Y h:i:s A', time());
			$time= $this->getTimeStamp();
			$log.="       time:".$time."\n\n";
			fwrite($logFile, $log);
			fclose($logFile);
		}
		private function setTimeZone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		private function getTimeStamp(){
			$this->setTimeZone();
			$time = time();
			return $time;
		}
		public function getAllCategories(){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_categories ORDER BY id ASC";
			$stmt = $mysqli->prepare($sql);
			
			$stmt->execute();
			$stmt->bind_result($id, $category);
			$data = array();
			$i=0;
			while ($stmt->fetch()) {
				$data[$i]['id'] = $id;
				$data[$i++]['category'] = $category;
			}
			return $data;
		}
		public function getCategoryById($id){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_categories WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $id);			
			$stmt->execute();
			$stmt->bind_result($id, $category);
			$data = array();
			$stmt->fetch();
			$data['id'] = $id;
			$data['category'] = $category;
			return $data;
		}
		public function getCategoryByName($category){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_categories WHERE category=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('s', $category);			
			$stmt->execute();
			$stmt->bind_result($id, $category);
			$data = array();
			$i=0;
			while ($stmt->fetch()) {
				$data[$i]['id'] = $id;
				$data[$i++]['category'] = $category;
			}
			return $data;
		}
		public function getSubCategoryById($id){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_subcategories WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $id);			
			$stmt->execute();
			$stmt->bind_result($id, $subcategory, $category_id);
			$data = array();
			$stmt->fetch();
			$data['id'] = $id;
			$data['subcategory'] = $subcategory;
			$data['category_id'] = $category_id;
		
			return $data;
		}
		public function getSubCategoryByName($subcategory){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_subcategories WHERE subcategory=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('s', $subcategory);			
			$stmt->execute();
			$stmt->bind_result($id, $subcategory, $category_id);
			$data = array();
			$stmt->fetch();
			$data['id'] = $id;
			$data['subcategory'] = $subcategory;
			$data['category_id'] = $category_id;
		
			return $data;
		}
		public function getSubCategoryByCategoryId($category_id){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_subcategories WHERE category_id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $category_id);			
			$stmt->execute();
			$stmt->bind_result($id, $subcategory, $category_id);
			$data = array();
			$i=0;
			while ($stmt->fetch()) {
				$data[$i]['id'] = $id;
				$data[$i]['subcategory'] = $subcategory;
				$data[$i++]['category_id'] = $category_id;
			}
			return $data;
		}
		public function getAdsById($id){
			$mysqli = $this->connect();
			$sql = "SELECT 
					  mct_ads_details.*,
					  mct_ads.`cat_id` 
					FROM
					  mct_ads_details 
					  INNER JOIN mct_ads 
						ON mct_ads_details.`ads_id` = mct_ads.`id` 
					WHERE mct_ads_details.`ads_id` = ? ";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $id);			
			$stmt->execute();
			$stmt->bind_result($id,$ads_id,$title,$description,$country,$city,$address,$image,$video,$price,$posted_by,$category_id);
			$data = array();
			$i=0;
			$this->logger($stmt->error);
			while ($stmt->fetch()) {
				$data['id'] = $id;
				$data['ads_id'] = $ads_id;
				$data['title'] = $title;
				$data['description'] = $description;
				$data['country'] = $country;
				$data['city'] = $city;
				$data['address'] = $address;
				$data['image'] = $image;
				$data['video'] = $video;
				$data['price'] = $price;
				$data['posted_by'] = $posted_by;
				$data['cat_id'] = $category_id;
			}
			return $data;
		}
		private function getPostTime($params){
			$mysqli = $this->connect();
			$sql = "SELECT ".$params['time']." FROM mct_ads WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $params['id']);			
			$stmt->execute();
			$stmt->bind_result($time);
			$stmt->fetch();
			$this->logger(">>>>>>>time:>>>>>>>>".$params['id']."<<<<<<<<<<");
			$t = date('d-M-Y h:i A', $time);
			return $t;
			
		}
		public function getAdsByUser($guid){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_ads_details WHERE posted_by=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $guid);			
			$stmt->execute();
			$stmt->bind_result($id,$ads_id,$title,$description,$country,$city,$address,$image,$video,$price,$posted_by);
			$data = array();
			$i=0;
			$this->logger($stmt->error);
			$params['time']='created';
			while ($stmt->fetch()) {
				$data[$i]['id'] = $id;
				$data[$i]['ads_id'] = $ads_id;
				$params['id'] = $ads_id;
				$data[$i]['created']= $this->getPostTime($params);
				$data[$i]['title'] = $title;
				$data[$i]['description'] = $description;
				$data[$i]['country'] = $country;
				$data[$i]['city'] = $city;
				$data[$i]['address'] = $address;
				$data[$i]['image'] = $image;
				$data[$i]['video'] = $video;
				$data[$i]['price'] = $price;
				$data[$i++]['posted_by'] = $posted_by;
			}
			return $data;
		}
		public function getAllAds(){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_ads_details";
			$stmt = $mysqli->prepare($sql);			
			$stmt->execute();
			$stmt->bind_result($id,$ads_id,$title,$description,$country,$city,$address,$image,$video,$price,$posted_by);
			$data = array();
			$i=0;
			while ($stmt->fetch()) {
				$data[$i]['id'] = $id;
				$data[$i]['ads_id'] = $ads_id;
				$data[$i]['title'] = $title;
				$data[$i]['description'] = $description;
				$data[$i]['country'] = $country;
				$data[$i]['city'] = $city;
				$data[$i]['address'] = $address;
				$data[$i]['image'] = $image;
				$data[$i]['video'] = $video;
				$data[$i]['price'] = $price;
				$data[$i++]['posted_by'] = $posted_by;
			}
			return $data;
		}
		public function getAdsBySubCategory($subcat_id){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_ads_details WHERE ads_id IN(SELECT id FROM mct_ads WHERE subcat_id=?)";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param('i', $subcat_id);		
			$stmt->execute();
			$stmt->bind_result($id,$ads_id,$title,$description,$country,$city,$address,$image,$video,$price,$posted_by);
			$data = array();
			$i=0;
			while ($stmt->fetch()) {
				$data[$i]['id'] = $id;
				$data[$i]['ads_id'] = $ads_id;
				$data[$i]['title'] = $title;
				$data[$i]['description'] = $description;
				$data[$i]['country'] = $country;
				$data[$i]['city'] = $city;
				$data[$i]['address'] = $address;
				$data[$i]['image'] = $image;
				$data[$i]['video'] = $video;
				$data[$i]['price'] = $price;
				$data[$i++]['posted_by'] = $posted_by;
			}
			$this->logger(json_encode($data));
			return $data;
		}
		public function getAdsByCategory($category_id){
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_ads_details WHERE ads_id IN
						(SELECT id FROM mct_ads WHERE cat_id = ?)";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param('i', $category_id);		
			$stmt->execute();
			$stmt->bind_result($id,$ads_id,$title,$description,$country,$city,$address,$image,$video,$price,$posted_by);
			$data = array();
			$i=0;
			while ($stmt->fetch()) {
				$data[$i]['id'] = $id;
				$data[$i]['ads_id'] = $ads_id;
				$data[$i]['title'] = $title;
				$data[$i]['description'] = $description;
				$data[$i]['country'] = $country;
				$data[$i]['city'] = $city;
				$data[$i]['address'] = $address;
				$data[$i]['image'] = $image;
				$data[$i]['video'] = $video;
				$data[$i]['price'] = $price;
				$data[$i++]['posted_by'] = $posted_by;
			}
			return $data;
		}
		public function getAdsCountByCategory($id){
			$mysqli = $this->connect();
			$sql = "SELECT COUNT(*) AS COUNT FROM mct_ads WHERE cat_id=?";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param('i', $id);		
			$stmt->execute();
			$stmt->bind_result($count);
			$stmt->fetch();
			return $count;
		}
		public function getAdsCountBySubCategory($id){
			$mysqli = $this->connect();
			$sql = "SELECT COUNT(*) AS count FROM mct_ads_details WHERE ads_id IN
					(SELECT id FROM mct_ads WHERE subcat_id=?)";
			$stmt = $mysqli->prepare($sql);	
			$stmt->bind_param('i', $id);		
			$stmt->execute();
			$stmt->bind_result($count);
			$stmt->fetch();
			return $count;
		}
		public function isCategory($category){
			$mysqli = $this->connect();
			$flag = false;
			$categories = $this->getAllCategories();
			foreach($categories as $cat){
				if(strcasecmp($category, $cat['category'])==0){
					$flag=true;
					break;
				}
			}
			return $flag;
		}
		public function isSubCategory($params){
			$mysqli = $this->connect();
			$flag = false;
			$subcategories = $this->getSubCategoryByCategoryId($params['category_id']); 
			foreach($subcategories as $subcat){
				if(strcasecmp($params['subcategory'], $subcat['subcategory'])==0){
					$flag=true;
					break;
				}
			}
			return $flag;
		}
	}
?>