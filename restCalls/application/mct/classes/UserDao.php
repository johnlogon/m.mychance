<?php
	/*
		Author  : Suhail;
		Created : 30-Nov-2016;
	*/
	class UserDao{
		public function connect(){
			$conn = new mysqli(MY_HOST,MY_USER, MY_PWD, MY_DB);
			if ($conn->connect_errno) {
				$this->logger($conn->connect_error);
			}
			return $conn;
		}
		public function logger($log){
			$file = $_SERVER['DOCUMENT_ROOT']."/mtrade/log.txt";
			$logFile = fopen($file, 'a');
			$time = date('d-m-Y h:i:s A', time());
			$time= $this->getTimeStamp();
			$log.="       time:".$time."\n\n";
			fwrite($logFile, $log);
			fclose($logFile);
		}
		private function setTimeZone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		private function getTimeStamp(){
			$this->setTimeZone();
			$time = time();
			return $time;
		}
		public function getUserSettings($guid){
			if($guid=='')
				return false;
			$mysqli = $this->connect();
			$sql = "SELECT * FROM mct_user_settings WHERE guid=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $guid);
			$stmt->execute();
			$stmt->bind_result($id,$guid,$phone,$show_email,$show_phone);
			$data = array();
			$stmt->fetch();
			$this->logger($stmt->error);
			$this->logger($mysqli->error);
			$data['id'] = $id;
			$data['guid'] = $guid;
			$data['phone'] = $phone;
			$data['show_email'] = $show_email;
			$data['show_phone'] = $show_phone;
			return $data;
		}
		public function saveOrUpdateSettings($params){
			$mysqli = $this->connect();
			$data = $this->getUserSettings($params['guid']);
			if(empty($data['id'])){
				$sql = "INSERT INTO mct_user_settings (guid,phone,show_email,show_phone) VALUES(?,?,?,?)";
				$stmt = $mysqli->prepare($sql);
				$stmt->bind_param('isii',$params['guid'],$params['phone'],
								$params['show_email'],$params['show_phone']);
				if($stmt->execute())
					return true;
				return false;
			}
			$sql = "UPDATE mct_user_settings SET phone=?,show_email=?,show_phone=? WHERE guid=?";
				$stmt = $mysqli->prepare($sql);
				$stmt->bind_param('siii',$params['phone'],$params['show_email'],
										$params['show_phone'],$params['guid']);
				if($stmt->execute())
					return true;
				return false;
		}
	}
?>