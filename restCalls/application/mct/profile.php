<?php
	try{
		require_once('session.php');
		$user = new UserDao;
		$obj = new MCTDAOFetchData;
		$user_logged = ossn_loggedin_user();
		$data['settings'] = $user->getUserSettings($user_logged->guid);
		$data['user'] = $user_logged;
		$data['ads'] = $obj->getAdsByUser($user_logged->guid);
		echo json_encode($data);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>