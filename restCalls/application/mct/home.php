<?php
	try{
		require_once('session.php');
		$obj = new MCTDAOFetchData;
		$ads = $obj->getAllAds();
		$logged_in = ossn_loggedin_user()->guid;
		$response['ads'] = $ads;
		$response['logged_in'] = $logged_in;
		if(ossn_isAdminLoggedin()){
			/*$response['admin'] = '<li id="menu-item-1321" onClick="loadAdmin()" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1321 admin-menu"><a href="#">Admin</a></li>';*/
			$response['admin'] ='<td onClick="loadAdmin()" class="menu_table admin-menu"><i class="fa fa-user-md" aria-hidden="true"></i></td>';
		}
		echo json_encode($response);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>