<?php
	require_once('../loader.php');
	if(isset($_POST['session_id']) && $_POST['session_id']!=''){
		session_id($_POST['session_id']);
		session_start();
		if(!ossn_isLoggedin())
			throw new Exception('Action unauthorized');
	}
	else
		throw new Exception('Action unauthorized');
	if(isset($_POST['action'])){
		$obj = new MCTDAOFetchData;
		$save = new MCTDAOSaveUpdate;
		$settings = new UserDao;
		$action = $_POST['action'];
		if($action=='newad-subcat'){
			$value = $_POST['value'];
			$subcat = $obj->getSubCategoryByCategoryId($value);
			echo json_encode($subcat);
		}
		else if($action=="settings"){
			$_POST['guid'] = ossn_loggedin_user()->guid;
			$settings->saveOrUpdateSettings($_POST);
			$user_logged = ossn_loggedin_user();
			$data['settings'] = $settings->getUserSettings($user_logged->guid);
			$data['user'] = $user_logged;
			$data['ads'] = $obj->getAdsByUser($user_logged->guid);
			echo json_encode($data);
		}
		else if($action=="cat-update"){
			if(ossn_isAdminLoggedin()){
				$save->updateCategory($_POST);
				$obj = new MCTDAOFetchData;
				$categories = $obj->getAllCategories();
				$data['categories']=$categories;
				echo json_encode($data);
			}
		}
		else if($action=="cat-delete"){
			if(ossn_isAdminLoggedin()){
				$response = array();
				$count = $obj->getAdsCountByCategory($_POST['id']);
				if($count>0){
					$response['code']=201;
					$response['Message'] = 'Category cannot be deleted';
					echo json_encode($response);
				}
				else if($save->deleteCategoryById($_POST['id'])){
					$response['code'] = 200;
					$response['Message'] = 'Category deleted successfully';
					$obj = new MCTDAOFetchData;
					$categories = $obj->getAllCategories();
					$response['categories']=$categories;
					echo json_encode($response);
				}
				else{
					$response['code'] = 202;
					$response['Message'] = 'Oops! Something went wrong';
					echo json_encode($response);
					
				}
			}
		}
		else if($action=="cat-add"){
			if(ossn_isAdminLoggedin()){
				$response = array();
				if($obj->isCategory($_POST['category'])){
					$response['code']=201;
					$response['Message'] = 'Category already exist';
					echo json_encode($response);
				}
				else if($save->saveCategory($_POST['category'])){
					$response['code']=200;
					$response['Message'] = 'Category saved successfully';
					$obj = new MCTDAOFetchData;
					$categories = $obj->getAllCategories();
					$response['categories']=$categories;
					echo json_encode($response);
				}
				else {
					$response['code']=202;
					$response['Message'] = 'Oops! Something went wrong';
					echo json_encode($response);
				}
			}
		}
		else if($action=="subcat-view"){
			if(ossn_isAdminLoggedin()){
				$subcategories = $obj->getSubCategoryByCategoryId($_POST['id']);
				$data['subc'] =$subcategories;
				$data['cat'] = $obj->getCategoryById($_POST['id']);
				echo json_encode($data);
			}
		}
		else if($action=="subcat-update"){
			if(ossn_isAdminLoggedin()){
				if($save->updateSubCategory($_POST)){
					$response['code']=200;
					$response['Message'] = 'Category saved successfully';
					echo json_encode($response);
				}
				else{
					$response['code']=201;
					$response['Message'] = 'Oops! Something went wrong';
					echo json_encode($response);
				}
			}
		}
		else if($action=="subcat-delete"){
			if(ossn_isAdminLoggedin()){
				$count = $obj->getAdsCountBySubCategory($_POST['id']);
				if($count>0){
					$response['code']=201;
					$response['Message'] = 'Subcategory cannot be deleted';
					echo json_encode($response);
				}
				else{
					$flag = $save->deleteSubCategories(array($_POST['id']));
					if($flag){
						$response['code']=200;
						$response['Message'] = 'Subcategory deleted successfully';
						echo json_encode($response);
					}
					else{
						$response['code']=202;
						$response['Message'] = 'Oops! Something went wrong';
						echo json_encode($response);
					}
				}
			}
		}
		else if($action=="subcat-add"){
			if(ossn_isAdminLoggedin()){
				if($obj->isSubCategory($_POST)){
					$response['code']=201;
					$response['Message'] = 'Subcategory already exist';
					echo json_encode($response);
				}
				else if($save->saveSubCategory($_POST)){
					$response['code']=200;
					$response['Message'] = 'Subcategory saved successfully';
					$response['data'] = $obj->getSubCategoryByName($_POST['subcategory']);
					echo json_encode($response);
				}
				else{
					$response['code']=202;
					$response['Message'] = 'Oops! Something went wrong';
					echo json_encode($response);
				}
			}
		}
		else if($action=="ad-delete"){
			if($save->deleteAd($_POST['id'])){
				$response['code'] = 200;
				echo json_encode($response);
		    }
			else{
				$response['code'] = 201;
				echo json_encode($response);
			}
		}
		else if($action=="contact-poster"){
			if(empty($_POST['message_to']))
				echo 0;
			else{
				$message_to = $_POST['message_to'];
				$message = $_POST['message'];
				$message_from = ossn_loggedin_user()->guid;
				$msg = new OssnMessages;
				if($msg->send($message_from, $message_to, $message))
					echo 1;
				else
					echo 0;
			}
		}
		else if($action=="newad-add"){
			$save = new MCTDAOSaveUpdate;
			if(isset($_POST['ads_id']) && $_POST['ads_id']!=''){
				$response = $save->updateAds($_POST);
			}
			else{
				$response=$save->saveAds($_POST);
			}
			if($response['error']!=0){
				echo json_encode($response);
				die();
			}
			$ad = $obj->getAdsById($response['ads_id']);
			if(empty($ad))
				throw new Exception('Ad not found');
			$data['error'] = 0;
			$data['ad'] = $ad;
			$data['posted'] = ossn_user_by_guid($ad['posted_by']);
			$data['settings'] = $settings->getUserSettings($data['posted']->guid);
			echo json_encode($data);
			}
	}
?>