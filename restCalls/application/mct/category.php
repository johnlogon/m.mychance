<?php
	try{
		require_once('session.php');
		$obj = new MCTDAOFetchData;
		$user = ossn_loggedin_user();
		$data = array();
		if(!isset($_POST['cat_id']))
			throw new Exception('Id not present');
		else if(!ctype_digit($_POST['cat_id']))
			throw new Exception('Invalid ID');
		$userlogged = ossn_loggedin_user();
		$category = $obj->getCategoryById($_POST['cat_id']);
		$data['category'] = $category['category'];
		$data['ads_count'] = $obj->getAdsCountByCategory($_POST['cat_id']);
		$ads = $obj->getAdsByCategory($_POST['cat_id']);
		$data['ads'] = $ads;
		$data['logged_in'] = ossn_loggedin_user()->guid;
		if(ossn_isAdminLoggedin())
			$data['isAdmin'] = true;
		echo json_encode($data);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>