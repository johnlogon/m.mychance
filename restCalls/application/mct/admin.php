<?php
	try{
		require_once('session.php');
		if(!ossn_isAdminLoggedin()){
			throw new Exception("Unauthorized Action");
		}
		$obj = new MCTDAOFetchData;
		$categories = $obj->getAllCategories();
		$data['categories']=$categories;
		echo json_encode($data);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>