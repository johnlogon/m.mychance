<?php
	try{
		require_once('session.php');
		$obj = new MCTDAOFetchData;
		if(!isset($_POST['cat_id']) || empty($_POST['cat_id']))
			throw new Exception('Invalid request');
		$ads = $obj->getAdsByCategory($_POST['cat_id']);
		$category = $obj->getCategoryById($_POST['cat_id']);
		$logged_in = ossn_loggedin_user()->guid;
		$response['category'] = $category;
		$response['ads'] = $ads;
		$response['logged_in'] = $logged_in;
		echo json_encode($response);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>