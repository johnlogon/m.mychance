<?php
	try{
		require_once('session.php');
		$obj = new MCTDAOFetchData;
		if(!isset($_POST['subcategory_id']) || $_POST['subcategory_id']=='')
			throw new Exception('Oops! Something went wrong');
		$subcategory = $obj->getSubCategoryById($_POST['subcategory_id']);
		$count = $obj->getAdsCountBySubCategory($_POST['subcategory_id']);
		$data['subcategory'] = $subcategory['subcategory'];
		$data['count'] = $count;
		$ads = $obj->getAdsBySubCategory($_POST['subcategory_id']);
		$data['ads'] = $ads;
		if(ossn_isAdminLoggedin())
			$data['isAdmin'] = true;
		echo json_encode($data);
	}
	catch(Exception $e){
		$response['status']=400;
		$response['message'] = $e->getMessage();
		echo json_encode($response);
	}
?>