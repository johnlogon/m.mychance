<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('start.php');

$query = input('q');
$type = input('type');
$title = ossn_print("search:result", array($query));
if (empty($type)) {
	$params['type'] = 'users';
} else {
	$params['type'] = $type;
}
if($params['type']=='users'){
	$type = $params['type'];
	if (ossn_is_hook('search', "type:{$type}")) {
		$contents['contents'] = ossn_call_hook('search', "type:{$type}", array('q' => input('q')));
	}
	$contents = array('content' => ossn_plugin_view('search/pages/search', $contents),);
	$content = ossn_set_page_layout('search', $contents);
	echo $content;
	die();
}
else if($params['type']=='groups'){
	$Pagination = new OssnPagination;
	$groups     = new OssnGroup;
	$data       = $groups->searchGroups(input('q'));
	$Pagination->setItem($data);
	$group['groups'] = $Pagination->getItem();
	$search          = ossn_plugin_view('groups/search/view', $group);
	$search .= $Pagination->pagination();
	if(empty($data)) {
			echo ossn_print('ossn:search:no:result');
			die();
	}
	echo $search;
}
?>