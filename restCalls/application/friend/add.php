<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');


if (ossn_add_friend(ossn_loggedin_user()->guid, input('user'))) {
	echo json_encode(array(
		'type' => 1,
		'text' => ossn_print('ossn:notification:are:friends'),
	));
   
} 
else {
	echo json_encode(array(
		'type' => 1,
		'text' => ossn_print('ossn:add:friend:error'),
	));
}
