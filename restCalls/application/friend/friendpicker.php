<?php
	header('Access-Control-Allow-Origin: *');
	header("Access-Control-Allow-Headers: * ");
	header('Access-Control-Allow-Methods: *');
	
	session_id($_POST['session_id']);
	session_start();
	
	require_once('../start.php');

	header('Content-Type: application/json');
	
	$user    = new OssnUser;
	$q = input('q');
	$friends = $user->getFriends(ossn_loggedin_user()->guid);
	if(!$friends) {
			return false;
	}
	$usera = array();
	foreach($friends as $users) {
		if (stripos($users->first_name, $q) !== false) {
			$p['first_name'] = $users->first_name;
			$p['last_name']  = $users->last_name;
			$p['username']   = $users->username;
			$p['id']         = $users->guid;
			$usera[]         = $p;	
		}
	}
	echo json_encode($usera);
?>