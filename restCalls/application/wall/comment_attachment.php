<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

header('Content-Type: application/json');
if(isset($_FILES['file']['tmp_name']) && ($_FILES['file']['error'] == UPLOAD_ERR_OK && $_FILES['file']['size'] !== 0) && ossn_isLoggedin()) {
		if(preg_match("/image/i", $_FILES['file']['type'])) {
				$file    = $_FILES['file']['tmp_name'];
				$unique  = time() . '-' . substr(md5(time()), 0, 6) . '.jpg';
				$newfile = ossn_get_userdata("tmp/photos/{$unique}");
				$dir     = ossn_get_userdata("tmp/photos/");
				if(!is_dir($dir)) {
						mkdir($dir, 0755, true);
				}
				if(move_uploaded_file($file, $newfile)) {
						$file = base64_encode(ossn_string_encrypt($newfile));
						echo json_encode(array(
								'file' => base64_encode($file),
								'type' => 1
						));
						exit;
				}
		}
}
echo json_encode(array(
		'type' => 0
));
?>