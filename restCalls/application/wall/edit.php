<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$post = ossn_get_object($_POST['p_guid']);
						
if(!$post) {
		header("HTTP/1.0 404 Not Found");
}
$user = ossn_loggedin_user();
if($post->poster_guid == $user->guid || $user->canModerate()) {
		$params = array(
				'title' => ossn_print('edit'),
				'class' => "edit_form",
				'contents' => ossn_view_form('post/edit', array(
						'action' => '#',
						'data-action' => 'restCalls/application/wall/post/edit.php',
						'component' => 'OssnWall',
						'params' => array(
								'post' => $post
						)
				), false),
				'callback' => '#ossn-post-edit-save'
		);
		echo ossn_plugin_view('output/ossnbox', $params);
}
?>