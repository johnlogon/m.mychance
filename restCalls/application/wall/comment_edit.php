<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');


$comment = ossn_get_annotation($_POST['guid']);
if(false) {
		ossn_error_page();
}
if(!$comment) {
		header("HTTP/1.0 404 Not Found");
}
$user = ossn_loggedin_user();
if($comment->owner_guid == $user->guid || $user->canModerate()) {
		$params = array(
				'title' => ossn_print('edit'),
				'contents' => ossn_view_form('comment/edit', array(
						'action' => '#',
						'data-action' => 'restCalls/application/wall/post/comment_edit.php',
						'component' => 'OssnComments',
						'params' => array(
								'comment' => $comment
						)
				), false),
				'callback' => '#ossn-comment-edit-save'
		);
		echo ossn_plugin_view('output/ossnbox', $params);
}
?>