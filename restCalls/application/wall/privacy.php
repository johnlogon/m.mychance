<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../start.php');

$params = array(
		'title' => ossn_print('privacy'),
		'contents' => ossn_plugin_view('wall/privacy'),
		'callback' => '#ossn-wall-privacy'
);
echo ossn_plugin_view('output/ossnbox', $params);

?>