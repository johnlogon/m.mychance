<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');
//init ossnwall
$OssnWall = new OssnWall;

//poster guid and owner guid is same as user is posting on its own wall
$OssnWall->owner_guid = ossn_loggedin_user()->guid;
$OssnWall->poster_guid = ossn_loggedin_user()->guid;

//check if users is not posting on its own wall then change wallowner
$owner = input('wallowner');
if (isset($owner) && !empty($owner)) {
    $OssnWall->owner_guid = $owner;
}

//walltype is user
$OssnWall->name = 'user';


//getting some inputs that are required for wall post
$post = input('post');
$friends = input('friendsh');
$location = input('location');
$privacy = input('privacy');

//validate wall privacy 
$privacy = ossn_access_id_str($privacy);
if (!empty($privacy)) {
    $access = input('privacy');
} else {
    $access = OSSN_FRIENDS;
}
if ($OssnWall->Post($post, $friends, $location, $access)) {
	if(true) {
		$guid = $OssnWall->getObjectId();
		$get  = $OssnWall->GetPost($guid);
		if($get) {
			$get = ossn_wallpost_to_item($get);
			echo  ossn_wall_view_template($get);
		}
	}
} else {
    echo 0;
}
