<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$guid    = input('guid');
$text    = input('comment');
$comment = ossn_get_annotation($guid);

if(!$comment || empty($text)) {
	echo ossn_trigger_message(ossn_print('comment:edit:failed'), 'error');
	die();
}
//editing, then saving a comment gives warning #685

$comment->data	= new stdClass;
if($comment->type == 'comments:entity') {
		$comment->data->{'comments:entity'} = $text;
} elseif($comment->type == 'comments:post') {
		$comment->data->{'comments:post'} = $text;
}
$user = ossn_loggedin_user();

$data = array();
if(($comment->owner_guid == $user->guid || $user->canModerate()) && $comment->save()) {
		$data['text'] = $text;
		$data['type'] = "comment";;
		$params               = array();
		$params['text']       = $text;
		$params['annotation'] = $comment;
		ossn_trigger_callback('comment', 'edited', $params);
		
		$data['message'] = ossn_print('comment:edit:success');
		echo json_encode($data);
} else {
		$data['err'] = ossn_print('comment:edit:failed');
		echo json_encode($data);
}