<?php

header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../../start.php');


$OssnLikes = new OssnLikes;
$anotation = input('post');
$entity = input('entity');
if (!empty($anotation)) {
    $subject = $anotation;
    $type = 'post';
}
if (!empty($entity)) {
    $subject = $entity;
    $type = 'entity';

}

if ($OssnLikes->UnLike($subject, ossn_loggedin_user()->guid, $type)) {
	header('Content-Type: application/json');
	echo json_encode(array(
			'done' => 1,
			'button' => '<b style="color:black"><i class="fa fa-hand-o-right"aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></b>',
		));   
} else {
	 header('Content-Type: application/json');
	echo json_encode(array(
			'done' => 0,
			'button' => '<b style="color:#337ab7"><i class="fa fa-hand-o-right"aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></b>',
		));
}

exit;
