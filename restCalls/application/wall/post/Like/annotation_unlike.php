<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../../start.php');

$OssnLikes = new OssnLikes;
$anotation = input('annotation');
if ($OssnLikes->UnLike($anotation, ossn_loggedin_user()->guid, 'annotation')) {
   header('Content-Type: application/json');
	echo json_encode(array(
		'done' => 1,
		'button' => 'Like',
	));	
} else {
	header('Content-Type: application/json');
	echo json_encode(array(
			'done' => 0,
			'button' => 'Unlike',
		));
}

exit;