<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');

$ossnwall = new OssnWall;
$id = input('post');
$post = $ossnwall->GetPost($id);
if ($post->type == 'user' && !ossn_isAdminLoggedin()) {
    if ($post->poster_guid !== ossn_loggedin_user()->guid && $post->owner_guid !== ossn_loggedin_user()->guid) {
    	echo 0;
		exit;
    }
}
if ($post->type == 'group' && !ossn_isAdminLoggedin()) {
    $group = new OssnGroup;
    $group = $group->getGroup($post->owner_guid);
	//lastchange group admins are unable to delete member posting on group wall #171
	// change or operator to and
    if (($post->poster_guid !== ossn_loggedin_user()->guid) && (ossn_loggedin_user()->guid !== $group->owner_guid)) {
      echo 0;
      exit;
    }
}
if ($ossnwall->deletePost($id)) {
        echo 1;
} else {
   echo 0;
}
