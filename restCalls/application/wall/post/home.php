<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

session_id($_POST['session_id']);
session_start();

require_once('../../start.php');
//init ossnwall
$OssnWall = new OssnWall;
$data = array();
//poster guid and owner guid is same as user is posting on home page on its own wall
$OssnWall->owner_guid  = ossn_loggedin_user()->guid; //input('guid');//ossn_loggedin_user()->guid;
$OssnWall->poster_guid = ossn_loggedin_user()->guid; // input('guid'); //ossn_loggedin_user()->guid;

//check if owner guid is zero then exit
if ($OssnWall->owner_guid == 0 || $OssnWall->poster_guid == 0) {
    echo ossn_print('post:create:error');
    //redirect(REF);
}

//getting some inputs that are required for wall post
$post     = input('post');
$friends  = input('friendsh');
$location = input('location');
$privacy  = input('privacy');

//validate wall privacy 
$privacy = ossn_access_id_str($privacy);
$access  = '';
if (!empty($privacy)) {
    $access = input('privacy');
}

if ($OssnWall->Post($post, $friends, $location, $access)) {
    $guid = $OssnWall->getObjectId();
    $get  = $OssnWall->GetPost($guid);
    if ($get) {
        $get              = ossn_wallpost_to_item($get);
        $template = ossn_wall_view_template($get);
        
    }
    $message = ossn_print('post:created');
	echo $template;
} else {
    $data['errmessage'] = ossn_print('post:create:error');
	echo 0;
}
