<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: * ");
header('Access-Control-Allow-Methods: *');

require_once('../../start.php');
if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}
else{
	echo 0;
	die();
}

$guid = input('guid');
$post = input('post');

$object = ossn_get_object($guid);
$user   = ossn_loggedin_user();
if(!$object || empty($post)) {
	echo ossn_print('ossn:wall:post:save:error');
}

$post = htmlspecialchars($post, ENT_QUOTES, 'UTF-8');
$json = html_entity_decode($object->description);

$data         = json_decode($json, true);
$data['post'] = $post;

$data                = json_encode($data, JSON_UNESCAPED_UNICODE);
$object->description = $data;
$dat = array();
if(($object->poster_guid == $user->guid || $user->canModerate()) && $object->save()) {
		$params           = array();
		$params['text']   = $post;
		$params['object'] = $object;
		
		$dat['type'] = "wall";
		$text = json_decode(html_entity_decode($object->description));
		$te = ossn_restore_new_lines($text->post, true);
		$dat['text'] = $te;
		$dat['message'] = ossn_print('ossn:wall:post:saved');
		echo json_encode($dat);
} else {
		$dat['err'] =  ossn_print('ossn:wall:post:save:error');
		echo json_encode($dat);
}
  