<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	
	require_once("../system/start.php");
	
	$get                            = new OssnNotifications;
	$unread                         = ossn_call_hook('list', 'notification:unread', true);
	$notifications['notifications'] = $get->get($_POST['userId'], $unread);
	$notifications['seeall']        = ossn_site_url("notifications/all");
	$html = "";
	foreach($notifications as $not){
		foreach($not as $no)
			$html = $html . $no;
	}
	echo $html;
?>
	