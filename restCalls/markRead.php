<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST');

require_once("../system/start.php");
$userId       = $_POST['userId'];
$user         = ossn_user_by_guid($userId);
$notification = new OssnNotifications;
if ($notification->clearAll($user->guid)) {
    echo 1;
} else {
    echo 0;
}

?>