<?php
	try{
		require_once("db_connect.php");
		require_once("error_log.php");
		
		if (!$db) {
			error_write("Connection failed: " . mysqli_connect_error());
			die();
		}
		
		$sql= "CREATE TABLE IF NOT EXISTS ossn_deviceids_socialmedia (
			id INT(11) NOT NULL AUTO_INCREMENT,
			userid INT(11) NOT NULL,
			username varchar(50) NOT NULL,
			device_id TEXT,
			PRIMARY KEY (id))ENGINE=MyISAM DEFAULT CHARSET=utf8";
		if(!mysqli_query($db,$sql)){
			error_write("Error creating table ossn_deviceids_socialmedia: " . mysqli_error($db));
		}
	}catch(Exception $e){
		error_write($e->getMessage());
	}
?>