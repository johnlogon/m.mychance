<?php
	require_once("db_connect.php");
	require_once("session.php");
	
	$id=$_GET['id'];
		$row_game_contents;
		$sql_game_contents;
		$result_game_contents;
		$count_contents;
		
		$row_game_likes;
		$sql_game_likes;
		$result_game_likes;
		$count_likes;
		
		$row_game_comments;
		$sql_game_comments;
		$result_game_comments;
		$count_comments;
		
	if($_GET['id']!=''){
		$sql_game_contents="SELECT * FROM (SELECT * FROM ossn_game_post WHERE gameid=".$id. ") ossn_game_post ORDER BY likes DESC LIMIT 0,3";
		$result_game_contents=mysqli_query($db, $sql_game_contents);
		if(!$result_game_contents){
			echo "Error Fetching Games Contents:". mysqli_error($db);
		}
		$count_contents=mysqli_num_rows($result_game_contents);
	}
	
	function time_elapsed_string($ptime){
    	$etime = time() - $ptime;

    	if ($etime < 1){
        	return '0 seconds';
    	}

    	$a = array( 365 * 24 * 60 * 60  =>  'year',
                 30 * 24 * 60 * 60  =>  'month',
                      24 * 60 * 60  =>  'day',
                           60 * 60  =>  'hour',
                                60  =>  'minute',
                                 1  =>  'second'
                );
   		$a_plural = array( 'year'   => 'years',
                       'month'  => 'months',
                       'day'    => 'days',
                       'hour'   => 'hours',
                       'minute' => 'minutes',
                       'second' => 'seconds'
                );

    	foreach ($a as $secs => $str){
        $d = $etime / $secs;
        if ($d >= 1)
        {
            $r = round($d);
            return $r . ' ' . ($r > 1 ? $a_plural[$str] : $str) . ' ago';
        }
    }
}

?>