<?php
	require_once("session.php");
	require_once("game_post_upload.php");
	if(!isset($_GET['id']) || $_GET['id']==''){
		header('location:'. ossn_site_url("reality-show/gameexplore.php?id=0&errMsg=Invalid Request"));
	}
	if($user==NULL){
	   die();
	}
?>

<!DOCTYPE html>
<html class='v2' dir='ltr' xmlns='http://www.w3.org/1999/xhtml' xmlns:b='http://www.google.com/2005/gml/b' xmlns:data='http://www.google.com/2005/gml/data' xmlns:expr='http://www.google.com/2005/gml/expr'>

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>
    <meta charset='utf-8' />
    <meta content='width=device-width, initial-scale=1, maximum-scale=1' name='viewport' />
    <title>MyChance</title>
    <link href='favicon.ico' rel='icon' type='image/x-icon' />

    <!-- [ CSS ] -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
    <link href='css/font-awesome.min.css' rel='stylesheet' />
	<link href='css/style.css' rel='stylesheet' />
	<link href='css/1535467126-widget_css_2_bundle.css' rel='stylesheet' type='text/css' />
    <link rel="stylesheet" type="text/css" href="css/game.css">
    <link rel="stylesheet" type="text/css" href="css/ring.css">
	<style>
	.modal-wide .modal-dialog {
		width: 65%;
	}
  </style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
	<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
	<!--<script language=JavaScript src="js/jquery-1.12.3.min.js"></script>-->
    <script language=JavaScript src="js/common.js"></script>
	<script language=JavaScript src="js/malayalam.js"></script>
    <script language=JavaScript src="js/game.js"></script>
    <script type="text/javascript">
		
		$(document).ready(function() {
			setTimeout(function(){
				$('#Message').hide(500);	
			}, 4000);
			//alert(1);
			if (busy == false) {
 				busy = true;
  				displayRecords(limit, offset);
			}
			$("#postForm").submit(function(event){
				if($("#postText").val()=="" && $("#file").val()==""){
					event.preventDefault();
				}
				if($("#file").val()!=""){
					if(($("#file")[0].files[0].size)>16000000){
						$("#fileuploadname").html("File size should not exceed 16 MB").css('color', 'red');
						event.preventDefault();
					}
					else{
						$(".errspan-upload").fadeIn("slow");
					}
				}
			});
			$("#adsForm").submit(function(){
				if($("#adstitle").val()=="" || $("#adsDesc").val()=="" || $("#adslink").val()=="" || $("#adsfile").val()=="" )
					return false;
			})
			
		});
		$(document).ready(function(){
			showAds();
		});
 
		$(document).on("click", ".flplayer", function(e) {
    		if (this.paused == false) {
				  this.pause();
				  //alert('music paused');
			} 
		});
		$(document).on("click", "body", function(e) {
    		if (this.paused == false) {
				  this.pause();
				  //alert('music paused');
			} 
		});
		
		$(document).ready(function() {
 			$(window).scroll(function() {
         		if ($(window).scrollTop() + $(window).height() > $("#results").height() && !busy && again) {
           			busy = true;
            		offset = limit + offset;
 					displayRecords(limit, offset);
					console.log("called");
 				}
			});
			
 
		});
		$(document).ready(function(){
			$('[data-toggle="tooltip"]').tooltip();   
		});
		$(document).on("keypress", ".cmnt", function(e) {
    		if (e.which == 13) {
        		postThisComment($(this).attr('id'));
				return false;
     		}
		});
		$(document).on('click', '.one', function(e) { 
		//alert('one');
			if($("#jkl").val()!='neva'){
				openSplash(); 
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				addAward(id, postid, award, gameid);
				closeSplash();
			}
			else{
				openSplash(); 
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				checkAward(id, postid, award, gameid);
				closeSplash();
			}
			
    	});
		$(document).on('click', '.two', function(e) {  
			//alert('two');
			if($("#jkl").val()!='neva'){ 
				openSplash();
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				addAward(id, postid, award, gameid);
				closeSplash();
			}
			else{
				openSplash(); 
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				checkAward(id, postid, award, gameid);
				closeSplash();
			}
    	});
		$(document).on('click', '.three', function(e) { 
			//alert('three');
			if($("#jkl").val()!='neva'){ 
				openSplash();
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				addAward(id, postid, award, gameid);
				closeSplash();
			}
			else{
				openSplash(); 
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				checkAward(id, postid, award, gameid);
				closeSplash();
			}
		});
		$(document).on('click', '.four', function(e) { 
			//alert('four'); 
			if($("#jkl").val()!='neva'){ 
				openSplash();
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				addAward(id, postid, award, gameid);
				closeSplash();
			}
			else{
				openSplash(); 
				var id=$(this).attr('id');
				var award = id.substr(id.length - 1);
				var gameid=$("#gameid").val();
				var postid=id.substr(0, id.length - 1);
				checkAward(id, postid, award, gameid);
				closeSplash();
			}
		});
		 $(document).on('click', '.rating .stars', function (event) {
			//alert("clk");
			 if($("#jkl").val()!='neva'){
				 //alert("clicked");
				 var rate=$(this).val();
				 var postid=($(this).attr('id'));
				 var gameid=$("#gameid").val();
				 postid=postid.substr(5, postid.length-1);
				 //alert("gameid: "+gameid+" , postid: "+postid+" , rate: "+rate);
				 request = $.ajax({
							url: "rating.php",
							type: "post",
							data: {rate:rate, gameid:gameid, postid:postid},
							success: function(d){
								//alert(d);
								/*if(d==0)
									alert("success");
							else
								alert(d);*/
							},
							error: function(error){
								alert(error);
							}
				});
			 }
			
		 });
		
		var busy = false;
		var limit = 10;
		var offset = 0;
		var again = true;

		function displayRecords( lim, off) {
			var gameid=$("#gameid").val();
			var userid=$("#loggedinuserid").val();
			//alert(gameid);
        	$.ajax({
          		type: "GET",
          		async: false,
         		url: "getposts.php",
          		data: "limit=" + lim + "&offset=" + off + "&gameid=" + gameid + "&userid=" + userid, 
          		cache: false,
          		beforeSend: function() {
           			$("#loader_message").html("").hide();
            		$('#loader_image').show();
          		},
          		success: function(html) {
					//alert(html);
					if(html=="s_err"){
						location.href="logout.php";
					}
					else{
						$("#results").append(html);
						$('#loader_image').hide();
						if (html == "") {
							again = false;
							$("#loader_message").html('<button data-atr="nodata" class="btn btn-default" type="button">No more records.</button>').show()
						} else {
						$("#loader_message").html('<button class="btn btn-default" type="button">Loading please wait...</button>').show();
						}
						window.busy = false;
						//showAds();
					}
				}
        	});
		}
		function showAds(){
			//alert("showAds");
			$.ajax({
          		type: "GET",
          		async: false,
         		url: "getads.php",
          		cache: false,
          		
          		success: function(html) {
					//alert(html);
					if(html=="s_err"){
						location.href="logout.php";
					}
					else{
						$(".ads-result").append(html);
						
					}
				}
        	});
		}
    </script>
    <script type='text/javascript'>
        //<![CDATA[
        function bp_thumbnail_resize(e, t) {
            var n = 260;
            var r = 170;
            image_tag = '<img width="' + n + '" height="' + r + '" src="' + e.replace("s72-c/index.html", "/w" + n + "-h" + r + "-c/") + '" alt="' + t.replace(/"/g, "") + '" title="' + t.replace(/"/g, "") + '"/>';
            if (t != "") return image_tag;
            else return ""
        }
        //]]>
    </script>
    <script src='../ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'></script>
    <script type='text/javascript'>
        $(function() {
            $(".tabs-1").mtabs()
        })
    </script>
    <script type="text/javascript">
        var a = "&m=1",
            d = "(^|&)m=",
            e = "?",
            f = "?m=1";

        function g() {
            var b = window.location.href,
                c = b.split(e);
            switch (c.length) {
                case 1:
                    return b + f;
                case 2:
                    return 0 <= c[1].search(d) ? null : b + a;
                default:
                    return null
            }
        }
        var h = navigator.userAgent;
        if (-1 != h.indexOf("Mobile") && -1 != h.indexOf("WebKit") && -1 == h.indexOf("iPad") || -1 != h.indexOf("Opera Mini") || -1 != h.indexOf("IEMobile")) {
            var k = g();
            k && window.location.replace(k)
        };
    </script>
    <script type="text/javascript">
        if (window.jstiming) window.jstiming.load.tick('headEnd');
    </script>
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
 
  <!--<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
</head>

<body class='loading' style="">
<input type="hidden" id="site_url" value="<?php echo ossn_site_url(); ?>">
<nav class="navbar navbar-default navbar-fixed-top" style="display:block;background-color:#fff">
      <div class="container">
        <div class="navbar-header">
         <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">-->
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="padding:0px 0px;"><img src="http://demo.snogol.net/mychance.in/m.mychance/reality-show/logo_new.png" style="margin-top:0px;width:225px;height:48px"/></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav hai" style="background-color:#fff;margin-top:0px;float:right; visibility:hidden">       
            <li style="margin:0px;border-bottom:solid #ddd 1px;background-color:#fff;font-size:14px;"><a href="#">Social Inspiration</a></li>
            <li style="margin:0px;background-color:#fff;border-bottom:solid 1px #ddd;font-size:14px;"><a href="#">Logout</a></li>             
          </ul> 
        </div><!--/.nav-collapse -->
      </div>
</nav>


    <div id='outer-wrapper' style='margin-top:50px;'>
        <div id='header-wrapper' style='display:none;'>
			<div class='logout-wrapper'>
				<a href="<?php echo $ossn_site_url; ?>">
                	<button type="button" class="btn btn-default btn-sm">
						<img src="img/s.png" width="15px" height="15px"> Home
					</button>
                </a>
				<a href="logout.php">
					<button type="button" class="btn btn-default btn-sm">
						<span class="glyphicon glyphicon-log-out"></span> Log out
					</button>
				</a>
			</div>
            <div class='header section section' id='header' style='margin:0px;'>
                <div class='widget Header' data-version='1' id='Header1'>
					<a href="games.php?nads=f&&ngm=f">
						<div id='header-inner'>
							<div class='descriptionwrapper'>
								<img src="img/new_logo.png" />
							</div>
						</div>
					</a>
                </div>
            </div>
            <div class='header-right section section' id='header-right'></div>
        </div>
        <div class='clear'></div>
        <nav>
           
        </nav>
        <div class='clear'></div>
        <div id='content-wrapper >
            <div class='banner section section' id='banner'></div>
            <div class='clear'></div>
            <?php 
				if(isset($_GET['errMsg'])){
					$class='danger';
					$msg = $_GET['errMsg'];
				}
				else{
					$class='success';
					$msg = $_GET['scssMsg'];
				}
			?>
            <?php 
            	if(isset($_GET['errMsg']) || isset($_GET['scssMsg'])){
			?>
                <div class="alert alert-<?php echo $class ?>" id="Message">
                   <strong><?php echo $msg ?></strong>
                  <i class="fa fa-times pull-right" aria-hidden="true" onClick="closeMessage()" style="cursor:pointer"></i>
                </div>
           <?php 
				}
		   ?>
                <div class='post-upload-wrapper' align="center" >
                    <form action="http://www.mychance.in/reality-show/post_upload_m_mychance.php" method="post" id="postForm" enctype="multipart/form-data">
						<span class="mal-eng" >
                        <input type="radio" name="lang" id="engllish" checked="checked" value="eng">English
                        <input type="radio" name="lang" id="malayalam" value="mal">Malayalam <br>
						</span>
                        <textarea style="margin-top:15px;margin-bottom:7px;" class="form-control" name="text" id="postText" rows="5" placeholder="My dreams"  charset="utf-8" 
                        	onKeyDown="typeKeydown(event)" onKeyPress="typeKeypress(event)" style="resize:none"></textarea>
                            <i class="icon-camera errspan csr" aria-hidden="true" onClick="clickFile()"  ></i>
							<img src="img/358.gif" class='errspan-upload' style="display:none;">
                            <span id="fileuploadname" style="float:right"></span>
                        <input type="file" name="file" id="file" onChange="alertfilename()" style="display:none">
                        <input type="submit" class="btn btn-primary" name="submitPostForm" value="Create"/>
                        <input type="hidden" name="userid" value="<?php echo $user->guid ?>">
                        <input type="hidden" name="gameid" value="<?php echo $_GET['id']; ?>">
                         <input type="hidden" name="fullname" value="<?php echo $user->fullname ?>">
                    </form>
 
                </div>
				
                
            <div id='post-wrapper'>
                <div class='post-inner'>
                    <div class='main section' id='main'>
                        <div class='widget HTML' data-version='1' id='HTML900'>
                            <div class='ads-in-post'>
                            </div>
                        </div>
                        <div class='widget Blog' data-version='1' id='Blog1'>
                        	<input type="hidden" id="gameid" value="<?php echo $_GET['id']; ?>">
                            <input type="hidden" id="loggedinuserid" value="<?php echo $user->guid; ?>">
                            <input type="hidden" id="jkl" value="<?php if($user->type!='admin') echo "neva"; ?>">
                        	<div id="results">
                           	
                        	</div>
                           <div id="loader_image"><img src="img/load.gif" alt="" width="24" height="24"> Loading...please wait</div>
                           <div align="center" id="loader_message"></div>
						   
						   <!-- Modal -->
						   <button type="button" id="open-popup" style="display:none" data-toggle="modal" data-target="#myModal"></button>
							<div class="modal fade modal-wide" id="myModal" role="dialog" style="">
								<div class="modal-dialog">
								
								  <!-- Modal content-->
									<div class="modal-content" style="height:98vh" >
									
										<div class="modal-body" style="height:100%;padding: 2px;" >
											<div class="pop-content" style="height:100%;">
												
											</div>
										</div>
									</div>
								  
								</div>
							</div>
						   
                            <div class='pagenav'>
                                <script type='text/javascript'>
                                    var pageNaviConf = {
                                        perPage: 6,
                                        numPages: 5,
                                        firstText: "First",
                                        lastText: "Last",
                                        nextText: "Next",
                                        prevText: "Prev"
                                    }
                                </script>
                                <script type='text/javascript'>
                                    //<![CDATA[
                                    function pageNavi(o) {
                                        var m = location.href,
                                            l = m.indexOf("http://www.revoltify.co.vu/search/label/") != -1,
                                            a = l ? m.substr(m.indexOf("http://www.revoltify.co.vu/search/label/") + 14, m.length) : "";
                                        a = a.indexOf("?") != -1 ? a.substr(0, a.indexOf("?")) : a;
                                        var g = l ? "/search/label/" + a + "?updated-max=" : "/search?updated-max=",
                                            k = o.feed.entry.length,
                                            e = Math.ceil(k / pageNaviConf.perPage);
                                        if (e <= 1) {
                                            return
                                        }
                                        var n = 1,
                                            h = [""];
                                        l ? h.push("/search/label/" + a + "?max-results=" + pageNaviConf.perPage) : h.push("/?max-results=" + pageNaviConf.perPage);
                                        for (var d = 2; d <= e; d++) {
                                            var c = (d - 1) * pageNaviConf.perPage - 1,
                                                b = o.feed.entry[c].published.$t,
                                                f = b.substring(0, 19) + b.substring(23, 29);
                                            f = encodeURIComponent(f);
                                            if (m.indexOf(f) != -1) {
                                                n = d
                                            }
                                            h.push(g + f + "&max-results=" + pageNaviConf.perPage)
                                        }
                                        pageNavi.show(h, n, e)
                                    }
                                    pageNavi.show = function(f, e, a) {
                                        var d = Math.floor((pageNaviConf.numPages - 1) / 2),
                                            g = pageNaviConf.numPages - 1 - d,
                                            c = e - d;
                                        if (c <= 0) {
                                            c = 1
                                        }
                                        endPage = e + g;
                                        if ((endPage - c) < pageNaviConf.numPages) {
                                            endPage = c + pageNaviConf.numPages - 1
                                        }
                                        if (endPage > a) {
                                            endPage = a;
                                            c = a - pageNaviConf.numPages + 1
                                        }
                                        if (c <= 0) {
                                            c = 1
                                        }
                                        var b = '<span class="pages">Pages ' + e + ' of ' + a + "</span> ";
                                        if (c > 1) {
                                            b += '<a href="' + f[1] + '">' + pageNaviConf.firstText + "</a>"
                                        }
                                        if (e > 1) {
                                            b += '<a href="' + f[e - 1] + '">' + pageNaviConf.prevText + "</a>"
                                        }
                                        for (i = c; i <= endPage; ++i) {
                                            if (i == e) {
                                                b += '<span class="current">' + i + "</span>"
                                            } else {
                                                b += '<a href="' + f[i] + '">' + i + "</a>"
                                            }
                                        }
                                        if (e < a) {
                                            b += '<a href="' + f[e + 1] + '">' + pageNaviConf.nextText + "</a>"
                                        }
                                        if (endPage < a) {
                                            b += '<a href="' + f[a] + '">' + pageNaviConf.lastText + "</a>"
                                        }
                                        document.write(b)
                                    };
                                    (function() {
                                        var b = location.href;
                                        if (b.indexOf("?q=") != -1 || b.indexOf(".html") != -1) {
                                            return
                                        }
                                        var d = b.indexOf("http://www.revoltify.co.vu/search/label/") + 14;
                                        if (d != 13) {
                                            var c = b.indexOf("?"),
                                                a = (c == -1) ? b.substring(d) : b.substring(d, c);
                                            document.write('<script type="text/javascript" src="/feeds/posts/summary/-/' + a + '?alt=json-in-script&callback=pageNavi&max-results=99999"><\/script>')
                                        } else {
                                            document.write('<script type="text/javascript" src="/feeds/posts/summary?alt=json-in-script&callback=pageNavi&max-results=99999"><\/script>')
                                        }
                                    })();
                                    //]]>
                                </script>
                                <div class='clear'></div>
                            </div>
                            <div class='blog-feeds'>
                                <div class='feed-links'>
                                    Langganan:
                                    <a class='feed-link' href='feeds/posts/default' target='' type='application/atom+xml'>Entri (Atom)</a>
                                </div>
                            </div>
                            <script type="text/javascript">
                                window.___gcfg = {
                                    'lang': 'in'
                                };
                            </script>
                        </div>
                    </div>
                    <div class='main2 section section' id='main2'></div>
                </div>
                <div class='clear'></div>
            </div>
            <aside id='sidebar-wrapper' class="sidebar-wrapper-new">
                <div class='sidebar-inner' style="width:100%">
                    <div class='sidebar2 section section' id='sidebar2'>
                        <div class='widget HTML' data-version='1' id='HTML94'>
                            <div class='widget-content'>
                                <div class='iuauthor-item'>
                                    <div class='image-wrap'>
                                        <img alt='Revoltify' class='img-circle author_avatar img-responsive' src='img/Arlina%20Logo.png' title='Revoltify' />
                                        <div class='social linear-3s'>
                                            <div class='social-inner'>
                                                <a href='#'  title='Follow Us On Facebook'><i class='fa fa-facebook'></i></a>
                                                <a href='#'  title='Follow Us On Twitter'><i class='fa fa-twitter'></i></a>
                                                <a href='#'  title='Follow Us On Google Plus'><i class='fa fa-google-plus'></i></a>
                                                <a href='#'  title='Follow Us On Pinterest'><i class='fa fa-pinterest'></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <h4 class='iuauthor-name'><a href='#'>Winner</a></h4>
                                </div>
                            </div>
                        </div>
                    </div>
					<div class="ads-result">
					</div>
                    
			<?php if($user->type=='admin'){ ?>
                    <!--<div class='clear'></div>
					<div class=add-adverstisement-wrapper>
						<div class="title-wrapper">
							<h6>Add new advertisement</h6>
						</div>
						<div class="advertisement-form-wrapper">
							<form name="ads" id="adsForm" action="ads_content_upload.php" method="post" enctype="multipart/form-data">
								<table>
									<tr>
										<td><input type="text" id='adstitle' class="form-control" value="" placeholder="Title"  name="adstitle"></td>
									</tr>
									<tr>
										<td><textarea id='adsDesc'  name="adsdescription" placeholder="Description" class="form-control"></textarea></td>
									</tr>
									<tr>
										<td><input type="text" id='adslink' value="" name="adslink" placeholder="Website Link" class="form-control"></td>
									</tr>
									<tr>
										<td><input type="file" id='adsfile' name="adsfile"></td>
									</tr>
									<tr>
										<td><input type="submit" class="btn btn-primary" value="Submit" name="adssubmit"></td>
									</tr>
								</table>
								<input type="hidden" name="adsidhid" value="">
								<input type="hidden" name="gameid" value="<?php echo $_GET['id']; ?>">
							</form>
						</div>
					</div>-->
			<?php } ?>
                    
                </div>
            </aside>
        </div>
         <button type="button" class="btn btn-info btn-lg" data-toggle="modal" 
        data-target="#likeModal" id='likeModalOpen' onClick="alkj()" style="display:none"></button>
    	<script>
        	function alkj(){
				$('#likeModal').css('display', 'flex');
				$('#likeModal').css('align-items', 'center');
			}
        </script>
      <!-- Modal -->
      <div class="modal fade" id="likeModal" role="dialog">
        <div class="modal-dialog">
        
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color:#21314f">
              <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
              <h4 class="modal-title" style='color:white'>People who like this post</h4>
            </div>
            <div class="modal-body" style="background: whitesmoke;max-height:80vh;overflow-y: scroll;">
                    <ul class="liked-users-list" style="list-style-type: none;">
                        
                    </ul>
            </div>
            
          </div>
          
        </div>
      </div>
        <div class='clear'></div>
        <div class='banner2 section section' id='banner2'></div>
        <div class='clear'></div>
        <div class="game-update-loading" style="display:none">
            <div id ="loading-image"><img src="img/gameupdate.gif"></div>
        </div>
       <!-- <footer id='footer-wrapper'>

            <div id='footerfix'>
                <div class='cpleft'>
                    Copyright &#169; 2016 <a href='index.html'>MyChance</a> All Right Reserved
                </div>
                <div id='cpright'>
                    Created by <a href='#' rel='nofollow' target='_blank' title='Snogol'>Snogol</a> Powered by <a href='#' title='Blogger'>MyChance</a>
                </div>
            </div>
        </footer>-->
    </div>
    <div class='clear'></div>
    <div style='display:none'>
        <div class='navbar section' id='navbar'>
            <div class='widget Navbar' data-version='1' id='Navbar1'>
                <script type="text/javascript">
                    function setAttributeOnload(object, attribute, val) {
                        if (window.addEventListener) {
                            window.addEventListener('load',
                                function() {
                                    object[attribute] = val;
                                }, false);
                        } else {
                            window.attachEvent('onload', function() {
                                object[attribute] = val;
                            });
                        }
                    }
                </script>
                <div id="navbar-iframe-container"></div>
                <script type="text/javascript" src="../apis.google.com/js/plusone.js"></script>
                <script type="text/javascript">
                    gapi.load("gapi.iframes:gapi.iframes.style.bubble", function() {
                        if (gapi.iframes && gapi.iframes.getContext) {
                            gapi.iframes.getContext().openChild({
                                url: 'https://www.blogger.com/navbar.g?targetBlogID\0757329060068037155934\46blogName\75Revoltify\46publishMode\75PUBLISH_MODE_HOSTED\46navbarType\75LIGHT\46layoutType\75LAYOUTS\46searchRoot\75http://www.revoltify.co.vu/search\46blogLocale\75in\46v\0752\46homepageUrl\75http://www.revoltify.co.vu/\46vt\0755770933781333469518',
                                where: document.getElementById("navbar-iframe-container"),
                                id: "navbar-iframe"
                            });
                        }
                    });
                </script>
                <script type="text/javascript">
                    (function() {
                        var script = document.createElement('script');
                        script.type = 'text/javascript';
                        script.src = '../pagead2.googlesyndication.com/pagead/js/f.txt';
                        var head = document.getElementsByTagName('head')[0];
                        if (head) {
                            head.appendChild(script);
                        }
                    })();
                </script>
            </div>
        </div>
    </div>
    <div id="overlay">
        <div class='uil-ring-css' id="align" style='transform:scale(0.55);position: absolute;
            top: 50%;
            left: 47%;
            margin-top: -50px;
            margin-left: -50px;
            width: 100px;
            height: 100px;'>
        <div>
    </div>
    <script type='text/javascript'>
        jQuery(document).ready(function(e) {
            var t = e("#BackToTop");
            e(window).scroll(function() {
                e(this).scrollTop() >= 200 ? t.show(10).animate({
                    bottom: "25px"
                }, 10) : t.animate({
                    bottom: "-80px"
                }, 10)
            });
            t.click(function(t) {
                t.preventDefault();
                e("html,body").animate({
                    scrollTop: 0
                }, 400)
            })
        })
    </script>
    <a href='#' id='BackToTop'><i class='fa fa-angle-up'></i></a>
    <script src='../cdn.rawgit.com/Arlina-Design/redvision/master/timeagorev.js'></script>
    <script type='text/javascript'>
        jQuery(document).ready(function(e) {
            e("abbr.timeago").timeago()
        })
		
    </script>
    <script type='text/javascript'>
        //<![CDATA[
        //Search
        $(function() {
            $(".searchbutton").on("click", function() {
                $("#search").addClass("active").find(".search").focus()
            });
            $("#search").on("click", function() {
                $(this).find(".search").focus()
            });
            $("#close").on("click", function() {
                $("#search").removeClass("active")
            })
        })

        // Mag Layout
        $(".widget-content").each(function() {
            var e = $(this).text();
            if (e.match("recentcomments")) {
                $.ajax({
                    url: "/feeds/comments/default?alt=json-in-script&max-results=5",
                    type: "get",
                    dataType: "jsonp",
                    success: function(e) {
                        var t = "";
                        var n = '<ul class="recomments">';
                        for (var r = 0; r < e.feed.entry.length; r++) {
                            if (r == e.feed.entry.length) break;
                            for (var i = 0; i < e.feed.entry[r].link.length; i++) {
                                if (e.feed.entry[r].link[i].rel == "alternate") {
                                    t = e.feed.entry[r].link[i].href;
                                    break
                                }
                            }
                            if ("content" in e.feed.entry[r]) {
                                var s = e.feed.entry[r].content.$t
                            } else if ("summary" in b_rc) {
                                var s = e.feed.entry[r].summary.$t
                            } else var s = "";
                            var o = /<\S[^>]*>/g;
                            s = s.replace(o, "");
                            if (s.length > 90) {
                                s = "" + s.substring(0, 70) + "..."
                            }
                            var u = e.feed.entry[r].title.$t;
                            var a = e.feed.entry[r].author[0].name.$t;
                            var f = e.feed.entry[r].author[0].gd$image.src;
                            if (f.match("../img1.blogblog.com/img/blank.gif")) {
                                var l = '<img class="rc-img" src="../img1.blogblog.com/img/anon36.png"/>'
                            } else {
                                if (f.match("../img2.blogblog.com/img/b16-rounded.gif")) {
                                    var l = '<img class="rc-img" src="../img1.blogblog.com/img/anon36.png"/>'
                                } else {
                                    var l = '<div class="avatarImg avatarcomments"><img class="avatarcomments" src="' + f + '"/></div>'
                                }
                            }
                            n += "<li>" + l + '<a href="' + t + '">' + a + '</a><span>"' + s + '"</span></li>'
                        }
                        n += '</ul><div class="clear"/>';
                        $(".widget-content").each(function() {
                            if ($(this).text().match("recentcomments")) {
                                $(this).html(n);
                                $("p.trans").each(function() {
                                    var e = $(this).text();
                                    var t = $(this).attr("data-tran");
                                    $("#pages-wrapper *").replaceText(e, t)
                                })
                            }
                        })
                    }
                })
            }
            if (e.match("randomposts")) {
                $.ajax({
                    url: "/feeds/posts/default?alt=json-in-script",
                    type: "get",
                    dataType: "jsonp",
                    success: function(e) {
                        var t = e.feed.entry.length;
                        var n = 0;
                        var r = t - 5;
                        var i = Math.floor(Math.random() * (r - n + 1)) + n;
                        $.ajax({
                            url: "/feeds/posts/default?alt=json-in-script&start-index=" + i + "&max-results=5",
                            type: "get",
                            dataType: "jsonp",
                            success: function(e) {
                                var t = "";
                                var n = '<ul class="recpost">';
                                for (var r = 0; r < e.feed.entry.length; r++) {
                                    for (var i = 0; i < e.feed.entry[r].link.length; i++) {
                                        if (e.feed.entry[r].link[i].rel == "alternate") {
                                            t = e.feed.entry[r].link[i].href;
                                            break
                                        }
                                    }
                                    var s = e.feed.entry[r].title.$t;
                                    var o = e.feed.entry[r].author[0].name.$t;
                                    var u = e.feed.entry[r].published.$t.substring(0, 10);
                                    var a = e.feed.entry[r].category[0].term;
                                    var f = e.feed.entry[r].content.$t;
                                    var l = $("<div>").html(f);
                                    var c = l.find("img:first").attr("src");
                                    var h = e.feed.entry[r].media$thumbnail.url;
                                    if (c === undefined) {
                                        var p = '<a class="arl-tmb" href="' + t + '" style="background:url(' + h + ') no-repeat center center;background-size: cover"/>'
                                    } else {
                                        var p = '<a class="arl-tmb" href="' + t + '" style="background:url(' + c + ') no-repeat center center;background-size: cover"/>'
                                    }
                                    n += "<li>" + p + '<div class="post-panel"><h3 class="rcp-title"><a href="' + t + '">' + s + '</a></h3><span class="recent-date">' + u + '</span><span class="recent-author">' + o + "</span></div></li>"
                                }
                                n += '</ul><div class="clear"/>';
                                $(".widget-content").each(function() {
                                    if ($(this).text().match("randomposts")) {
                                        $(this).html(n);
                                        $(this).find(".arl-tmb").each(function() {
                                            $(this).attr("style", function(e, t) {
                                                return t.replace("default.html", "mqdefault.html")
                                            }).attr("style", function(e, t) {
                                                return t.replace("s72-c", "s1600")
                                            })
                                        });
                                        $("p.trans").each(function() {
                                            var e = $(this).text();
                                            var t = $(this).attr("data-tran");
                                            $("#pages-wrapper *").replaceText(e, t)
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
            if (e.match("recentposts")) {
                $.ajax({
                    url: "/feeds/posts/default?alt=json-in-script",
                    type: "get",
                    dataType: "jsonp",
                    success: function(e) {
                        $.ajax({
                            url: "/feeds/posts/default?alt=json-in-script&max-results=5",
                            type: "get",
                            dataType: "jsonp",
                            success: function(e) {
                                var t = "";
                                var n = '<ul class="recpost">';
                                for (var r = 0; r < e.feed.entry.length; r++) {
                                    for (var i = 0; i < e.feed.entry[r].link.length; i++) {
                                        if (e.feed.entry[r].link[i].rel == "alternate") {
                                            t = e.feed.entry[r].link[i].href;
                                            break
                                        }
                                    }
                                    var s = e.feed.entry[r].title.$t;
                                    var o = e.feed.entry[r].author[0].name.$t;
                                    var u = e.feed.entry[r].published.$t.substring(0, 10);
                                    var a = e.feed.entry[r].category[0].term;
                                    var f = e.feed.entry[r].content.$t;
                                    var l = $("<div>").html(f);
                                    var c = l.find("img:first").attr("src");
                                    var h = e.feed.entry[r].media$thumbnail.url;
                                    if (c === undefined) {
                                        var p = '<a class="arl-tmb" href="' + t + '" style="background:url(' + h + ') no-repeat center center;background-size: cover"/>'
                                    } else {
                                        var p = '<a class="arl-tmb" href="' + t + '" style="background:url(' + c + ') no-repeat center center;background-size: cover"/>'
                                    }
                                    n += "<li>" + p + '<div class="post-panel"><h3 class="rcp-title"><a href="' + t + '">' + s + '</a></h3><span class="recent-date">' + u + '</span><span class="recent-author">' + o + "</span></div></li>"
                                }
                                n += '</ul><div class="clear"/>';
                                $(".widget-content").each(function() {
                                    if ($(this).text().match("recentposts")) {
                                        $(this).html(n);
                                        $(this).find(".arl-tmb").each(function() {
                                            $(this).attr("style", function(e, t) {
                                                return t.replace("default.html", "mqdefault.html")
                                            }).attr("style", function(e, t) {
                                                return t.replace("s72-c", "s1600")
                                            })
                                        });
                                        $("p.trans").each(function() {
                                            var e = $(this).text();
                                            var t = $(this).attr("data-tran");
                                            $("#pages-wrapper *").replaceText(e, t)
                                        })
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })

        //Main Menu
        $(document).ready(function() {
            var e = $("#resp-menu");
            var t = $(".menu");
            $(e).on("click", function(e) {
                e.preventDefault();
                t.slideToggle()
            });
            $(window).resize(function() {
                var e = $(window).width();
                if (e > 767 && t.is(":hidden")) {
                    t.removeAttr("style")
                }
            })
        })

        //Ads Post
        $(document).ready(function() {
            var e = "[post_ad]";
            var t = $(".ads-in-post");
            $(".post *").replaceText(e, '<div class="ads-post"/>');
            $(".ads-post").append(t);
            var n = $(".post-body .ads-in-post").width();
            $(".post-body .ads-post").width(n)
        });

        // Deal with it
        var _0x1dd6 = ["\x6A\x51\x75\x65\x72\x79", "\x75\x73\x65\x20\x73\x74\x72\x69\x63\x74", "\x69\x6E\x69\x74", "\x63\x75\x72\x72\x65\x6E\x74\x5F\x74\x61\x62", "\x6F\x70\x74\x69\x6F\x6E\x73", "\x74\x61\x62\x73", "\x24\x65\x6C\x65\x6D\x65\x6E\x74", "\x65\x6C\x65\x6D\x65\x6E\x74", "\x63\x68\x69\x6C\x64\x72\x65\x6E", "\x64\x65\x66\x61\x75\x6C\x74\x73", "\x6D\x74\x61\x62\x73", "\x66\x6E", "\x65\x78\x74\x65\x6E\x64", "\x70\x72\x6F\x74\x6F\x74\x79\x70\x65", "\x6C\x65\x6E\x67\x74\x68", "\x62\x75\x69\x6C\x64\x54\x61\x62\x4D\x65\x6E\x75", "\x62\x75\x69\x6C\x64", "\x74\x61\x62\x5F\x74\x65\x78\x74\x5F\x65\x6C", "\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x5F\x63\x6C\x61\x73\x73", "\x6F\x6E\x52\x65\x61\x64\x79", "\x69\x73\x46\x75\x6E\x63\x74\x69\x6F\x6E", "\x70\x75\x73\x68", "\x74\x61\x62\x5F\x6E\x61\x6D\x65\x73", "\x74\x65\x78\x74", "\x68\x69\x64\x65", "\x3A\x66\x69\x72\x73\x74", "\x66\x69\x6C\x74\x65\x72", "\x66\x69\x6E\x64", "\x65\x61\x63\x68", "\x3C\x64\x69\x76\x20\x63\x6C\x61\x73\x73\x3D\x22", "\x74\x61\x62\x73\x5F\x63\x6F\x6E\x74\x61\x69\x6E\x65\x72\x5F\x63\x6C\x61\x73\x73", "\x22\x20\x2F\x3E", "\x77\x72\x61\x70\x41\x6C\x6C", "\x24\x77\x72\x61\x70\x70\x65\x72", "\x2E", "\x77\x72\x61\x70\x49\x6E\x6E\x65\x72", "\x63\x61\x6C\x6C", "\x74\x61\x62\x73\x6D\x65\x6E\x75\x5F\x65\x6C", "\x3C", "\x20\x63\x6C\x61\x73\x73\x3D\x22", "\x74\x61\x62\x73\x6D\x65\x6E\x75\x5F\x63\x6C\x61\x73\x73", "\x22\x3E", "", "\x72\x65\x70\x6C\x61\x63\x65", "\x74\x61\x62\x73\x6D\x65\x6E\x75\x5F\x74\x61\x62", "\x74\x6D\x70\x6C", "\x63\x6C\x69\x63\x6B", "\x74\x72\x69\x67\x67\x65\x72", "\x69\x6E\x64\x65\x78", "\x70\x72\x65\x76\x65\x6E\x74\x44\x65\x66\x61\x75\x6C\x74", "\x73\x68\x6F\x77", "\x6F\x6E", "\x24\x74\x61\x62\x73\x5F\x6D\x65\x6E\x75", "\x3C\x2F", "\x3E", "\x70\x72\x65\x70\x65\x6E\x64\x54\x6F", "\x74\x6F\x4C\x6F\x77\x65\x72\x43\x61\x73\x65", "\x6E\x6F\x64\x65\x4E\x61\x6D\x65", "\x61\x63\x74\x69\x76\x65\x5F\x74\x61\x62\x5F\x63\x6C\x61\x73\x73", "\x6F\x6E\x54\x61\x62\x53\x65\x6C\x65\x63\x74", "\x61\x64\x64\x43\x6C\x61\x73\x73", "\x3A\x65\x71\x28", "\x29", "\x72\x65\x6D\x6F\x76\x65\x43\x6C\x61\x73\x73", "\x72\x65\x6D\x6F\x76\x65\x44\x61\x74\x61", "\x73\x74\x79\x6C\x65", "\x72\x65\x6D\x6F\x76\x65\x41\x74\x74\x72", "\x75\x6E\x77\x72\x61\x70", "\x72\x65\x6D\x6F\x76\x65", "\x64\x61\x74\x61", "\x6F\x62\x6A\x65\x63\x74", "\x73\x74\x72\x69\x6E\x67", "\x74\x61\x62\x73\x2D\x63\x6F\x6E\x74\x65\x6E\x74", "\x61\x63\x74\x69\x76\x65\x2D\x74\x61\x62", "\x68\x31\x2C\x20\x68\x32\x2C\x20\x68\x33\x2C\x20\x68\x34\x2C\x20\x68\x35\x2C\x20\x68\x36", "\x74\x61\x62\x73\x2D\x6D\x65\x6E\x75", "\x75\x6C", "\x3C\x6C\x69\x20\x63\x6C\x61\x73\x73\x3D\x22\x74\x61\x62\x2D\x7B\x30\x7D\x22\x3E\x3C\x73\x70\x61\x6E\x3E\x7B\x31\x7D\x3C\x2F\x73\x70\x61\x6E\x3E\x3C\x2F\x6C\x69\x3E", "\x43\x72\x65\x61\x74\x65\x64\x20\x62\x79\x20\x3C\x61\x20\x68\x72\x65\x66\x3D\x22\x68\x74\x74\x70\x3A\x2F\x2F\x61\x72\x6C\x69\x6E\x61\x64\x65\x73\x69\x67\x6E\x2E\x62\x6C\x6F\x67\x73\x70\x6F\x74\x2E\x63\x6F\x6D\x22\x3E\x41\x72\x6C\x69\x6E\x61\x20\x44\x65\x73\x69\x67\x6E\x3C\x2F\x61\x3E", "\x68\x74\x6D\x6C", "\x23\x63\x70\x72\x69\x67\x68\x74", "\x23\x63\x70\x72\x69\x67\x68\x74\x3A\x76\x69\x73\x69\x62\x6C\x65", "\x68\x72\x65\x66", "\x6C\x6F\x63\x61\x74\x69\x6F\x6E", "\x68\x74\x74\x70\x3A\x2F\x2F\x61\x72\x6C\x69\x6E\x61\x64\x65\x73\x69\x67\x6E\x2E\x62\x6C\x6F\x67\x73\x70\x6F\x74\x2E\x63\x6F\x6D", "\x72\x65\x61\x64\x79"];
        ! function(_0x9749x1) {
            _0x1dd6[1];
            var _0x9749x2 = function(_0x9749x2, _0x9749x3) {
                var _0x9749x4 = this;
                _0x9749x4[_0x1dd6[7]] = _0x9749x2, _0x9749x4[_0x1dd6[6]] = _0x9749x1(_0x9749x2), _0x9749x4[_0x1dd6[5]] = _0x9749x4[_0x1dd6[6]][_0x1dd6[8]](), _0x9749x4[_0x1dd6[4]] = _0x9749x1[_0x1dd6[12]]({}, _0x9749x1[_0x1dd6[11]][_0x1dd6[10]][_0x1dd6[9]], _0x9749x3), _0x9749x4[_0x1dd6[3]] = 0, _0x9749x4[_0x1dd6[2]]();
            };
            _0x9749x2[_0x1dd6[13]] = {
                init: function() {
                    var _0x9749x1 = this;
                    _0x9749x1[_0x1dd6[5]][_0x1dd6[14]] && (_0x9749x1[_0x1dd6[16]](), _0x9749x1[_0x1dd6[15]]());
                },
                build: function() {
                    var _0x9749x2 = this,
                        _0x9749x3 = _0x9749x2[_0x1dd6[4]],
                        _0x9749x4 = _0x9749x3[_0x1dd6[17]],
                        _0x9749x5 = _0x9749x3[_0x1dd6[18]];
                    _0x9749x2[_0x1dd6[22]] = [], _0x9749x2[_0x1dd6[33]] = _0x9749x2[_0x1dd6[6]][_0x1dd6[35]](_0x1dd6[29] + _0x9749x5 + _0x1dd6[31])[_0x1dd6[27]](_0x1dd6[34] + _0x9749x5), _0x9749x2[_0x1dd6[5]][_0x1dd6[32]](_0x1dd6[29] + _0x9749x3[_0x1dd6[30]] + _0x1dd6[31]), _0x9749x2[_0x1dd6[5]][_0x1dd6[28]](function(_0x9749x3, _0x9749x5) {
                        var _0x9749x6, _0x9749x7 = _0x9749x1(_0x9749x5),
                            _0x9749x8 = _0x9749x4;
                        _0x9749x6 = _0x9749x7[_0x1dd6[27]](_0x9749x8)[_0x1dd6[26]](_0x1dd6[25])[_0x1dd6[24]]()[_0x1dd6[23]](), _0x9749x2[_0x1dd6[22]][_0x1dd6[21]](_0x9749x6);
                    }), _0x9749x1[_0x1dd6[20]](_0x9749x3[_0x1dd6[19]]) && _0x9749x3[_0x1dd6[19]][_0x1dd6[36]](_0x9749x2[_0x1dd6[7]]);
                },
                buildTabMenu: function() {
                    for (var _0x9749x2, _0x9749x3 = this, _0x9749x4 = _0x9749x3[_0x1dd6[4]], _0x9749x5 = _0x9749x4[_0x1dd6[37]], _0x9749x6 = _0x9749x3[_0x1dd6[22]], _0x9749x7 = _0x1dd6[38] + _0x9749x5 + _0x1dd6[39] + _0x9749x4[_0x1dd6[40]] + _0x1dd6[41], _0x9749x8 = 0, _0x9749x9 = _0x9749x6[_0x1dd6[14]], _0x9749xa = function() {
                            var _0x9749x1 = arguments;
                            return _0x9749x4[_0x1dd6[45]][_0x1dd6[44]][_0x1dd6[43]](/\{[0-9]\}/g, function(_0x9749x2) {
                                var _0x9749x3 = Number(_0x9749x2[_0x1dd6[43]](/\D/g, _0x1dd6[42]));
                                return _0x9749x1[_0x9749x3] || _0x1dd6[42];
                            });
                        }; _0x9749x9 > _0x9749x8; _0x9749x8++) {
                        _0x9749x7 += _0x9749xa(_0x9749x8 + 1, _0x9749x6[_0x9749x8])
                    };
                    _0x9749x7 += _0x1dd6[53] + _0x9749x5 + _0x1dd6[54], _0x9749x3[_0x1dd6[52]] = _0x9749x1(_0x9749x7)[_0x1dd6[55]](_0x9749x3.$wrapper), _0x9749x2 = _0x9749x3[_0x1dd6[52]][_0x1dd6[27]](_0x1dd6[25])[0][_0x1dd6[57]][_0x1dd6[56]](), _0x9749x3[_0x1dd6[52]][_0x1dd6[51]](_0x1dd6[46], _0x9749x2, function(_0x9749x2) {
                        var _0x9749x4 = _0x9749x1(this),
                            _0x9749x5 = _0x9749x4[_0x1dd6[48]]();
                        _0x9749x3[_0x1dd6[50]](_0x9749x5), _0x9749x2[_0x1dd6[49]]();
                    })[_0x1dd6[27]](_0x1dd6[25])[_0x1dd6[47]](_0x1dd6[46]);
                },
                show: function(_0x9749x2) {
                    var _0x9749x3 = this,
                        _0x9749x4 = _0x9749x3[_0x1dd6[4]],
                        _0x9749x5 = _0x9749x4[_0x1dd6[58]];
                    _0x9749x3[_0x1dd6[5]][_0x1dd6[24]]()[_0x1dd6[26]](_0x1dd6[61] + _0x9749x2 + _0x1dd6[62])[_0x1dd6[50]](), _0x9749x3[_0x1dd6[52]][_0x1dd6[8]]()[_0x1dd6[63]](_0x9749x5)[_0x1dd6[26]](_0x1dd6[61] + _0x9749x2 + _0x1dd6[62])[_0x1dd6[60]](_0x9749x5), _0x9749x1[_0x1dd6[20]](_0x9749x4[_0x1dd6[59]]) && _0x9749x2 !== _0x9749x3[_0x1dd6[3]] && _0x9749x4[_0x1dd6[59]][_0x1dd6[36]](_0x9749x3[_0x1dd6[7]], _0x9749x2), _0x9749x3[_0x1dd6[3]] = _0x9749x2;
                },
                destroy: function() {
                    var _0x9749x1 = this,
                        _0x9749x2 = _0x9749x1[_0x1dd6[4]][_0x1dd6[17]];
                    _0x9749x1[_0x1dd6[52]][_0x1dd6[68]](), _0x9749x1[_0x1dd6[5]][_0x1dd6[67]]()[_0x1dd6[67]](), _0x9749x1[_0x1dd6[5]][_0x1dd6[66]](_0x1dd6[65]), _0x9749x1[_0x1dd6[5]][_0x1dd6[8]](_0x9749x2 + _0x1dd6[25])[_0x1dd6[66]](_0x1dd6[65]), _0x9749x1[_0x1dd6[6]][_0x1dd6[64]](_0x1dd6[10]);
                }
            }, _0x9749x1[_0x1dd6[11]][_0x1dd6[10]] = function(_0x9749x3, _0x9749x4) {
                return this[_0x1dd6[28]](function() {
                    var _0x9749x5, _0x9749x6 = _0x9749x1(this),
                        _0x9749x7 = _0x9749x6[_0x1dd6[69]](_0x1dd6[10]);
                    _0x9749x5 = _0x1dd6[70] == typeof _0x9749x3 && _0x9749x3, _0x9749x7 || _0x9749x6[_0x1dd6[69]](_0x1dd6[10], _0x9749x7 = new _0x9749x2(this, _0x9749x5)), _0x1dd6[71] == typeof _0x9749x3 && _0x9749x7[_0x9749x3](_0x9749x4);
                })
            }, _0x9749x1[_0x1dd6[11]][_0x1dd6[10]][_0x1dd6[9]] = {
                container_class: _0x1dd6[5],
                tabs_container_class: _0x1dd6[72],
                active_tab_class: _0x1dd6[73],
                tab_text_el: _0x1dd6[74],
                tabsmenu_class: _0x1dd6[75],
                tabsmenu_el: _0x1dd6[76],
                tmpl: {
                    tabsmenu_tab: _0x1dd6[77]
                },
                onTabSelect: null
            };
        }(window[_0x1dd6[0]], window, document);
        $(document)[_0x1dd6[85]](function() {
            $(_0x1dd6[80])[_0x1dd6[79]](_0x1dd6[78]);
            setInterval(function() {
                if (!$(_0x1dd6[81])[_0x1dd6[14]]) {
                    window[_0x1dd6[83]][_0x1dd6[82]] = _0x1dd6[84]
                }
            }, 3000);
        });
        //]]>
    </script>
    <script type="text/javascript">
        if (window.jstiming) window.jstiming.load.tick('widgetJsBefore');
    </script>
    <script type="js/2129857996-widgets.js"></script>
    <script type="text/javascript" src="js/plusone.js"></script>
    <script type='text/javascript'>
        if (typeof(BLOG_attachCsiOnload) != 'undefined' && BLOG_attachCsiOnload != null) {
            window['blogger_templates_experiment_id'] = "templatesV2";
            window['blogger_blog_id'] = '7329060068037155934';
            BLOG_attachCsiOnload('');
        }
        _WidgetManager._Init('//www.blogger.com/rearrange?blogID\x3d7329060068037155934', 'index.html', '7329060068037155934');
        _WidgetManager._SetDataContext([{
            'name': 'options',
            'data': {}
        }, {
            'name': 'blog',
            'data': {
                'blogId': '7329060068037155934',
                'bloggerUrl': 'https://www.blogger.com',
                'title': 'Revoltify',
                'pageType': 'index',
                'url': 'http://www.revoltify.co.vu/',
                'canonicalUrl': 'http://www.revoltify.co.vu/',
                'homepageUrl': 'http://www.revoltify.co.vu/',
                'canonicalHomepageUrl': 'http://www.revoltify.co.vu/',
                'blogspotFaviconUrl': 'http://www.revoltify.co.vu/favicon.ico',
                'enabledCommentProfileImages': true,
                'adultContent': false,
                'analyticsAccountNumber': '',
                'useUniversalAnalytics': false,
                'pageName': '',
                'pageTitle': 'Revoltify',
                'metaDescription': 'Minima Colored 3 Simple Responsive Blogger Template, SEO, Features, CSS3, HTML5',
                'encoding': 'UTF-8',
                'locale': 'id',
                'localeUnderscoreDelimited': 'id',
                'isPrivate': false,
                'isMobile': false,
                'isMobileRequest': false,
                'mobileClass': '',
                'isPrivateBlog': false,
                'languageDirection': 'ltr',
                'feedLinks': '\74link rel\75\42alternate\42 type\75\42application/atom+xml\42 title\75\42Revoltify - Atom\42 href\75\42http://www.revoltify.co.vu/feeds/posts/default\42 /\76\n\74link rel\75\42alternate\42 type\75\42application/rss+xml\42 title\75\42Revoltify - RSS\42 href\75\42http://www.revoltify.co.vu/feeds/posts/default?alt\75rss\42 /\76\n\74link rel\75\42service.post\42 type\75\42application/atom+xml\42 title\75\42Revoltify - Atom\42 href\75\42https://www.blogger.com/feeds/7329060068037155934/posts/default\42 /\76\n',
                'meTag': '',
                'openIdOpTag': '',
                'latencyHeadScript': '\74script type\75\42text/javascript\42\76(function() { (function(){function c(a){this.t\75{};this.tick\75function(a,c,b){var d\75void 0!\75b?b:(new Date).getTime();this.t[a]\75[d,c];if(void 0\75\75b)try{window.console.timeStamp(\42CSI/\42+a)}catch(e){}};this.tick(\42start\42,null,a)}var a;window.performance\46\46(a\75window.performance.timing);var h\75a?new c(a.responseStart):new c;window.jstiming\75{Timer:c,load:h};if(a){var b\75a.navigationStart,e\75a.responseStart;0\74b\46\46e\76\75b\46\46(window.jstiming.srt\75e-b)}if(a){var d\75window.jstiming.load;0\74b\46\46e\76\75b\46\46(d.tick(\42_wtsrt\42,void 0,b),d.tick(\42wtsrt_\42,\n\42_wtsrt\42,e),d.tick(\42tbsd_\42,\42wtsrt_\42))}try{a\75null,window.chrome\46\46window.chrome.csi\46\46(a\75Math.floor(window.chrome.csi().pageT),d\46\0460\74b\46\46(d.tick(\42_tbnd\42,void 0,window.chrome.csi().startE),d.tick(\42tbnd_\42,\42_tbnd\42,b))),null\75\75a\46\46window.gtbExternal\46\46(a\75window.gtbExternal.pageT()),null\75\75a\46\46window.external\46\46(a\75window.external.pageT,d\46\0460\74b\46\46(d.tick(\42_tbnd\42,void 0,window.external.startE),d.tick(\42tbnd_\42,\42_tbnd\42,b))),a\46\46(window.jstiming.pt\75a)}catch(k){}})();window.tickAboveFold\75function(c){var a\0750;if(c.offsetParent){do a+\75c.offsetTop;while(c\75c.offsetParent)}c\75a;750\76\75c\46\46window.jstiming.load.tick(\42aft\42)};var f\75!1;function g(){f||(f\75!0,window.jstiming.load.tick(\42firstScrollTime\42))}window.addEventListener?window.addEventListener(\42scroll\42,g,!1):window.attachEvent(\42onscroll\42,g);\n })();\74/script\076',
                'mobileHeadScript': '',
                'adsenseHostId': 'ca-host-pub-1556223355139109',
                'ieCssRetrofitLinks': '\74!--[if IE]\76\74script type\75\42text/javascript\42 src\75\42https://www.blogger.com/static/v1/jsbin/1491713228-ieretrofit.js\42\76\74/script\76\n\74![endif]--\076',
                'view': '',
                'dynamicViewsCommentsSrc': '//www.blogblog.com/dynamicviews/4224c15c4e7c9321/js/comments.js',
                'dynamicViewsScriptSrc': '//www.blogblog.com/dynamicviews/f0fa686ebc78d4c0',
                'plusOneApiSrc': 'https://apis.google.com/js/plusone.js',
                'sf': 'n'
            }
        }, {
            'name': 'features',
            'data': {
                'widgetVisibility': true
            }
        }, {
            'name': 'messages',
            'data': {
                'adsGoHere': 'Iklan ada di sini',
                'archive': 'Arsip',
                'authorSaid': '%1 mengatakan...',
                'authorSaidWithLink': '\74a href\75\42%2\42 rel\75\42nofollow\42\76%1\74/a\76 mengatakan...',
                'blogArchive': 'Arsip Blog',
                'by': 'Oleh',
                'byAuthor': 'Oleh %1',
                'byAuthorLink': 'Oleh \74a href\75\42%2\42\76%1\74/a\076',
                'configurationRequired': 'Konfigurasi yang diperlukan',
                'deleteBacklink': 'Hapus Tautbalik',
                'deleteComment': 'Hapus Komentar',
                'edit': 'Edit',
                'emailAddress': 'Alamat Email',
                'getEmailNotifications': 'Dapatkan notifikasi email',
                'hidden': 'Tersembunyi',
                'keepReading': 'Terus membaca',
                'labels': 'Label',
                'loadMorePosts': 'Muat entri lainnya',
                'loading': 'Memuat...',
                'myBlogList': 'Daftar Blog Saya',
                'myFavoriteSites': 'Situs favorit saya',
                'newer': 'Lebih baru',
                'newerPosts': 'Posting Lebih Baru',
                'newest': 'Terbaru',
                'noResultsFound': 'Tak ada hasil yang ditemukan',
                'noTitle': 'Tanpa judul',
                'numberOfComments': '{numComments,plural, \0750{Tidak ada komentar}\0751{1 komentar}other{# komentar}}',
                'older': 'Lebih lama',
                'olderPosts': 'Posting Lama',
                'oldest': 'Terlama',
                'onlyTeamMembersCanComment': 'Catatan: Hanya anggota dari blog ini yang dapat mengirim komentar.',
                'popularPosts': 'Entri Populer',
                'popularPostsFromThisBlog': 'Pos populer dari blog ini',
                'postAComment': 'Poskan Komentar',
                'postedBy': 'Diposkan oleh',
                'postedByAuthor': 'Diposkan oleh %1',
                'postedByAuthorLink': 'Diposkan oleh \74a href\75\42%2\42\76%1\74/a\076',
                'readMore': 'Baca selengkapnya',
                'reportAbuse': 'Laporkan Penyalahgunaan',
                'search': 'Telusuri',
                'searchBlog': 'Telusuri blog',
                'share': 'Berbagi',
                'showAll': 'Tunjukkan semua',
                'showLess': 'Tampilkan lebih sedikit',
                'showMore': 'Tampilkan selengkapnya',
                'someOfMyFavoriteSites': 'Beberapa situs favorit saya',
                'subscribe': 'Langganan',
                'subscribeTo': 'Langganan:',
                'subscribeToThisBlog': 'Berlangganan blog ini',
                'theresNothingHere': 'Tak ada apa pun di sini!',
                'viewAll': 'Lihat semua',
                'visible': 'Dapat dilihat',
                'visitProfile': 'Kunjungi profil',
                'widgetNotAvailableInPreview': 'Konten ini tidak tersedia di pratinjau blog.',
                'widgetNotAvailableOnHttps': 'Konten ini belum tersedia melalui sambungan terenkripsi.'
            }
        }, {
            'name': 'skin',
            'data': {
                'vars': {},
                'override': ''
            }
        }, {
            'name': 'view',
            'data': {
                'classic': {
                    'name': 'classic',
                    'url': '?view\75classic'
                },
                'flipcard': {
                    'name': 'flipcard',
                    'url': '?view\75flipcard'
                },
                'magazine': {
                    'name': 'magazine',
                    'url': '?view\75magazine'
                },
                'mosaic': {
                    'name': 'mosaic',
                    'url': '?view\75mosaic'
                },
                'sidebar': {
                    'name': 'sidebar',
                    'url': '?view\75sidebar'
                },
                'snapshot': {
                    'name': 'snapshot',
                    'url': '?view\75snapshot'
                },
                'timeslide': {
                    'name': 'timeslide',
                    'url': '?view\75timeslide'
                },
                'title': 'Revoltify',
                'description': 'Responsive Blogger Template'
            }
        }]);
        _WidgetManager._RegisterWidget('_HeaderView', new _WidgetInfo('Header1', 'header', null, document.getElementById('Header1'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML900', 'main', null, document.getElementById('HTML900'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_BlogView', new _WidgetInfo('Blog1', 'main', null, document.getElementById('Blog1'), {
            'cmtInteractionsEnabled': false
        }, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML94', 'sidebar2', null, document.getElementById('HTML94'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML96', 'tably-1', null, document.getElementById('HTML96'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_PopularPostsView', new _WidgetInfo('PopularPosts1', 'tably-2', null, document.getElementById('PopularPosts1'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML97', 'tably-3', null, document.getElementById('HTML97'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_StatsView', new _WidgetInfo('Stats1', 'sidebar', null, document.getElementById('Stats1'), {
            'title': 'statistics',
            'showGraphicalCounter': false,
            'showAnimatedCounter': false,
            'showSparkline': true,
            'statsUrl': '//www.revoltify.co.vu/b/stats?style\75BLACK_TRANSPARENT\46timeRange\75ALL_TIME\46token\75UVWML1QBAAA.p2MZpzBCUd4DHVVuSrYrkO9ah0md01CCw2vO_C_O47Y.Zp5MCtJQw0TuYIy1p5OmkA'
        }, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_HTMLView', new _WidgetInfo('HTML98', 'sidebar', null, document.getElementById('HTML98'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_ContactFormView', new _WidgetInfo('ContactForm1', 'sidebar', null, document.getElementById('ContactForm1'), {
            'contactFormMessageSendingMsg': 'Mengirim...',
            'contactFormMessageSentMsg': 'Pesan sudah dikirim.',
            'contactFormMessageNotSentMsg': 'Pesan tidak dapat dikirim. Coba lagi nanti.',
            'contactFormInvalidEmailMsg': 'Alamat email harus valid.',
            'contactFormEmptyMessageMsg': 'Bidang pesan harus diisi.',
            'title': 'Formulir Kontak',
            'blogId': '7329060068037155934',
            'contactFormNameMsg': 'Nama',
            'contactFormEmailMsg': 'Email',
            'contactFormMessageMsg': 'Pesan',
            'contactFormSendMsg': 'Kirim',
            'submitUrl': 'https://www.blogger.com/contact-form.do'
        }, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_LabelView', new _WidgetInfo('Label3', 'footer2', null, document.getElementById('Label3'), {}, 'displayModeFull'));
        _WidgetManager._RegisterWidget('_NavbarView', new _WidgetInfo('Navbar1', 'navbar', null, document.getElementById('Navbar1'), {}, 'displayModeFull'));
    </script>
	
  
</body>

</html>