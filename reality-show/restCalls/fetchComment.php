<?php
	header('Access-Control-Allow-Headers: *');
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: *'); 
	require_once('../../system/start.php');
	require_once("../db_connect.php");
	if(isset($_POST)){
		$gameid=$_POST['gameid'];
		$postid=$_POST['postid'];
		$user=$_POST['userid'];
		$logged_in_user = ossn_user_by_guid($user);
		$usertype = $logged_in_user->type;
		$sql="SELECT id, userid, userfullname, comment FROM ossn_game_comments WHERE gameid=$gameid AND postid=$postid ORDER BY id DESC";
		$data='';
		if($result=mysqli_query($db, $sql)){
			if(!(mysqli_num_rows($result)>0)){
				echo "<div class='comment-username'>No Awards</div>
					  <div id='comment-loading' style='text-align:center;display:none;'><img src='img/cm-load.gif'></div>";
			}
			else{
				while($row=mysqli_fetch_array($result, MYSQLI_ASSOC)){
					$userguid = $row['userid'];
					/*$sqlfetchpic="SELECT value FROM ossn_entities_metadata WHERE guid=(SELECT MAX(guid) 
					FROM ossn_entities WHERE owner_guid=$user AND subtype='file:profile:photo');";
					if($resultfetchpic=mysqli_query($db, $sqlfetchpic)){
						if(mysqli_num_rows($resultfetchpic)>0){
							$r=mysqli_fetch_array($resultfetchpic, MYSQLI_ASSOC);
							$url=array_pop(explode('/', $r['value']));
							$profile_pic="http://".$_SERVER['SERVER_NAME']."/mychance.in/album/getphoto/$user/$url?type=1";
						}
						else
							$profile_pic="img/smaller.jpg";
					}
					else
						echo mysqli_error($db);*/
						$r = mysqli_fetch_array(mysqli_query($db, "SELECT username FROM ossn_users WHERE guid=$userguid"), MYSQLI_ASSOC);
						$profile_pic=$ossn_site_url."/avatar/".$r['username']."/small";
						
						$data=$data."<div class='comment' id='d".$row['id']."'>
										<div class='user-profile-pic' style='display:inline-block; margin-left:3px; margin:bottom:3px'>
											<img src=$profile_pic width='25px' height='25px' style='border-radius: 100%; margin-bottom: 3px;'>
										</div> 
										<div class='comment-username' style='display:inline-block'>".$row['userfullname']."</div>&nbsp;&nbsp;
										<div class='comment-single' style='display:inline-block; font-size:14px'>".$row['comment']."</div>
										<input type='hidden' id='c".$row['id']."' value='".$row['userid']."'>";
						if($usertype=='admin' || $user==$row['userid']){
						   $data=$data."<div class='comment-delete'>
											<span class='glyphicon glyphicon-remove' onclick='deleteComment(".$row['id'].", ".$postid.")'
											 style='float:right;display:inline-block ' data-toggle='tooltip' title='Delete comment'></span>
										</div>";
						}
						 $data=$data."</div>";
				}
				$data=$data."<div id='comment-loading".$postid."' style='text-align:center;display:none;'><img src='img/cm-load.gif'></div>";
				echo $data;
			}
		}
		else
			echo mysqli_error($db);
	}
?>