<?php
	header('Access-Control-Allow-Headers: *');
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: *'); 
	require_once('../../system/start.php');
	require_once("../db_connect.php");
	if($_POST){
		$data = array();
		if(!isset($_POST['postid']) && !isset($_POST['userid'])){
			$data['err'] = true;
			echo json_encode($data);
			die();
		}
		$postid=$_POST['postid'];
		$gameid=$_POST['gameid'];
		$likestatus=$_POST['btnvalue'];
		$userid=$_POST['userid'];
		$user = ossn_user_by_guid($userid);
		$userfullname = $user->fullname;
		
		if($likestatus=="Like"){
			$sql = "SELECT id FROM ossn_game_likes WHERE gameid=$gameid AND postid=$postid AND userid=$userid AND category=1";
			if(mysqli_num_rows(mysqli_query($db, $sql))>0){
				$data['likes']="err";
				echo json_encode($data);
				die();
			}
			//category->   realityshow =1, audition =2
			$sql="INSERT INTO ossn_game_likes (gameid, postid, userid, category, created)
			  	  VALUES('".$gameid."','".$postid."','".$userid."','1','".time()."')";
			if(!mysqli_query($db, $sql)){
				echo "Error creating record in ossn_game_likes:". mysqli_error($db);
				die();
			}
			else{
				$sql="UPDATE ossn_game_post SET likes=likes+1 WHERE id=".$postid;
				if(!mysqli_query($db, $sql)){
					echo "Error creating like in ossn_game_likes:". mysqli_error($db);
					die();
				}
				else{
					$sql="SELECT likes, comments FROM ossn_game_post WHERE id=$postid";
					$result=mysqli_query($db,$sql);
					if(!$result){
						echo mysqli_error($db);
						die();
					}
					else if(mysqli_num_rows($result)>0){
						$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
						$data['likes']=intval($row['likes']);
						$data['comments']=intval($row['comments']);
						echo json_encode($data);
					}
				}
			}
		}
		else{
				$sql = "SELECT id FROM ossn_game_likes WHERE gameid=$gameid AND postid=$postid AND userid=$userid AND category=1";
				if(!mysqli_num_rows(mysqli_query($db, $sql))>0){
					$data['likes']="err";
					echo json_encode($data);
					die();
				}
			
				$sql="UPDATE ossn_game_post SET likes=likes-1 WHERE id=".$postid;
				if(!mysqli_query($db, $sql)){
					echo "Error creating record in ossn_game_likes:". mysqli_error($db);
					die();
				}
				else {
					$sql="DELETE FROM ossn_game_likes WHERE postid=".$postid." AND userid=".$userid." AND category=1";
					if(!mysqli_query($db, $sql)){
						echo mysqli_error($db);
						die();
					}
					else{
						$sql="SELECT likes, comments FROM ossn_game_post WHERE id=$postid";
						$result=mysqli_query($db,$sql);
						if(!$result){
							echo mysqli_error($db);
						}
						else if(mysqli_num_rows($result)>0){
							$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
							$data['likes']=intval($row['likes']);
							$data['comments']=intval($row['comments']);
							echo json_encode($data);
						}
					}
				}
			//}
		}
	  
	}
?>