<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: POST');
	require_once('../../system/start.php');
	$siteurl = ossn_site_url();
	if(isset($_POST['searchString']) && isset($_POST['userid'])){
		$searchString = $_POST['searchString'];
		$userid = $_POST['userid'];
		if($userid==NULL || $userid=='')
			die();
		$friends = ossn_user_by_guid($userid)->getFriends();
		$data = array();
		foreach($friends as $friend){
			$name = $friend->first_name." ". $friend->last_name;
			if (stripos($name, $searchString) !== false) {
				$data[] = $friend;
			}
		}
		echo json_encode($data);
	}
?>