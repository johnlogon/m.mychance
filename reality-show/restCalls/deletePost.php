<?php
	header('Access-Control-Allow-Headers: *');
	header('Access-Control-Allow-Credentials: true');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: *'); 
	require_once("../db_connect.php");
	require_once('../../system/start.php');
	
	if(isset($_POST)){
		$postid=$_POST['postid'];
		$sql="DELETE FROM ossn_game_comments WHERE postid=".$postid;
		if(!mysqli_query($db, $sql))
			echo "Error deleting records from comments".mysqli_error($db);
			
		$sql="DELETE FROM ossn_game_likes WHERE postid=".$postid;
		if(!mysqli_query($db, $sql))
			echo "Error deleting records from likes".mysqli_error($db);
			
		$sql="DELETE FROM ossn_game_rating WHERE postid=".$postid;
		if(!mysqli_query($db, $sql))
			echo "Error deleting records from rating".mysqli_error($db);
			
		$sql="DELETE FROM ossn_game_rewards WHERE postid=".$postid;
		if(!mysqli_query($db, $sql))
			echo "Error deleting records from rewards".mysqli_error($db);
			
		$sql="SELECT * FROM ossn_game_post WHERE id=".$postid;
		if($result=mysqli_query($db, $sql)){
			$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
			if($row['post_type']!='text'){
				unlink($_SERVER['DOCUMENT_ROOT'].$row['link']);
			}
		}
		else
			echo mysqli_error($db);
			
		$sql="DELETE FROM ossn_game_post WHERE id=".$postid;
		if(!mysqli_query($db, $sql))
			echo "Error deleting records from posts".mysqli_error($db);
		else
			echo "success";
			
	}
	
?>