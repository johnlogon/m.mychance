<?php
	include("db_connect.php");
	if (!$db) {
    	die("Connection failed: " . mysqli_connect_error());
	} 
	if (!file_exists($logopath)) {
    	mkdir($logopath, 0777, true);
	} 
	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$gamephotodir)) {
    	mkdir($_SERVER['DOCUMENT_ROOT'].$gamephotodir, 0777, true);
	}    
	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$gamevideodir)) {
    	mkdir($_SERVER['DOCUMENT_ROOT'].$gamevideodir, 0777, true);
	} 
	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$gameaudiodir)) {
    	mkdir($_SERVER['DOCUMENT_ROOT'].$gameaudiodir, 0777, true);
	}
	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$gameadsdir)) {
    	mkdir($_SERVER['DOCUMENT_ROOT'].$gameadsdir, 0777, true);
	}
	$sql = "CREATE TABLE IF NOT EXISTS ossn_games (
  			id INT(11) NOT NULL AUTO_INCREMENT,
			name VARCHAR(100) DEFAULT NULL,
  			description VARCHAR(2000) DEFAULT NULL,
			enabled INT (11) DEFAULT 1,
  			created INT (11) DEFAULT NULL,
  			updated INT (11) DEFAULT NULL,
  			logo VARCHAR(500) DEFAULT NULL,
  			PRIMARY KEY (id))";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_games: " . mysqli_error($db);
	}
	
	$sql= "CREATE TABLE IF NOT EXISTS ossn_game_post (
		id INT(11) NOT NULL AUTO_INCREMENT,
		gameid INT(11) NOT NULL,
  		userid INT(11) NOT NULL,
		userfullname VARCHAR(50),
		post_type VARCHAR(50) DEFAULT NULL,
		link VARCHAR(500) DEFAULT NULL,
		text_content LONGTEXT,
		post_description TEXT,
		likes INT(11) DEFAULT 0,
		comments INT(11) DEFAULT 0,
		rating FLOAT(10,1) DEFAULT 0.0,
		number_of_ratings INT(11) DEFAULT 0,
		total_rate_val INT(11) DEFAULT 0,
  		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
  		PRIMARY KEY (id))ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_post: " . mysqli_error($db);
	}
		
	$sql= "CREATE TABLE IF NOT EXISTS ossn_game_likes (
		id INT(11) NOT NULL AUTO_INCREMENT,
		gameid INT(11) DEFAULT NULL,
  		postid INT(11) NOT NULL,
		userid INT(11) NOT NULL,
		category INT(5) NOT NULL,
  		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
  		PRIMARY KEY (id))";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_likes: " . mysqli_error($db);
	}
		
	$sql= "CREATE TABLE IF NOT EXISTS ossn_game_comments (
		id INT(11) NOT NULL AUTO_INCREMENT,
		gameid INT(11) NOT NULL,
  		postid INT(11) NOT NULL,
		userid INT(11) NOT NULL,
		userfullname VARCHAR(50),
		comment LONGTEXT,
  		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
  		PRIMARY KEY (id))";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_comments: " . mysqli_error($db);
	}
	
	$sql= "CREATE TABLE IF NOT EXISTS ossn_game_rewards (
		id INT(11) NOT NULL AUTO_INCREMENT,
		gameid INT(11) NOT NULL,
  		postid INT(11) NOT NULL,
		reward VARCHAR (3) NOT NULL,
		assignedbyid INT(11) NOT NULL,
  		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
  		PRIMARY KEY (id))";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_rewards: " . mysqli_error($db);
	}
	$sql="CREATE TABLE IF NOT EXISTS ossn_game_rating (
  		id INT(11) NOT NULL AUTO_INCREMENT,
		gameid INT(11) NOT NULL,
  		postid INT(11) NOT NULL,
  		rate INT(11) DEFAULT NULL,
  		assignedbyid INT(11) DEFAULT NULL,
		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
 		PRIMARY KEY (id))";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_rating: " . mysqli_error($db);
	}
	$sql="CREATE TABLE IF NOT EXISTS ossn_game_ads (
  		id INT(11) NOT NULL AUTO_INCREMENT,
		postedbyid INT (11),
		ads_title TEXT,
		ads_link VARCHAR(100),
		ads_description LONGTEXT,
		ads_content_link VARCHAR(100) DEFAULT NULL,
		ads_content_type VARCHAR(50),
		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
 		PRIMARY KEY (id))";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_ads " . mysqli_error($db);
	}
		
?>
