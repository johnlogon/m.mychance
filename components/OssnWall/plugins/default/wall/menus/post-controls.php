<style>
	.dropdown-menu.multi-level{
		width: 130px !important;
		position: fixed !important;
		top: 50% !important;
		right: 50% !important;
		margin-right: -65px !important;
		text-align: center !important;
		margin-top: -70px !important;
	}
	dropdown-menu.multi-level li{
		text-align:center;
	}
</style>

<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$postcontrols = $params['menu'];
$gid = $params['menu']['like'][0]['gid'];

if($postcontrols){
?>
<!--<a id="dLabel" role="button" data-toggle="dropdown" class="btn" data-target="#">-->
	<i class="fa fa-angle-down control-popup" style="margin-bottom:10px"></i>
<!--</a>-->
<ul class="dropdown-menu multi-level" role="menu" aria-labelledby="dropdownMenu" style="width:130px !important;margin-right:-65px !important;">
            <?php
                foreach ($postcontrols as $menu) {
                    foreach ($menu as $link) {
						$identifier = "";
						if($link['name']=='like')
							$identifier="id='starpoplabel".$gid."'";
					 	$class = "post-control-".$link['name'];
					 	if(isset($link['class'])){
							$link['class'] = $class.' '.$link['class'];	
						} else {
							$link['class'] = $class;
						}						
						unset($link['name']);
						$link = ossn_plugin_view('output/url', $link);						
                        ?>
                        <li style="padding:7px" <?php echo $identifier ?>><?php echo $link; ?></li>
                    <?php
                    }
                }
            ?>
           
    </ul>
<?php 
}
?>
