<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */

$image = $params['image'];
$like		= new OssnLikes;
$loggeduser = ossn_loggedin_user();
?>

<div class="ossn-wall-item" id="activity-item-<?php echo $params['post']->guid; ?>">
	<div class="row">
		<div class="meta">
			<img class="user-img" src="<?php echo $params['user']->iconURL()->small; ?>" />
			<div class="post-menu">
            	<span style="display:inline-block;margin-right: 0px;">
                    <i class="fa fa-angle-down control-popup" style="font-size: 18px !important;"></i>
                    <ul class="dropdown-menu post-controls-popup" role="menu" aria-labelledby="dropdownMenu">
                    <?php
                    	if($like->isLiked($params['post']->guid, $loggeduser->guid))
							echo '<li id="starpoplabel'.$params['post']->guid.'" style="padding:7px">
									<table width="100%">
										<tr>
											<td width="30%" style="text-align:right">
												<i class="fa fa-star" style="display:inline-block;"></i>
											</td>
											<td width="70%" style="text-align:left">
												<a class="post-control-star" data-guid='.$params['post']->guid.' style="display:inline-block;padding:0px"><b>Star</b></a>
											</td>
										<tr>
									</table>
								 </li>';
						else
							echo '<li id="starpoplabel'.$params['post']->guid.'" style="padding:7px">
									<table width="100%">
										<tr>
											<td width="30%" style="text-align:right">
												<i class="fa fa-star" style="display:inline-block;"></i>
											</td>
											<td width="70%" style="text-align:left">
												<a class="post-control-star" data-guid='.$params['post']->guid.'  style="display:inline-block;padding:0px">Star</a>
											</td>
									</table>
								  </li>';
					?>
                        <li style="padding:7px" onclick="postInspiration('wall-post', <?php echo $params['post']->guid ?>)">
                        	<table width="100%">
                            	<tr>
                                	<td width="30%" style="text-align:right">
                                    	<i class="fa fa-comments" aria-hidden="true" style="display:inline-block"></i>
                                    </td>
                                    <td width="70%" style="text-align:left">
                                    	<a class="post-control-inspire" style="display:inline-block;padding:0px">Inspire</a>
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <li style="padding:7px">
                        	<table width="100%">
                            	<tr>
                                	<td width="30%" style="text-align:right">
                                    	<i class="fa fa-share" aria-hidden="true" style="display:inline-block"></i>
                                    </td>
                                    <td width="70%" style="text-align:left">
                                    	<a class="post-control-pass" data-guid="<?php echo $params['post']->guid; ?>" style="display:inline-block;padding:0px;">Pass</a>
                                    </td>
                                </tr>
                            </table>
                        </li>
                        <?php
							if($params['post']->owner_guid==$loggeduser->guid){
						?>
                        	<li style="padding:7px">
                            	<table  width="100%">
                                     <tr>
                                        <td width="30%" style="text-align:right">
                                        	<i class="fa fa-pencil-square-o" aria-hidden="true" style="display:inline-block"></i>
                                        </td>
                                        <td width="70%" style="text-align:left">
                                        	<a class="post-control-change" data-guid="<?php  echo $params['post']->guid; ?>"  
                                	style="display:inline-block;padding:0px">Change</a>
                                        </td>
                                     </tr>
                                 </table>
                            </li>
                         <?php
							}
						?>
                         <?php
							if($params['post']->owner_guid==$loggeduser->guid || ossn_isAdminLoggedin()){
						?>
                            <li style="padding:7px;">
                            	<table  width="100%">
                                	<tr>
                                    	<td width="30%" style="text-align:right">
                                        	<i class="fa fa-trash" aria-hidden="true" style="display:inline-block"></i>
                                        </td>
                                        <td width="70%" style="text-align:left">
                                        	 <a class="post-control-vanish" data-guid="<?php  echo $params['post']->guid; ?>" 
                                	style="color:red;display:inline-block;padding:0px">Vanish</a>
                                        </td>
                                    </tr>
                                </table>
                            </li>
						<?php
							}
						?>
                       
                    </ul>
                </span>
				<div class="dropdown" style="display:none">
                 <?php
           			if (ossn_is_hook('wall', 'post:menu') && ossn_isLoggedIn()) {
                		$menu['post'] = $params['post'];
               			echo ossn_call_hook('wall', 'post:menu', $menu);
            			}
            		?>   
				</div>
			</div>
			<div class="user">
           <?php if ($params['user']->guid == $params['post']->owner_guid) { ?>
                <a class="owner-link"
                   href="<?php echo $params['user']->profileURL(); ?>"> <?php echo $params['user']->fullname; ?> </a>
              <?php if($params['post']->subtype=='wallshare' || $params['post']->subtype=='wallsharevideo'){
					  		 $original_owner = $params['post']->post_owner_guid;
							 $post_owner = ossn_user_by_guid($original_owner);
							 if($params['post']->media!='')
							 	$image = $params['post']->media;
					 		 echo "<span style='color: #1c3b5a;font-size: 15px;'>&nbsp;
							 		<i class='fa fa-share'></i>&nbsp;</span>
								  <a class='owner-link' href=".$post_owner->profileURL().">".$post_owner->fullname."</a>
								  <span style='color: #1c3b5a;font-size: 15px;'>'s 
								  	<a href='#' onclick='viewLikedPost(".$params['post']->post_guid.")'>
										post
									</a> 
								  </span>";
						} 
						else if($params['post']->subtype=='wall_video' || $params['post']->subtype=='wallsharevideo'){
							$src = $params['post']->media;
							$image = $src;
						}
				  ?> 
            <?Php
            } else {
                $owner = ossn_user_by_guid($params['post']->owner_guid);
                ?>
                <a class="owner-link" href="<?php echo $params['user']->profileURL(); ?>">
                    <?php echo $params['user']->fullname; ?>
                </a>
                <i class="fa fa-angle-right fa-lg"></i>
                <a class="owner-link" href="<?php echo $owner->profileURL(); ?>"> <?php echo $owner->fullname; ?></a>
            <?php } ?>
			</div>
			<div class="post-meta">
				<span class="time-created"><?php echo ossn_user_friendly_time($params['post']->time_created); ?></span>
                <span class="time-created"><?php echo $params['location']; ?></span>
                <?php
					echo ossn_plugin_view('privacy/icon/view', array(
							'privacy' => $params['post']->access,
							'text' => '-',
							'class' => 'time-created',
					));
				?>
			</div>
		</div>
		<div class="post-contents">
        <?php if(!empty($params['text'])){ ?>
			<p style="width:100%"><?php echo stripslashes($params['text']); ?></p>
			 <?php }
						if(!empty($params['friends'])){
							echo '<div class="friends">';
	                        foreach ($params['friends'] as $friend) {
								if(!empty($friend)){
	    	                        $user = ossn_user_by_guid($friend);
    	    	                    $url = $user->profileURL();
        	    	                $friends[] = "<a class='owner-link' href='{$url}'>{$user->fullname}</a>";
								}
                	        }
							if(!empty($friends)){
								echo implode(', ', $friends);
							}
							echo '</div>';
						}
              ?>
            <?php
            if (!empty($image)) {
                if($params['post']->subtype=='wallshare'){
                ?>
                	<img src="<?php echo ossn_site_url("post/photo/{$params['post']->post_guid}/{$image}"); ?>" 
                    onError="this.src='<?php echo ossn_site_url('others/error.png') ?>'"/>
                <?php
				}
				else if($params['post']->subtype=='wall_video' || $params['post']->subtype=="wallsharevideo"){
					echo '<div class="wrapper">
							<video preload="none" poster="'.$params['post']->thumbnail.'" 
								class="flplayer" id="video'.$params['post']->guid.'"  style="width:100%;height: 160px;">
						     <source type="video/mp4" src="'.$image.'">
						     <source type="video/webm" src="'.$image.'">
						     Your device does not support this video.
						  </video>
						  <div class="playpause"> <i class="fa fa-play-circle" style="font-size: 50px;color: white;" aria-hidden="true"></i></div>
						   <!--<canvas id="canvas'.$params['post']->guid.'" class="playpause-canvas" style="width:100%"></canvas>-->
						   </div>
						   <!--<script>
							$(document).ready(function(e) {
								var vid'.$params['post']->guid.' = document.getElementById("video'.$params['post']->guid.'");
								vid'.$params['post']->guid.'.addEventListener("loadeddata", function() {
									var canvas'.$params['post']->guid.' = document.getElementById("canvas'.$params['post']->guid.'");
									var video'.$params['post']->guid.' = document.getElementById("video'.$params['post']->guid.'");
									canvas'.$params['post']->guid.'.getContext("2d").drawImage(video'.$params['post']->guid.', 0, 0, 
										video'.$params['post']->guid.'.videoWidth, video'.$params['post']->guid.'.videoHeight);
								}, false);
							});
						</script>-->';
				}
				else{
				?>
                	<img src="<?php echo ossn_site_url("post/photo/{$params['post']->guid}/{$image}"); ?>"/>
                 

            <?php }
			} ?>           
		</div>
		<div class="comments-likes">
        	<?php
      		  if (ossn_is_hook('post', 'likes')) {
          			  echo ossn_call_hook('post', 'likes', $params['post']);
        		}
      		  ?>    
			<div class="menu-likes-comments-share" style="padding: 8px;margin-bottom:0px !important">
				<?php echo ossn_view_menu('postextra', 'wall/menus/postextra');?>
                <li><a style='color:black;text-decoration: none;cursor:pointer' id='share-btn' data-guid='<?php echo $params['post']->guid ?>'><b><i class="fa fa-share" aria-hidden="true"></i></b></a></li>
			</div>       
			<div class="comments-list">
            	<input type="hidden" class="vdo_identifier" />
                
              <?php
          			  if (ossn_is_hook('post', 'comments')) {
                			echo ossn_call_hook('post', 'comments', $params['post']);
           			   }
            		?>            				
			</div>
		</div>
	</div>
</div>
