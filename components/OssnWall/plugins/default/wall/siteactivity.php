<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$wall       = new OssnWall;

$accesstype = ossn_get_homepage_wall_access();
$loggedinuser = ossn_loggedin_user();

// allow admin to watch ALL postings independant of Wall setting
if($loggedinuser->canModerate()) {
	$posts = $wall->getAllPosts(array(
				'type' => 'user',
				'distinct' => true,
	));
	$count = $wall->getAllPosts(array(
			'type' => 'user',
			'count' => true,
			'distinct' => true,
	));
// wall mode: all site posts
} elseif($accesstype == 'public') {
	$posts = $wall->getPublicPosts(array(
				'type' => 'user',
				'distinct' => true,
	));
	$count = $wall->getPublicPosts(array(
			'type' => 'user',
			'count' => true,
			'distinct' => true,
	));
// wall mode: friends-only posts	
} elseif($accesstype == 'friends') {
	$posts = $wall->getFriendsPosts(array(
				'type' => 'user',
				'distinct' => true,
	));
	$count = $wall->getFriendsPosts(array(
			'type' => 'user',
			'count' => true,
			'distinct' => true,
	));
}

if($posts) {
		foreach($posts as $post) {
				$item = ossn_wallpost_to_item($post);
				echo ossn_wall_view_template($item);
		}
		
}

$ads = new OssnAds;
$ads = $ads->getAds();
if ($ads) {
		echo '<div class="ossn-ads">';
        foreach ($ads as $ad) {
          	$items[] = ossn_plugin_view('ads/item', array(
											'item' => $ad, 
					   ));
        }
		echo ossn_plugin_view('widget/view', array(
					'title' => ossn_print('sponsored'),
					'contents' => implode('', $items),
		));	
		echo '</div>';
    }
//echo ossn_view_pagination($count);
if($_POST['offset']=='1'){
	?>
	<style>
	.pagination{
		/*display:none;*/
	}
	.modal {
	  text-align: center;
	  padding: 0!important;
	}
	
	.modal:before {
	  content: '';
	  display: inline-block;
	  height: 100%;
	  vertical-align: middle;
	  margin-right: -4px; /* Adjusts for spacing */
	}
	
	.modal-dialog {
	  display: inline-block;
	  text-align: left;
	  vertical-align: middle;
	}
	.modal-header .close {
		margin-top: 1px !important;
		margin-right: 4px !important;
	}
	.modal-header{
		padding: 4px;
	}
	.modal-body {
		position: relative;
		padding: 4px !important;
	}
	.modal-footer {
    	padding: 4px !important;
	}
	.btn{
		padding: 2px 7px !important;
	}
	.post-control-comment, .post-control-like, .entity-menu-extra-comment, .entity-menu-extra-like{
		color:black;
	}

</style>
<script>
	$(document).ready(function(e) {
		//$('.menu-likes-comments-share').append("<li><a style='color: #337ab7;text-decoration: none;cursor:pointer' id='share-btn'>Share</a></li>")
    });
	$(document).on('click', '#share-btn', function(e){
		e.preventDefault();
		var $this = $(this);
		var guid = $this.parent().parent().parent().parent().parent().attr('id');
		guid = guid.substr(14, guid.length);
		$(".yes-btn").attr('onclick', 'sharePost('+guid+')');
		$('#sharemodalbtn').click();
	})
	function sharePost(guid){
		$.ajax({
			url: "http://m.mychance.in/share/xhr.php",
			type: "post",
			data: {'guid':guid, 'session_id':window.localStorage.getItem('session_id')},
			beforeSend: function(){
				$('.confirm').hide();
				$('body').css('pointer-events', 'none').css('opacity', '0.4');
			},
			success: function(data){
					$('#activity-item-'+guid).find('.row').first().append('<div class="alert alert-success"> <strong>Success!</strong> Post successfully passed to friends. </div>');
					window.setTimeout(function(){
						$('.alert-success').fadeOut(1000).remove();
					}, 3000);				
			},
			error:function(req, err,error){
				$('#activity-item-'+guid).find('.row').first().append('<div class="alert alert-danger"> <strong>Failed!</strong> Post cannot be passed to friends right now. </div>');
					window.setTimeout(function(){
						$('.alert-danger').fadeOut(1000).remove();
					}, 3000);
			},
			complete:function(){
				$('body').css('pointer-events', 'auto').css('opacity', '1');
				$('#no-btn').click();
			}
		});
	}
</script>


<button type="button" class="btn btn-info btn-lg" style="display:none" data-toggle="modal" id="sharemodalbtn" data-target="#myModalShare">Open Modal</button>

<!-- Modal -->
<div id="myModalShare" class="modal fade" role="dialog">
  <div class="modal-dialog" style="font-size: 17px;font-weight: bold;">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background: #337ab7; color: white;display:none">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Confirm</h4>
      </div>
      <div class="modal-body">
      	<table style="width:100%">
        	<tr style="width:100%">
            	<td width="75%">Pass this item to your friends</td>
                <td width="25%" style="text-align:right"><i class="fa fa-share yes-btn" area-hidden="true"></i></td>
            </tr>
        </table>
      </div>
      <div class="modal-footer" style="display:none">
        <button type="button" class="btn btn-danger" id="no-btn"data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="yes-btn" class="yes-btn" data-dismiss="modal">Yes</button>
      </div>
    </div>

  </div>
</div>
	<?php
}
?>