<?php

if(isset($_POST['session_id'])){
	session_id($_POST['session_id']);
	session_start();
}
else if(isset($_GET['session_id'])){
	session_id($_GET['session_id']);
	session_start();
}

$album = input('album');
echo ossn_view_form('photos/add', array(
    'action' => '#',
    'method' => 'POST',
    'component' => 'OssnPhotos',
	'data-action' => 'restCalls/application/user/photo/photos/add.php?album='.$album,
), false);
