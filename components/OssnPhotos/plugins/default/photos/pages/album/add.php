<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
echo ossn_view_form('album/add', array(
    'action' => '#',
	'data-action' => 'restCalls/application/user/photo/album/add.php',
    'component' => 'OssnPhotos',
), false);