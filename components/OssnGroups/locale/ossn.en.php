<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$en = array(
    'groups' => 'Teams',
    'add:group' => 'Add Team',
    'requests' => 'Requests',

    'members' => 'Members',
    'member:add:error' => 'Something went wrong! Please try again later.',
    'member:added' => 'Membership request approved!',

    'member:request:deleted' => 'Membership request declined!',
    'member:request:delete:fail' => 'Cannot decline membership request! Please try again later.',
    'membership:cancel:succes' => 'Membership request cancelled!',
    'membership:cancel:fail' => 'Cannot cancel membership request! Please try again later.',

    'group:added' => 'Successfully created the team!',
    'group:add:fail' => 'Cannot create team! Please try again later.',

    'memebership:sent' => 'Request successfully sent!',
    'memebership:sent:fail' => 'Cannot send request! Please try again later.',

    'group:updated' => 'Team has been updated!',
    'group:update:fail' => 'Cannot update team! Please try again later.',

    'group:name' => 'Team Name',
    'group:desc' => 'Team Description',
    'privacy:group:public' => '',
    'privacy:group:close' => '',

    'group:memb:remove' => 'Remove',
    'leave:group' => 'Leave Team',
    'join:group' => 'Join Team',
    'total:members' => 'Total Members',
    'group:members' => "Members (%s)",
    'view:all' => 'View all',
    'member:requests' => 'REQUESTS (%s)',
    'about:group' => 'Team About',
    'cancel:membership' => 'Membership cancel',

    'no:requests' => 'No Requests',
    'approve' => 'Approve',
    'decline' => 'Decline',
    'search:groups' => 'Search Teams',

    'close:group:notice' => 'Join this Team to see the posts, photos, and comments.',
    'closed:group' => 'Closed team',
    'group:admin' => 'Admin',
	
	'title:access:private:group' => 'Team post',

	// #186 group join request message var1 = user, var2 = name of group
	'ossn:notifications:group:joinrequest' => '%s has requested to join %s',
	'ossn:group:by' => 'By:',
	
	'group:deleted' => 'Team and team contents deleted',
	'group:delete:fail' => 'Team could not be deleted',	
);
ossn_register_languages('en', $en); 
