<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
?>
<div class="messages-inner">
    <div class="notification-friends">
        <?php
        if ($params['friends']) {
            $confirmbutton = ossn_print('ossn:notifications:friendrequest:confirmbutton');
            $denybutton = ossn_print('ossn:notifications:friendrequest:denybutton');
            foreach ($params['friends'] as $users) {
                $baseurl = ossn_site_url();
                $url = $users->profileURL();
                $img = "<img src='{$users->iconURL()->small}' />";
                $messages[] = "<li id='notification-friend-item-{$users->guid}'>
		              <div class='ossn-notifications-friends-inner'>
		               
		                <div class='notfi-meta'>
		                
						<span style='color: #337ab7;font-size: 15px;font-weight: bold;' class='user' onclick=LoadTimeline('{$users->username}')>{$users->fullname}</span>
						  <div class='controls' id='ossn-nfriends-{$users->guid}'>
						  <span id='add-friend-{$users->guid}'>
                           <input class='btn btn-primary' type='button' value='{$confirmbutton}' onclick=confirmFriend({$users->guid}) />
						   </span>
						   	<span id='remove-friend-{$users->guid}'>
						   <input class='btn btn-default' type='button' value='{$denybutton}' onclick=removeFriend({$users->guid}) />
						   </span>

                           </div>
  
						</div>
						</div>
						</li>";
            }
        }
        echo implode('', $messages);
        ?>
    </div>
</div>
<div class="bottom-all">
    <!-- <a href="#">See All</a> -->
</div>
