<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */

$en = array(
    'ossn:notifications:comments:post' => "%s left an inspiration on your post.",
    'ossn:notifications:like:post' => "%s starred your post.",
    'ossn:notifications:like:annotation' => "%s starred your inspiration.",
    'ossn:notifications:like:entity:file:ossn:aphoto' => "%s starred your photo.",
    'ossn:notifications:comments:entity:file:ossn:aphoto' => '%s left an inspiration on your photo.',
    'ossn:notifications:wall:friends:tag' => '%s tagged you in a post.',
    'ossn:notification:are:friends' => 'You are now friends!',
    'ossn:notifications:comments:post:group:wall' => "%s left an inspiration on your group post.",
    'ossn:notifications:like:entity:file:profile:photo' => "%s starred the profile photo.",
    'ossn:notifications:comments:entity:file:profile:photo' => "%s left an inspiration on the profile photo.",
	'ossn:notifications:group:invitation' => "%s invited you to join his team.",
    'ossn:notifications:like:post:group:wall' => '%s starred your post.',
	
    'ossn:notification:delete:friend' => 'Friend request deleted!',
    'notifications' => 'Notifications',
    'see:all' => 'See All',
    'friend:requests' => 'Friend Requests',
    'ossn:notifications:friendrequest:confirmbutton' => 'Confirm',
    'ossn:notifications:friendrequest:denybutton' => 'Deny',
	
    'ossn:notification:mark:read:success' => 'Successfully marked all as read',
    'ossn:notification:mark:read:error' => 'Can not mark all as read',
    
    'ossn:notifications:mark:as:read' => 'Mark all as read',
);
ossn_register_languages('en', $en); 
