<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */

$image = ossn_get_entity($params['post']->item_guid);
$image = ossn_profile_coverphoto_wall_url($image);

$like		= new OssnLikes;
$loggeduser = ossn_loggedin_user();
?>
<div class="ossn-wall-item" id="activity-item-<?php echo $params['post']->guid; ?>">
	<div class="row">
		<div class="meta">
			<img class="user-img" src="<?php echo $params['user']->iconURL()->small; ?>" />
			<div class="post-menu">
            	<span style="display:inline-block;">
                    <i class="fa fa-angle-down control-popup" style="font-size: 18px !important;"></i>
                    <ul class="dropdown-menu post-controls-popup-photo" role="menu" aria-labelledby="dropdownMenu" 
                        style="margin-top:-50px !important; ">
                        <?php
							if($like->isLiked($params['post']->item_guid, $loggeduser->guid, 'entity'))
								echo '<li id="starpoplabel'.$params['post']->item_guid.'" style="padding:7px">
										<table width="100%">
                               				<tr>
                                    			<td width="30%" style="text-align:right">
													<i class="fa fa-star" style="display:inline-block;"></i>
												</td>
												<td width="70%" style="text-align:left">
													<a class="post-control-star" data-guid='.$params['post']->guid.' style="display:inline-block;"><b>Star</b></a>
												</td>
											</tr>
										</table>
									 </li>';
							else
								echo '<li id="starpoplabel'.$params['post']->item_guid.'" style="padding:7px">
										<table width="100%">
                               				<tr>
                                    			<td width="30%" style="text-align:right">
													<i class="fa fa-star" style="display:inline-block;"></i>
												</td>
												</td width="70%" style="text-align:left">
													<a class="post-control-star" data-guid='.$params['post']->guid.' style="display:inline-block;">Star</a>
												</td>
											</tr>
										</table>
									   </li>';
						?>
                        <li style="padding:7px" onclick="postInspiration('covers', <?php echo $params['post']->item_guid ?>)">
                        	<table width="100%">
                                <tr>
                                    <td width="30%" style="text-align:right">
                                    	<i class="fa fa-comments" aria-hidden="true" style="display:inline-block"></i>
                                    </td>
                                    <td width="70%" style="text-align:left">
                                    	 <a class="post-control-inspire" style="display:inline-block;padding:0px">Inspire</a>
                                    </td>
                                 </tr>
                              </table>
                        	
                           
                        </li>
                        <?php
							if($params['post']->owner_guid==$loggeduser->guid || ossn_isAdminLoggedin()){
						?>
                        	<li style="padding:7px;">
                            	<table width="100%">
                                    <tr>
                                        <td width="30%" style="text-align:right">
                                            <i class="fa fa-trash" aria-hidden="true" style="display:inline-block"></i>
                                        </td>
                                        <td width="70%" style="text-align:left">
                                             <a class="post-control-vanish" style="color:red"  data-guid="<?php  echo $params['post']->guid; ?>">Vanish</a>
                                        </td>
                                     </tr>
                                  </table>
                            	</li>
                        <?php
							}
						?>
                    </ul>
                </span>
				<div class="dropdown" style="display:none">
                 <?php
           			if (ossn_is_hook('wall', 'post:menu') && ossn_isLoggedIn()) {
                		$menu['post'] = $params['post'];
               			echo ossn_call_hook('wall', 'post:menu', $menu);
            			}
            		?>   
				</div>
			</div>
			<div class="user">
           <?php if ($params['user']->guid == $params['post']->owner_guid) { ?>
                <a class="owner-link" href="<?php echo $params['user']->profileURL(); ?>"> <?php echo $params['user']->fullname; ?> </a>
                <div class="ossn-wall-item-type"><?php echo ossn_print('ossn:profile:cover:picture:updated');?></div>
            <?Php
            } else {

                $owner = ossn_user_by_guid($params['post']->owner_guid);
                ?>
                <a href="<?php echo $params['user']->profileURL(); ?>">
                    <?php echo $params['user']->fullname; ?>
                </a>
                <i class="fa fa-angle-right fa-lg"></i>
                <a href="<?php echo $owner->profileURL(); ?>"> <?php echo $owner->fullname; ?></a>
            <?php } ?>
			</div>
			<div class="post-meta">
				<span class="time-created"><?php echo ossn_user_friendly_time($params['post']->time_created); ?></span>
			</div>
		</div>

       <div class="post-contents">
                 <p><img src="<?php echo $image; ?>"/></p>
    	</div>
	<?php
		$vars['entity'] = ossn_get_entity($params['post']->item_guid);
		echo ossn_plugin_view('entity/comment/like/share/view', $vars);
	?>    
	</div>
</div>
