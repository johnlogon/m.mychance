<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$user = $params['user'];
if (OssnChat::getChatUserStatus($user->guid) == 'online') {
    $status = 'ossn-chat-icon-online';
} else {
    $status = 'ossn-chat-icon-offline';
}
$messages = ossn_chat()->getNew($user->guid, ossn_loggedin_user()->guid);
$total = '';
if ($messages) {
    $total = get_object_vars($messages);
    $total = count($total);
}
$tab_class = '';
$style = '';

if ($total > 0) {
    $tab_class = 'ossn-chat-tab-active';
    $style = 'style="display:block;"';
}

?>
<!-- Item -->
<style>
	.message{
		height:100px; 
		width:130px; 
		margin:0 auto; 
		background:black
	}
</style>
<div class="friend-tab-item" id="ftab-i<?php echo $user->guid; ?>">
	<button type="button" style="display:none" class="btn btn-info btn-lg" data-toggle="modal" id="modelButton" data-target="#myModal">Open Modal</button>
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
       <div class="modal-dialog" style="padding-top:15%">
          <!-- Modal content-->
          <div class="modal-content">
             <div class="modal-header" style="background: #1c3b5a;color: white;">
                <button type="button" class="close" data-dismiss="modal" style="color:#ffffff">&times;</button>
                <h4 class="modal-title">Select File to Send</h4>
             </div>
             <div class="modal-body">
             	<input type="file" id="file" name="fileToSend" />                
             </div>
             <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="sendBtn" onclick="sendThisFile(<?php echo ossn_loggedin_user()->guid ?>)">Send</button>
             </div>
          </div>
       </div>
    </div>
    <!-- MESSAGE MEDIA POP-UP -->
	<button type="button" class="btn btn-info btn-lg" id="message-pop" data-toggle="modal" data-target="#myModal2" style="display:none"></button>

 
  <div class="modal fade" id="myModal2" role="dialog" style="">
    <div class="modal-dialog" style="width:auto">
    
      
      <div class="modal-content" style="width:100%">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" >&times;</button>
        </div>
        <div class="modal-body" id="message-body" style="text-align:center;background-color:black; width:100%; height:100%">
         
        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- END OF POP-UP -->
    <!-- $arsalan.shah tab container start -->
    <div class="tab-container">

        <div class="ossn-chat-tab-titles" id="ftab-t<?php echo $user->guid; ?>"
             onclick="Ossn.ChatCloseTab(<?php echo $user->guid; ?>);">
            <div class="text ossn-chat-inline-table"><?php echo $user->fullname; ?></div>
            <div class="options ossn-chat-inline-table">
                <i class="fa fa-paperclip" style="font-size: 15px;" aria-hidden="true" title="Attach file" onclick="showUploadPanel(<?php echo $user->guid; ?>)"></i>
                <div class="ossn-chat-inline-table ossn-chat-tab-close" id="ftab-c<?php echo $user->guid; ?>"
                     onclick="Ossn.ChatTerminateTab(<?php echo $user->guid; ?>);"> X
                </div>
            </div>
        </div>
        
        <div class="ossn-chat-icon-smilies">
            <?php
            $vars['tab'] = $user->guid;
            echo ossn_plugin_view('chat/smilies/view', $vars);
            ?>
        </div>
        <!-- $arsalan.shah datatstart -->
        <div class="data" id="ossn-chat-messages-data-<?php echo $user->guid; ?>">
            <?php
            $messages_meta = ossn_chat()->get(ossn_loggedin_user()->guid, $user->guid);
            if ($messages_meta) {
                foreach ($messages_meta as $message) {
                    if (ossn_loggedin_user()->guid == $message->message_from) {
                        $vars['message'] = $message->message;
                        $vars['time'] = $message->time;
                        $vars['id'] = $message->id;
                        echo ossn_plugin_view('chat/message-item-send', $vars);
                    } else {
                        $vars['reciever'] = ossn_user_by_guid($message->message_from);
                        $vars['message'] = $message->message;
                        $vars['time'] = $message->time;
                        $vars['id'] = $message->id;
                        echo ossn_plugin_view('chat/message-item-received', $vars);
                    }
                }
            }
            ?>

        </div>
        <!-- $arsalan.shah datatend -->

    </div>
    <!-- $arsalan.shah tab container end -->
    <div class="inner friend-tab <?php echo $tab_class; ?>" id="ftab<?php echo $user->guid; ?>"
         onclick="Ossn.ChatOpenTab(<?php echo $user->guid; ?>);">
        <script>Ossn.ChatSendForm(<?php echo $user->guid;?>);</script>
        <form autocomplete="off" id="ossn-chat-send-<?php echo $user->guid; ?>">
            <input type="text" name="message" autocomplete="off" id="ossn-chat-input-<?php echo $user->guid; ?>"/>
            <div class="ossn-chat-message-sending">
               <div class="ossn-chat-sending-icon"></div>
            </div>
            <div class="ossn-chat-inline-table ossn-chat-icon-smile-set">
                <div class="ossn-chat-icon-smile" onClick="Ossn.ChatShowSmilies(<?php echo $user->guid; ?>);"></div>
            </div>
             <?php echo ossn_plugin_view('input/security_token'); ?>
            <input type="hidden" name="to" value="<?php echo $user->guid; ?>"/>
        </form>
        <div class="ossn-chat-new-message" <?php echo $style; ?>><?php echo $total; ?></div>
        <div id="ossnchat-ustatus-<?php echo $user->guid; ?>" class="<?php echo $status; ?>">
            <div class="ossn-chat-inner-text">
                <?php echo $user->fullname; ?>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
var to_id;
 function showUploadPanel(id){
	 to_id = id;
	 $('#file').val('');
	 $(".modal-backdrop").css('display', 'none');
	 $("#modelButton").click();
 }
</script>
<script type="text/javascript">

function sendThisFile(message_from){
	message_to = to_id;
	$('#sendBtn').attr('disabled', true);
	if($('#file').val()=='')
		return false; 
    var data = new FormData();
    data.append('file', $('#file').prop('files')[0]);
	data.append('cat', 'insp');
	data.append('message_to', message_to);
	data.append('message_from', message_from);
	data.append('message_to_name', $('.ossn-chat-inline-table').html());
	data.append('message_from_name', $('.name').find('a:first').html());
    $.ajax({
        type: 'POST',               
        processData: false, // important
        contentType: false, // important
        data: data,
        url: "./send-message/sendmessage.php",
        dataType : 'json',
		beforeSend: function(){
			$('.close').click();
			$('#ftab'+message_to).find('.ossn-chat-message-sending').css('display', 'block');
		},  
        success: function(jsonData){
				
           
        },
		error: function(xhr, status, error) {
		  Ossn.ChatOpenTab(message_to);
		  $('#ossn-chat-messages-data-'+message_to).append(xhr.responseText);
		},
		complete:function(){
			$('#sendBtn').attr('disabled', false);
			$('#ftab'+message_to).find('.ossn-chat-message-sending').css('display', 'none');
		}
    
    });
  }
  $(document).on('click', '.message-sender , .message-reciever', function(){
  	var id = $(this).attr("id");
	var src = $("#"+id).find('#message-photo').attr('src');
	//alert(src);
	if(src!='' && src!=undefined){
		var html = "<img src="+src+">"
		$("#message-body").html(html);
		$("#message-pop").click();
	}
  });
</script>
<!-- Item End -->
    
