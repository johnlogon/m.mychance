<div class="ossn-ad-item">
<div class="row">
	<a class="open-with-browser" href="<?php echo $params['item']->site_url;?>">
		<div class="col-md-12">
			<div class="ad-title"><?php echo $params['item']->title;?></div>
			<div class="ad-link"><?php echo $params['item']->site_url;?></div>
			<img class="ad-image" src="<?php echo ossn_ads_image_url($params['item']->guid); ?>" />
			<p><?php echo $params['item']->description;?></p>
		</div>
	</a>
</div>
</div>
<style>
.ossn-widget .widget-heading{
	color: white !important;
	background: #3278b3;
	border: 1px solid #f7f7f7 !important;
}
</style>