<?php
/**
 * Open Source Social Network
 *
 * @package   Open Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$object = $params['object'];

?>
<style>

</style>
<script>
	function postThisComment(id){
		if($('#comment-container').is(":visible")){
			$('#comment-container').find('#comment-container-'+id).submit()
		}
		else
			$('#comment-container-'+id).submit();
	}
</script>
<table width="102%">
	<tr>
    	<td width="80%">
        	<input type="text" name="comment" id="comment-box-<?php echo $object; ?>" class="comment-box"
                   placeholder="<?php echo ossn_print('write:comment'); ?>" style="height:45px"/>
            <input type="hidden" name="entity" value="<?php echo $object; ?>"/>
            <input type="hidden" name="comment-attachment"/>
        </td>
        <td style="text-align:center">
        	<div class="cmnt-btn-icons" onclick="postThisComment(<?php echo $object; ?>);"><i class="fa fa-paper-plane" style="margin-right: 0px;" ></i></div>
            <div class="cmnt-btn-icons" onclick="Ossn.Clk('#ossn-comment-image-file-<?php echo $object; ?>');">
            	<i class="fa fa-camera" style="margin-right: 0px;color:red"></i>
            </div>
           <!-- <div class="ossn-comment-attach-photo">
               
            </div>-->
        </td>
    </tr>
</table>
          
      