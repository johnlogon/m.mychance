<?php
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
$en = array(
	'write:comment' => 'My inspiration...',
	'like' => '<b style="color:black"><i class="fa fa-star" aria-hidden="true" style="color:red"></i></b>',
	'unlike' => '<b style="color:#337ab7"><i class="fa fa-star" style="color:red" aria-hidden="true"></i></b>',
	'comment:deleted' => 'Inspiration successfully deleted!',
	'comment:delete:error' => 'Cannot delete inspiration! Please try again later.',
	'comment:delete' => 'Delete',
	'comment:comment' => '<b><i class="fa fa-comment" aria-hidden="true"></i></b>',
	'comment:view:all' => 'View all inspirations',
	'comment:edit:success' => 'Inspiration has been edited successfully',
	'comment:edit:failed' => 'Can not edit your inspiration',
);
ossn_register_languages('en', $en); 
