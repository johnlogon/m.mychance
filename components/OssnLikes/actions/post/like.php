<?php
header('Access-Control-Allow-Origin: *');
/**
 * Open Source Social Network
 *
 * @packageOpen Source Social Network
 * @author    Open Social Website Core Team <info@informatikon.com>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
session_id('03e28b6ee8caecfd1d8a4a508fba9188');
session_start();
$OssnLikes = new OssnLikes;
$anotation = input('post');
$entity = input('entity');
if (!empty($anotation)) {
    $subject = $anotation;
    $type = 'post';
}
if (!empty($entity)) {
    $subject = $entity;
    $type = 'entity';

}
if ($OssnLikes->Like($subject, ossn_loggedin_user()->guid, $type)) {
    if (false) {
       // redirect(REF);
    } else {
        header('Content-Type: application/json');
        echo json_encode(array(
                'done' => 1,
                'button' => ossn_print('ossn:unlike'),
            ));
    }
} else {
    if (false) {
        //redirect(REF);
    } else {
        header('Content-Type: application/json');
        echo json_encode(array(
                'done' => 0,
                'button' => ossn_print('ossn:like'),
            ));
    }
}
exit;
