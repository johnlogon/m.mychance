<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
?>
<div class="row ossn-page-contents">
		<div class="col-md-6 home-left-contents">
					
			<div class="col-md-12" style="
    text-align: center;
    color: #fff;
    font-size: 18px;
    font-weight: 700;
    margin-top: 10%;
">
				<div class="col-md-12"><a href="<?php echo ossn_site_url('login');?>" style="color:#fff;padding-top:3px"><img style="max-height: 109px;" src="others/s.png"/ ></a></div>
				<div class="col-md-12" style="padding-top:15px"><a href="<?php echo ossn_site_url('login');?>" style="color:#fff;padding-top:3px">SOCIAL INSPIRATION</a></div> 
				<div class="col-md-12" style="padding-top:3px"><a href="reality-show" style="color:#fff;padding-top:3px">SCHOOL OF DREAMS</a></div>
				<div class="col-md-12" style="padding-top:3px"><a href="audition" style="color:#fff;padding-top:3px">UPCOMING STARS</a></div>
                <div class="col-md-12" style="padding-top:3px"><a href="send-message" style="color:#fff;padding-top:3px">MY CHAT</a></div>
				<div class="col-md-12" style="padding-top:3px"><a href="others/shopping/index.html" style="color:#fff;padding-top:3px">MCT</a></div>
			</div>
             
			
			 
 	   </div>   
       <div class="col-md-6">
    	<?php 
			$contents = ossn_view_form('signup', array(
        					'id' => 'ossn-home-signup',
        				'action' => ossn_site_url('action/user/register')
	   	 	));
			$heading = "<p>".ossn_print('its:free')."</p>";
			echo ossn_plugin_view('widget/view', array(
						'title' => ossn_print('create:account'),
						'contents' => $heading.$contents,
			));
			?>	       			
       </div>     
</div>	
