<?php
	require_once("db_connect.php");
	require_once('../system/start.php');
	if($_SESSION['OSSN_USER']==NULL || !isset($_POST['m_mychance'])){
		echo "Unauthorized action";
		die();
	}
	else{
		if(isset($_POST['m_mychance'])){
			$userid = $_POST['userid'];
			$userfullname = $_POST['userfullname'];
		}
		else{
			$user = $_SESSION['OSSN_USER'];
			$userid = $user->guid;
			$userfullname= $user->fullname;
		}
		if(isset($_POST["submitPostForm"])){
			if (!($_FILES["file"]["error"] > 0)){
				//echo pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				/** IF PHOTO/VIDEO/AUDIO IS SUBMITTED **/
			 
				$allowedExts = array("webm", "mp4", "WEBM", "MP4", 
									 "gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF",
									 "mp3", "wav", "MP3", "WAV");
				$photoExts = array("gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
				$videoExts = array("mpeg", "avi", "mp4", "MPEG", "AVI", "MP4");
				$audioExts = array("mp3", "wav", "MP3", "WAV");
				$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

				if ((($_FILES["file"]["type"] == "video/mp4")
					|| ($_FILES["file"]["type"] == "video/webm")
					|| ($_FILES["file"]["type"] == "image/gif")
					|| ($_FILES["file"]["type"] == "image/jpeg")
					|| ($_FILES["file"]["type"] == "image/jpg")
					|| ($_FILES["file"]["type"] == "image/pjpeg")
					|| ($_FILES["file"]["type"] == "image/x-png")
					|| ($_FILES["file"]["type"] == "image/png")
					|| ($_FILES["file"]["type"] == "audio/mp3")
					|| ($_FILES["file"]["type"] == "audio/mpeg")
					|| ($_FILES["file"]["type"] == "audio/wav"))
					&& in_array($extension, $allowedExts)){
						$dir;
						$type;
						if(in_array($extension, $photoExts)){
							$dir=$gamephotodir;
							$type=$phototype;
						}
						else if(in_array($extension, $videoExts)){
							$dir=$gamevideodir;
							$type=$videotype;
						}
						else if(in_array($extension, $audioExts)){
							$dir=$gameaudiodir;
							$type=$audioType;
						}
						else
							$type=$texttype;
						
						if ($_FILES["file"]["error"] > 0){
							echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
						}
						else{
							$textdata=$_POST['text'];
							//$textdata=utf8_encode($textdata);
							$textdata=htmlspecialchars($textdata, ENT_QUOTES, 'UTF-8');
							$temp = explode(".", $_FILES["file"]["name"]);
							$newfilename = round(microtime(true)) . '.' . end($temp);
							//move_uploaded_file($_FILES["video"]["tmp_name"], $targetdir . $newfilename);
							move_uploaded_file($_FILES["file"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].$dir .  $newfilename);
							$link=$dir .  $newfilename;
							$sql="INSERT INTO ossn_game_guest_post (userid, userfullname, post_type, link, text_content, created)
								  VALUES ('".$userid."','".$userfullname."','".$type."','".$link."','".$textdata."','".time()."')";
							if(!mysqli_query($db,$sql)){
								echo "Error creating record: " . mysqli_error($db);
							}
							else{
								$_SESSION['ERROR']='';
								header('Location: index.php');
							}
						}
				}
				else{
					$_SESSION['ERROR']='Invalid file';
					header('Location: index.php');
				}
			}
			else{
				//echo "file not empty";
				/** IF TEXT IS SUBMITTED **/
				$textdata=$_POST['text'];
				$textdata= preg_replace('/\t/', ' ', $textdata);
				$textdata=htmlspecialchars($textdata, ENT_QUOTES, 'UTF-8');
				//$textdata=utf8_encode($textdata);
				//echo $textdata;
				$sql="INSERT INTO ossn_game_guest_post (userid, userfullname, post_type, text_content, created)
								  VALUES ('".$userid."','".$userfullname."','text','".$textdata."','".time()."')";
				if(!mysqli_query($db,$sql)){
					echo "Error :" . mysqli_error($db);
				}
				else{
					$_SESSION['ERROR']='';
					header('Location: index.php');
				}
			}
		}
	}
?>