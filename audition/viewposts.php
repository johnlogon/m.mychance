<?php
	require_once("init.php");
	require_once("../system/start.php");
	
	if($_SESSION['OSSN_USER']==NULL){
		
	}

?>
<!DOCTYPE html>
<html>
	
<!-- Mirrored from pixelgeeklab.com/html/marvel/index-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 06 May 2016 04:48:16 GMT -->
<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>MyChance Audition</title>		
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Marvel - MultiShop Responsive HTML5 Template">
		<meta name="author" content="pixelgeeklab.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
		<link href='css/font-awesome.min.css' rel='stylesheet' />
	    
		<!-- Vendor CSS -->		
		
		

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-animate.css">
		
		
		<link href='css/1535467126-widget_css_2_bundle.css' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="css/game.css">
		<!-- Style Switcher-->
		<link rel="stylesheet" href="style-switcher/css/style-switcher.css">
		<link href="css/colors/default/style.html" rel="stylesheet" id="layoutstyle">
        
        <style>
			.modal-wide .modal-dialog {
				width: 65%;
			}
			.aud-post-like:hover{
				font-weight:bold;
			}
			.fa-trash:hover{
				font-weight:bold;
			}
			.like-label:hover{
				cursor:pointer;
				font-weight:bold;
			}
			.modal-backdrop{
				display:none; !important
			}
			@media screen and (min-width: 800px) {
    			.searchPosts{
					float:right; !important
				}
			}
			#performSearchIcon:hover{
				
			}
			#performSearch{
				background: #dd4e4e;
    			color: white;
			}
			.searchResultItem:hover{
				background: whitesmoke;
				cursor:pointer
			}
		</style>
		
		<!-- Head libs -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script language=JavaScript src="js/common.js"></script>
		<script language=JavaScript src="js/malayalam.js"></script>
        <script language=JavaScript src="js/game.js"></script>
        <script type="text/javascript">
			
			$(document).ready(function() {
				setTimeout(function(){
					$('#Message').hide(500);	
				}, 4000);

				
				if (busy == false) {
					busy = true;
					displayRecords(limit, offset);
				}
				$("#postForm").submit(function(){
					if($("#postText").val()=="" && $("#file").val()==""){
						//alert("no inputs");
						return false;
					}
					if($("#file").val()!="")
						$(".errspan-upload").fadeIn("slow");
				});
				$("#adsForm").submit(function(){
					if($("#adstitle").val()=="" || $("#adsDesc").val()=="" || $("#adslink").val()=="" || $("#adsfile").val()=="" )
						return false;
				})
				
			});
	 
			$(document).on("click", ".flplayer", function(e) {
				
					if (this.get(0).play) {
						  this.get(0).pause();
						  alert('music paused');
					} 
				});
				$(document).ready(function() {
				$(window).scroll(function() {
					if ($(window).scrollTop() + $(window).height() > $("#results").height() && !busy && again) {
						busy = true;
						offset = limit + offset;
						displayRecords(limit, offset);
						console.log("called");
					}
				});
				
	 
			});
			$(document).on('click', '.rating .stars', function (event) {
			
				 //alert("clicked");
				 var rate=$(this).val();
				 var postid=($(this).attr('id'));
				 
				 postid=postid.substr(5, postid.length-1);
				 //alert("gameid: "+gameid+" , postid: "+postid+" , rate: "+rate);
				 request = $.ajax({
									url: "rating.php",
									type: "post",
									data: {rate:rate, postid:postid},
									success: function(d){
										
									},
									error: function(error){
										alert(error);
									}
						  });
			});
			var busy = false;
			var limit = 8;
			var offset = 0;
			var again = true;
	
			function displayRecords( lim, off) {
			var gameid=$("#gameid").val();
			var userid=$("#userguid").val();
			var cid = $("#catid").val();
        	$.ajax({
          		type: "GET",
          		async: false,
         		url: "getposts.php",
          		data: "limit=" + lim + "&offset=" + off + "&gameid=" + gameid + "&guid=" + userid + "&cid=" + cid, 
          		cache: false,
          		beforeSend: function() {
           			$("#loader_message").html("").hide();
            		$('#loader_image').show();
          		},
          		success: function(html) {
					$("#results-allposts").append(html);
					
					$('#loader_image').hide();
					if (html == "") {
						again = false;
						$("#loader_message").html('<button data-atr="nodata" class="btn btn-default" type="button">No more records.</button>').show()
					} else {
					$("#loader_message").html('<button class="btn btn-default" type="button">Loading please wait...</button>').show();
					}
					window.busy = false;
					//showAds();
					
				}
        	});
		}
		$(document).ready(function(e) {
               $('#searchboxFriends').keyup(function(){
					var searchString = $('#searchboxFriends').val();
					if(searchString==''){
						$('#searchResults').fadeOut();
						return;
					}
					var url = $('#site_url').val()+"searchFriend.php";
					$.ajax({
						type: "POST",
						async: true,
						url: url,
						data: {searchString: searchString},
						cache: false,
						beforeSend: function() {
							
						},
						success: function(data) {
							var obj = $.parseJSON(data);
							if(obj.length==0){
								$('#searchResults').html("<div style='text-transform:none;'>No results</div>").fadeIn();
								return;
							}
							var html;
							$('#searchResults').html("");
							$('#searchResults').fadeIn();
							for(var i =0; obj[i]!=null; i++){
								html='<div class="searchResultItem" data-guid="'+obj[i]['guid']+'" id="item"'+obj[i]['guid']+' style="text-transform:none;text-align:-webkit-auto;height:25px;margin-top:2px;margin-bottom:2px;"><img src="'+$('#site_url').val()+'avatar/'+obj[i]['username']+'/small" style="display:inline-block; width:25px;border-radius:100%;margin-left: 10px;"><p id="name" style="display:inline-block;margin-left: 10px;">'+obj[i]['fullname']+'</p></div>';
								$('#searchResults').append(html);
							}
						},
						error:function(){
							alert("Oops! Something went wrong");
						}
					});
				}); 
				/*$('#searchboxFriends').blur(function(){
					$('#searchboxFriends').val("");
					$('#searchResults').html("");
					$('#searchResults').fadeOut();
				});*/
            });
			var selectedGuid;
			$(document).on('click', '.searchResultItem', function(){
				var guid = $(this).attr('data-guid');
				selectedGuid = guid;
				var name = $(this).find('#name').text();
				$('#searchboxFriends').val(name);
				$('#searchResults').fadeOut(1);
			});
			function loadSearchedEnity(){
				
				if(selectedGuid!='' && selectedGuid!=null){
					var cid = get('cid');
					if(cid=='' || cid==null){
						alert('Invalid request parameters');
						return;
					}
					location.href = $('#site_url').val()+"audition/viewposts.php?cid="+cid+"&guid="+selectedGuid;
				}
				else{
					alert('Invalid request parameters');
				}
			}
			function get(name){
			   if(name=(new RegExp('[?&]'+encodeURIComponent(name)+'=([^&]*)')).exec(location.search))
				  return decodeURIComponent(name[1]);
			}
		
		</script>

		<!--[if IE]>
			<link rel="stylesheet" href="css/ie.css">
		<![endif]-->

		<!--[if lte IE 8]>
			<script src="vendor/respond/respond.js"></script>
			<script src="vendor/excanvas/excanvas.js"></script>
		<![endif]-->

	</head>
	<body class="front bg-pattern-dark">
		<div class="body">
			<header id="header" class="center flat-top">
			
				<div class="container text-center">
					
					
                    <button type="button" id="open-login" style="display:none" data-toggle="modal" data-target="#LoginModal"></button>
							<div class="modal fade modal-wide" id="LoginModal" role="dialog" style="">
								<div class="modal-dialog">
								
								  <!-- Modal content-->
									<div class="modal-content" >
                                    
									<!--<div class="modal-header">
                                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                                      <h4 class="modal-title">Login</h4>
                                    </div>-->
										<div class="modal-body" >
                                        <h4 class="modal-title">Login</h4>
											<div class="panel-body" id="loginForm">
                                                <form accept-charset="UTF-8" role="form">
                                                <fieldset>
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="Username" name="username" id="uname" type="text">
                                                    </div>
                                                    <div class="form-group">
                                                        <input class="form-control" placeholder="Password" name="password" id="passwd" 
                                                        	type="password" value="">
                                                    </div>
                                                    <div class="login-error" style="color:red;" align="center"></div>
                                                    <div class="checkbox">
                                                        <label>
                                                            <input name="remember" type="checkbox" value="Remember Me"> Remember Me
                                                        </label>
                                                    </div>
                                                    <div id="signin-load" align="center" style="margin-bottom:-40px; display:none">
                                                        <img src="img/signinload.gif"  width="35px" height="35px">
                                                    </div>
                                                    <input class="btn btn-lg btn-success btn-block" type="button" id="login-btn" 
                                                    	onClick="submitLogin()" value="Login">
                                                </fieldset>
                                                </form>
                                            </div>
										</div>
									</div>
								  
								</div>
							</div>
                    <input type="hidden" value="<?php echo ossn_site_url(); ?>" id="site_url">
					<h1 class="logo">
						<a href="index.php"><img alt="Marvel" src="img/logo3.png"></a>
					</h1>
                     <?php 
						if(isset($_GET['errMsg'])){
							$class='danger';
							$msg = $_GET['errMsg'];
						}
						else{
							$class='success';
							$msg = $_GET['scssMsg'];
						}
					?>
					<?php 
						if(isset($_GET['errMsg']) || isset($_GET['scssMsg'])){
					?>
						<div class="alert alert-<?php echo $class ?>" id="Message">
						   <strong><?php echo $msg ?></strong>
						  <i class="fa fa-times pull-right" aria-hidden="true" onClick="closeMessage()" style="cursor:pointer"></i>
						</div>
				   <?php 
						}
				   ?>							
				</div>	
                <input type="hidden" name="catid" id="catid" value="<?php echo $_GET['cid'];?>">
                <input type="hidden" name="userguid" id="userguid" value="<?php echo $_GET['guid'];?>">
                <?php if($_SESSION['OSSN_USER']!=NULL){ 
					$user = $_SESSION['OSSN_USER'];
				?>
                <div class='container'>
				<div class='post-upload-wrapper' align="center"  >
                    <form action="http://www.mychance.in/audition/game_post_upload.php" method="post" id="postForm" enctype="multipart/form-data">
                        <input type="radio" name="lang" id="engllish" checked="checked" value="eng">English
                        <input type="radio" name="lang" id="malayalam" value="mal">Malayalam <br>
                        <textarea style="margin-top:15px;margin-bottom:7px;" class="form-control" name="text" id="postText" rows="5" placeholder="My chance."  charset="utf-8" 
                        	onKeyDown="typeKeydown(event)" onKeyPress="typeKeypress(event)" style="resize:none"></textarea>
                            <i class="fa fa-camera errspan csr" aria-hidden="true" onClick="clickFile()"  ></i>
							<img src="img/358.gif" class='errspan-upload' style="display:none;">
                            <span id="fileuploadname" style="float:right"></span>
                        <input type="file" name="file" id="file" onChange="alertfilename()" style="display:none">
                        <div id="post-upload-error" style="text-align:center; color:red; float:right"><?php echo $_SESSION['ERROR']; ?></div>
                        <input type="hidden" name="userid" value="<?php echo $user->guid ?>">
                         <input type="hidden" name="userfullname" value="<?php echo $user->fullname ?>">
                         <input type="hidden" name="mmychance" val="mychance">
                          <input type="hidden" name="catid" id="catid" value="<?php echo $_GET['cid'];?>">
                        <input type="submit" class="btn btn-primary" name="submitPostForm" value="&nbsp;&nbsp;&nbsp;Make&nbsp;&nbsp;&nbsp;" style="margin-bottom: 10px;"/>
                    </form>
 
                </div>
				</div>
             <?php } ?>			
			</header>

			<div role="main" class="main">
				<section class="main-content-wrap">	
					
					
					<section class="product-tab">
						<div class="container">
							<!-- Nav tabs -->
							<ul class="nav nav-tabs pro-tabs mason-tabs text-center" role="tablist">
								<li class="active"><a onClick="dis('all')" role="tab" data-toggle="tab">All</a></li>
								<li><a  onClick="dis('photo')" role="tab" data-toggle="tab">Profile</a></li>
								<li><a  onClick="dis('text')" role="tab" data-toggle="tab">Writings</a></li>
								<li><a  onClick="dis('audio')" role="tab" data-toggle="tab">Audio</a></li>
								<li><a  onClick="dis('video')" role="tab" data-toggle="tab">Video</a></li>
                                <?php if(ossn_isLoggedin()){ ?>
                                		<div class="searchPosts">
                                            <input type="text" class="" placeholder="Enter name" id="searchboxFriends">
                                            <button type="button" id="performSearch" onClick="loadSearchedEnity()">
                                                <i class="fa fa-search" id="performSearchIcon" aria-hidden="true"></i>
                                            </button>
                                            <div style="text-align:center">
                                                <div style="width:232px;border:solid 2px;position:absolute;z-index:99999;background:white;display:none" 
                                                    id="searchResults">
                                                    
                                                    
                                                </div>
                                            
                                            </div>
                                        </div>
                                <?php } ?>
							</ul>

							<!-- Tab panes -->
                            <div id="loader_image" style="text-align:center"><img src="img/load.gif" alt="" width="24" height="24">
                            	 Loading...please wait</div>
                            <button type="button" id="open-interest" style="display:none" data-toggle="modal" 
                                             	data-target="#InterestModal"></button>
                                            <div class="modal fade modal-wide interest-modal" id="InterestModal" role="dialog" style="">
                                                <div class="modal-dialog">
                                                
                                                  <!-- Modal content-->
                                                    <div class="modal-content" >
                                                    
                                                        <div class="modal-body" >
                                                            <div class="panel-body" id="interestForm">
                                                                <h3>Send Interest</h3>
                                                                <form accept-charset="UTF-8" role="form">
                                                                    <fieldset>
                                                                        <div class="form-group">
                                                                            <input class="form-control" placeholder="Full Name"
                                                                                 name="fullname" id="fullname" type="text">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input class="form-control" placeholder="Contact Number" name="number" id="number" 
                                                                                type="number" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <input class="form-control" placeholder="Email" name="email" id="email" 
                                                                                type="email" value="">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <textarea class="form-control" placeholder="Address" name="address" 
                                                                                id="address" style="resize:none"></textarea>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <textarea class="form-control" placeholder="Description" name="description" 
                                                                                id="description" style="resize:none"></textarea>
                                                                        </div>
                                                                        <div id="send-interest-load" align="center" style="margin-bottom:-40px; display:none ">
                                                                            <img src="img/signinload.gif"  width="35px" height="35px">
                                                                        </div>
                                                                        <div class="send-status" style="color:green; text-align:center"> <br><br></div>
                                                                        <input class="btn btn-lg btn-success btn-block" type="button" id="send-interest-btn" 
                                                                            onClick="sendInterest()" value="Send">
                                                                    </fieldset>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                            <button type="button" id="open-popup" style="display:none" data-toggle="modal" data-target="#myModal"></button>
                                            <div class="modal fade modal-wide" id="myModal" role="dialog" style="">
                                                <div class="modal-dialog">
                                                
                                                  <!-- Modal content-->
                                                    <div class="modal-content">
                                                    	<form action="edit_post.php" method="post" id="edit-post">
                                                    	<div class="modal-header">
                                                            <button type="button" class="btn btn-default" style="float:right" data-dismiss="modal" 
                                                            onClick="dataDismisal()">Close</button>
                                                            <a type="button" class="btn btn-default" style="float:right" 
                                                            onClick="editContent()"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                                                          </div>
                                                    
                                                        <div class="modal-body" >
                                                            <div class="pop-content" style="width:100%;">
                                                                
                                                            </div>
                                                            <input type="submit" style="display:none;" class="btn-edit" value="Save" name="Submitpostedit"/>
                                                        </div>
                                                        </form>
                                                    </div>
                                                  
                                                </div>
                                            </div>
                                             <!-- Post likes user list -->
                                              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" 
                                              	data-target="#likeModal" id='likeModalOpen' style="display:none"></button>
                                            <!-- Modal -->
                                              <div class="modal fade" id="likeModal" role="dialog">
                                                <div class="modal-dialog">
                                                
                                                  <!-- Modal content-->
                                                  <div class="modal-content">
                                                    <div class="modal-header" style="background-color:#21314f">
                                                      <button type="button" class="close" data-dismiss="modal" style="color: white;">&times;</button>
                                                      <h4 class="modal-title" style='color:white'>People who like this post</h4>
                                                    </div>
                                                    <div class="modal-body" style="background: whitesmoke;max-height:80vh;overflow-y: scroll;">
                                                        	<ul class="liked-users-list" style="list-style-type: none;">
                                                            	
                                                            </ul>
                                                    </div>
                                                    
                                                  </div>
                                                  
                                                </div>
                                              </div>
							<div class="tab-content">
								<div class="tab-pane active" id="allposts">
									<div class="row" style="min-height:360px;">
										<div class="masonry-m">
											<div id="results-allposts">
                                            	
                                            </div>
                                        </div>
									</div>
                                </div>
								<div class="tab-pane" id="photo">
									<div class="row">
										<div class="masonry-m">
											<div id="results-photos">
                                            	
                                            </div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="texts">
									<div class="row">
										<div class="masonry-m">
											<div id="results-texts">
                                            	
                                            </div>
										</div>
									</div>
								</div>
								<div class="tab-pane" id="audios">
									<div class="row">
										<div class="masonry-m">
											<div id="results-audios">
                                            	
                                            </div>
										</div>
									</div>
								</div>
                                <div class="tab-pane" id="videos">
									<div class="row">
										<div class="masonry-m">
											<div id="results-videos">
                                            	
                                            </div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</section>
					
					<section class="parallax">
						<div class="container mask3" style="background-image: url(images/bg/bg-demo-5.jpg);">
							
						</div>
					</section>
					
										
					
				</section>
			</div>
		</div>

		<!-- Begin Search -->
		
		
	
		<!-- Vendor -->
		
		
		<!-- Theme Base, Components and Settings -->
		<script src="js/theme.js"></script>
		
		<!-- Style Switcher -->
		<script type="text/javascript" src="style-switcher/js/switcher.js"></script>
		
	</body>

<!-- Mirrored from pixelgeeklab.com/html/marvel/index-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 06 May 2016 04:50:27 GMT -->
</html>