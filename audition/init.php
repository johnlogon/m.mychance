<?php
	require_once("db_connect.php");
	
	if (!$db) {
    	die("Connection failed: " . mysqli_connect_error());
	}
	
	$sql= "CREATE TABLE IF NOT EXISTS ossn_game_post_categories (
		id INT(11) NOT NULL AUTO_INCREMENT,
		category VARCHAR(50),
		logo TEXT DEFAULT NULL,
  		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
  		PRIMARY KEY (id))ENGINE=MyISAM DEFAULT CHARSET=utf8";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_post: " . mysqli_error($db);
	} 
	
	$sql= "CREATE TABLE IF NOT EXISTS ossn_game_guest_post (
		id INT(11) NOT NULL AUTO_INCREMENT,
  		userid INT(11) NOT NULL,
		userfullname VARCHAR(50),
		post_type VARCHAR(50) DEFAULT NULL,
		link VARCHAR(500) DEFAULT NULL,
		text_content LONGTEXT,
		post_description TEXT,
		rating FLOAT(10,1) DEFAULT 0.0,
		category INT (11),
  		created INT (11) DEFAULT NULL,
  		updated INT (11) DEFAULT NULL,
  		PRIMARY KEY (id))ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8";
	if(!mysqli_query($db,$sql)){
		echo "Error creating table ossn_game_post: " . mysqli_error($db);
	} 
?>