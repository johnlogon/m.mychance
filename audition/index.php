<?php
	require_once("init.php");
	require_once("../system/start.php");
	
	if($_SESSION['OSSN_USER']==NULL){
		
	}

?>
<!DOCTYPE html>
<html>
	
<!-- Mirrored from pixelgeeklab.com/html/marvel/index-4.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 06 May 2016 04:48:16 GMT -->
<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<title>MyChance Audition</title>		
		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Marvel - MultiShop Responsive HTML5 Template">
		<meta name="author" content="pixelgeeklab.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Web Fonts  -->
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css">
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css' />
		<link href='css/font-awesome.min.css' rel='stylesheet' />
	    
		<!-- Vendor CSS -->		
		
		

		<!-- Theme CSS -->
		<link rel="stylesheet" type="text/css" href="css/theme.css">
		<link rel="stylesheet" href="css/theme-animate.css">
		
		
		<link href='css/1535467126-widget_css_2_bundle.css' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" type="text/css" href="css/game.css">
		<!-- Style Switcher-->
		<link rel="stylesheet" href="style-switcher/css/style-switcher.css">
		<link href="css/colors/default/style.html" rel="stylesheet" id="layoutstyle">
        
        <style>
			.modal-wide .modal-dialog {
				width: 65%;
			}
		</style>
		
		<!-- Head libs -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script language=JavaScript src="js/common.js"></script>
		<script language=JavaScript src="js/malayalam.js"></script>
        <script language=JavaScript src="js/game.js"></script>
        <script type="text/javascript">
			
			$(document).ready(function() {
				//alert(1);
				
				if (busy == false) {
					busy = true;
					displayRecords(limit, offset);
				}
				
				
			});
	 
			
			var busy = false;
			var limit = 8;
			var offset = 0;
			var again = true;
	
			function displayRecords( lim, off) {
				var gameid=$("#gameid").val();
				var userid=$("#loggedinuserid").val();
				//alert(gameid);
				$.ajax({
					type: "GET",
					async: false,
					url: "getCategories.php",
					data: "limit=" + lim + "&offset=" + off + "&gameid=" + gameid + "&userid=" + userid, 
					cache: false,
					beforeSend: function() {
						$("#loader_message").html("").hide();
						$('#loader_image').show();
					},
					success: function(html) {
						$("#results-allposts").append(html);
						
						$('#loader_image').hide();
						if (html == "") {
							again = false;
							$("#loader_message").html('<button data-atr="nodata" class="btn btn-default" type="button">No more records.</button>').show()
						} else {
						$("#loader_message").html('<button class="btn btn-default" type="button">Loading please wait...</button>').show();
						}
						window.busy = false;
						//showAds();
						
					}
				});
			}
		
		</script>

	</head>
	<body class="front bg-pattern-dark">
		<div class="body">
			<header id="header" class="center flat-top">
			
				<div class="container text-center">
					<h1 class="logo">
						<a href="index.php"><img alt="Marvel" src="img/logo3.png"></a>
					</h1>					
				</div>	
               		
			</header>

			<div role="main" class="main">
				<section class="main-content-wrap">	
					
					
					<section class="product-tab">
						<div class="container">
							
                            <div id="loader_image" style="text-align:center; margin-top:50px">
                            	<img src="img/load.gif" alt="" width="24" height="24">
                            	 Loading...please wait
                            </div>
                         
							<div class="tab-content">
								<div class="tab-pane active" id="allposts">
									<div class="row" style="min-height:360px;">
										<div class="masonry-m" style="margin-top:25px">
											<div id="results-allposts">
                                            	
                                            </div>
                                        </div>
									</div>
                                </div>
							</div>
						</div>
					</section>
					
					<section class="parallax">
						<div class="container mask3" style="background-image: url(images/bg/bg-demo-5.jpg);">
							
						</div>
					</section>
				</section>
			</div>
		</div><script src="js/theme.js"></script>
		<script type="text/javascript" src="style-switcher/js/switcher.js"></script>
	</body>
</html>