<?php
	require_once("db_connect.php");
	require_once("../system/start.php");
	
	$user = $_SESSION['OSSN_USER'];
	if($user==NULL)
		die();
	
	if(isset($_POST["adssubmit"])){
		$adstitle=$_POST['adstitle'];
		$adsdecription=$_POST['adsdescription'];
		$adslink=$_POST['adslink'];
		$adsid=$_POST['adsidhid'];
		$gameid=$_POST['gameid'];
		
		 $photoExts = array("gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
 		 $videoExts = array("webm","mp4", "WEBM", "MP4");
		
		if (!($_FILES["adsfile"]["error"] > 0)){
			$allowedExts = array("webm", "mp4", "WEBM", "MP4", 
									 "gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
			$extension = pathinfo($_FILES['adsfile']['name'], PATHINFO_EXTENSION);
			if(in_array($extension, $photoExts)){
							$type="photo";
						}
			else if(in_array($extension, $videoExts)){
				$type="video";
			}
			
			if ((($_FILES["adsfile"]["type"] == "video/mp4")
					|| ($_FILES["adsfile"]["type"] == "video/webm")
					|| ($_FILES["adsfile"]["type"] == "image/gif")
     	 			|| ($_FILES["adsfile"]["type"] == "image/jpeg")
      	 			|| ($_FILES["adsfile"]["type"] == "image/jpg")
      	 			|| ($_FILES["adsfile"]["type"] == "image/pjpeg")
      	 			|| ($_FILES["adsfile"]["type"] == "image/x-png")
      	 			|| ($_FILES["adsfile"]["type"] == "image/png"))
					&& in_array($extension, $allowedExts)){	
				if ($_FILES["adsfile"]["error"] > 0){
    				echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    			}
  				else{
				$temp = explode(".", $_FILES["adsfile"]["name"]);
				$newfilename = round(microtime(true)) . '.' . end($temp);
				$link=$gameadsdir .  $newfilename;
				move_uploaded_file($_FILES["adsfile"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].$gameadsdir .  $newfilename);
      			echo "Stored in: " . $_SERVER['DOCUMENT_ROOT'].$gameadsdir. $_FILES["adsfile"]["name"];
				//IF UPDATING ADS WITH NEW CONTENT
				if($adsid!=''){
					echo "update";
					$sql="SELECT ads_content_link FROM ossn_game_ads WHERE id=".$adsid;
					if($resultr=mysqli_query($db, $sql)){
						$row=mysqli_fetch_array($resultr);
					 	unlink($_SERVER['DOCUMENT_ROOT'].$row['ads_content_link']);
						$sql="UPDATE ossn_game_ads SET ads_title='".$adstitle."', ads_link='".$adslink."', 
				  			  ads_description='".$adsdecription."', ads_content_link='".$link."', ads_content_type='".$type."', updated=".time();
						if(!mysqli_query($db,$sql)){
							echo "Error creating record: " . mysqli_error($db);
						}
						else{
							header('Location: add_ads.php?nads=u&&adsid=');
						}
					}
				}
				else{
					echo "new";
					$sql="INSERT INTO ossn_game_ads (postedbyid, ads_title, ads_link, ads_description, ads_content_link, ads_content_type, created)
					  	VALUES ('".$user->guid."','".$adstitle."','".$adslink."','".$adsdecription."','".$link."','".$type."','".time()."')";
					if(!mysqli_query($db,$sql)){
						echo "Error creating record: " . mysqli_error($db);
					}
					else{
						header('Location: index.php');
					}
				}
				}
				
			}
			else{
				echo "UNSUPORTED FILE TYPE";
			}
		}
		
		else if($adsid!=''  && ($_FILES["adsfile"]["error"] > 0) ){
			$sql="UPDATE ossn_game_ads SET ads_title='".$adstitle."', ads_link='".$adslink."', 
				  ads_description='".$adsdecription."', updated=".time();
			if(mysqli_query($db, $sql)){
				echo "Updated Successfully";
				header("location: add_ads.php?nads=u&&adsid=");
			}
			else
			   echo "Updation Failed:".mysqli_error($db);
		
		}
		
		else{
			echo "Return Code: " . $_FILES["adsfile"]["error"] . "<br />";
		}
	}
?>