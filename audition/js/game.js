function popLoginForm(){
	$("#open-login").click();
	$(".modal-backdrop").css("display" , "none");
}
function dis(data){
	//alert();
	$('#results-allposts').children().show();
	if(data=='all')
		$('#results-allposts').children().show();
	else
		$('#results-allposts > div').not('.'+data).hide()
}
var interestid;
function popSendInterest(userid){
	//alert("sendinterest");
	interestid=userid;
	$(".send-status").html("");
	$('#fullname').val("");
	$('#number').val("");
	$('#email').val("");
	$('#address').val("");
	$('#description').val("");
	$("#open-interest").click();
	$(".modal-backdrop").css("display" , "none");
}
function sendInterest(){
	flag=true;
	if($('#fullname').val()=='' ||	$('#number').val()==''	||	$('#email').val()=='')
		flag=false;
	if(interestid!='' && flag){
		$("#send-interest-btn").attr("disabled", true);
		$('#send-interest-load').show();
		var res='';
		$.ajax({
			url: "sendInterest.php",
			type: "post",
			data: {userid:interestid},
			success: function(response){
						//alert(response);
						res=response;
						if(res!="0"){
							var fullname=$('#fullname').val();
							var contact_number=$('#number').val();
							var email=$('#email').val();
							var address=$('#address').val();
							var description=$('#description').val();
							var subject="Someone is interested in your profile";
							var msg="<tr><td>Name:</td><td>"+fullname+"</td></tr><tr><td>Contact No:</td><td>"+contact_number+"</td></tr><tr><td>Email:</td><td>"+email+"</td></tr>"+"<tr><td>Address:</td><td>"+address+"</td></tr>"+"<tr><td>Description:</td><td>"+description+"</td></tr>";
							$.ajax({
								url: "http://services.snogol.net/MailServices/send_mail.php",
								type: "post",
								crossDomain:true,
								data: {subject:subject, msg:msg, email:res},
								success: function(response){
											//alert(response);
											$('#send-interest-load').hide();
											$("#send-interest-btn").attr("disabled", false);
											$(".send-status").html(response+"<br><br>");
										}
							});
						}
					}
		});
	}
	else if($('#fullname').val()=='')
		$('#fullname').focus();
	else if($('#number').val()=='')
		$('#number').focus();
	else if($('#email').val()=='')
		$('#email').focus();
}
function submitLogin(){
	$("#login-btn").attr("disabled", true);
	$('#signin-load').show();
	var uname=$("#uname").val();
	var passwd=$("#passwd").val();
	//alert(uname+passwd);
	if(uname!='' && passwd!=''){
		//alert("ajax");
		$.ajax({
        	url: "validate.php",
        	type: "post",
        	data: {username:uname, password:passwd},
			
			success: function(data){
            			if(data=='tr'){
							//alert("inside");
							/*$('#signin-load').hide();
							$("#login-btn").attr("disabled", false);*/
							location.reload();
						}
						else{
							$('#signin-load').hide();
							$("#login-btn").attr("disabled", false);
							$(".login-error").html("Your Login Name or Password is invalid");
						}
					 },
			error:function(req, err,error){
				alert(req.status);
			}
    	});
	}
	else{
		$('#signin-load').hide();
		$("#login-btn").attr("disabled", false);
		$(".login-error").html("Your Login Name or Password is invalid");
	}
}

function typeKeydown(event){
	if($('input[name=lang]:checked').val()=="mal")
		toggleKBMode(event);
}

function typeKeypress(event){
	if($('input[name=lang]:checked').val()=="mal")
		javascript:convertThis(event);
}


function toggle(e) {
	alert("toggle");
              toggleKBMode(e);
              
 }
function convert(e){
	alert("convert");
              convertThis(e);
 }

function HandleFileButtonClick(){
    document.getElementById('file').click();
	return false;
 }



function deleteGame(id){
	var r= confirm("Do you really want to delete the game?");
	if(r== false){
	}
	else{
	request = $.ajax({
        url: "delete.php",
        type: "post",
        data: {id:id},
		success: function(response){
                   // alert(response);
					location.href="games.php?nads=f&&ngm=f";
                }
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
			
        );
		
    });
	}
}

function likethispost(postid, gameid){
	$("span").css("pointer-events", "none");
	buttonvalue=$("#likebutton"+postid).val();
	//alert(buttonvalue);
	$("#likebutton"+postid).attr('disabled', true);
	 //$("loading").css('display' , 'block');
	request = $.ajax({
        url: "like.php",
        type: "post",
        data: {postid:postid, gameid:gameid, btnvalue:buttonvalue},
		success: function(data){
					//alert(data);
					//location.reload();
					var res = $.parseJSON(data);
					var likes=res.likes;
					var comments=res.comments;
					//alert(data);
					//alert(comments);
					if(likes==1)
						$("#ajlk"+postid).text("( 1 Like ");
					else
						$("#ajlk"+postid).text("( " + likes + " Like ");
					if(comments==1)
						$("#ajcm"+postid).text(" 1 Comment )");
					else
						$("#ajcm"+postid).text(" " + comments + " Comments )");
					if(buttonvalue=='Like'){
						$("#likebutton"+postid).attr("value", "Unlike");
						$("#d"+postid).html("Unlike");
					}
					if(buttonvalue=='Unlike'){
						$("#likebutton"+postid).attr("value", "Like");
						$("#d"+postid).html("Like");
					}
					$("#likebutton"+postid).attr('disabled', false);
					$("span").css("pointer-events", "auto");
					
                },
		error: function(err){
			alert("Oops!! Something went wrong");
		}
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
			
        );
		
    });
}
/*$(document).ready(function(){
    $(document).on('click', '.one', function(e) { 
	alert('one');
		if($("#jkl").val()!='neva'){ 
			var id=$(this).attr('id');
			var award = id.substr(id.length - 1);
			var gameid=$("#gameid").val();
			var postid=id.substr(0, id.length - 1);
			addAward(id, postid, award, gameid);
		}
			
    });
	$(document).on('click', '.two', function(e) {  
	alert('two');
		if($("#jkl").val()!='neva'){ 
			var id=$(this).attr('id');
			var award = id.substr(id.length - 1);
			var gameid=$("#gameid").val();
			var postid=id.substr(0, id.length - 1);
			addAward(id, postid, award, gameid);
		}
    });
	$(document).on('click', '.three', function(e) { 
	alert('three');
		if($("#jkl").val()!='neva'){ 
			var id=$(this).attr('id');
			var award = id.substr(id.length - 1);
			var gameid=$("#gameid").val();
			var postid=id.substr(0, id.length - 1);
			addAward(id, postid, award, gameid);
		}
    });
	$(document).on('click', '.four', function(e) { 
	alert('four'); 
		if($("#jkl").val()!='neva'){ 
			var id=$(this).attr('id');
			var award = id.substr(id.length - 1);
			var gameid=$("#gameid").val();
			var postid=id.substr(0, id.length - 1);
			addAward(id, postid, award, gameid);
		}
	});
	 $(document).on('click', '.rating .stars', function (event) {
		 var rate=$(this).val();
		 var postid=($(this).attr('id'));
		 var gameid=$("#gameid").val();
		 postid=postid.substr(5, postid.length-1);
		 alert("gameid: "+gameid+" , postid: "+postid+" , rate: "+rate);
		 request = $.ajax({
        		   	url: "rating.php",
       				type: "post",
        			data: {rate:rate, gameid:gameid, postid:postid},
					success: function(d){
						if(d=='0')
							alert("You have already rated this post");
                	},
					error: function(error){
						alert(error);
					}
    	});
		
        $(this).attr("checked");
     });
	/* $(document).ajaxStart(function(){
        $('html, document').css("cursor", "wait");
    });
    $(document).ajaxComplete(function(){
        ('html, document').css("cursor", "auto");
    });*/
	
	
	
//});*/
var postidGl;

function addAward(id,postid, award, gameid){
	request = $.ajax({
        url: "award.php",
        type: "post",
        data: {postid:postid, award:award, gameid:gameid},
		success: function(response){
					//alert(response);
					if(response=="added") 
						$('#img'+id).attr('src', 'img/'+award+'.png');
					if(response=="removed") 
						$('#img'+id).attr('src', 'img/'+award+award+'.png');
					
                }
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
			
        );
		
    });

}
var pstid;
function showComments(postid, ajaxcall, toggle){
	//alert("show comments: "+postid);
	/*if($('#panel'+postid).is(':visible')){
		//alert('visible');
		ajaxcall=false;
	}*/
	$('#panelloader'+postid).fadeIn("slow");
	var gameid=$("#gameid").val();
	//alert(gameid);
	if(postid!='' && gameid!='' && ajaxcall){
		console.log("showComment");
		$.ajax({
        url: "fetchcomment.php",
        type: "post",
        data: {postid:postid, gameid:gameid},
		success: function(response){
					//alert(response);
					
					//$("#panel"+postid).css("display" , "block");
					$("#panel"+postid).html(response);
					//show=false;
					
                }
    	});
	}
	if(toggle)
		$("#panel"+postid).slideToggle("fast");
}
function closePop(){
	$("#cmntcontainer").css('display', 'none');
}

function postThisComment(postid){
	
	var postid=postid.substr(1,postid.length-1);
	$("#comment-loading"+postid).fadeIn("slow");
	$("#c"+postid).attr('readonly','readonly');
	//alert(postid);
	$("#l"+postid).css("display" , "block");
	var comment=$("#c"+postid).val();
	//alert(comment);
	var gameid=$("#gameid").val();
	//alert(postidGl);
	if(comment!=''){
		$.ajax({
			url: "postcomment.php",
			type: "post",
			data: {postid:postid, gameid:gameid, comment:comment},
			success: function(data){
						$("#c"+postid).val("");
						var res = $.parseJSON(data);
						var likes=res.likes;
						var comments=res.comments;
						if($('#panel'+postid).is(':visible')){
							//alert('visibile');
							flag=true;
							toggle=false;
							showComments(postid, true, false);
						}
						//alert(data);
						//alert(comments);
						if(likes==1)
							$("#ajlk"+postid).text("( 1 Like ");
						else
							$("#ajlk"+postid).text("( " + likes + " Like ");
						if(comments==1)
							$("#ajcm"+postid).text(" 1 Comment )");
						else
							$("#ajcm"+postid).text(" " + comments + " Comments )");
						$("#comment-loading").fadeOut("slow");
						$("#c"+postid).prop('readonly', false);
					}
			            
   		 });
		 $("#l"+postid).css("display" , "none");
	}
}

function enableDisable(gameid){
	var flag=0;
	//alert($("#endis"+gameid).text().trim());
	if($("#endis"+gameid).text().trim()=="Enable"){
		flag=1;
	}
	$.ajax({
        url: "enable_disable.php",
        type: "post",
        data: {gameid:gameid, flag:flag},
		success: function(response){
					//alert(response);
					if(flag==0)
						$("#endis"+gameid).text("Enable");
					else
						$("#endis"+gameid).text("Disable");
                }
    });
}

function deletePost(postid, userid){
	var r= confirm("Do you really want to delete the this post?");
	if(r== false){
	}
	else{
		$.ajax({
        	url: "http://mychance.in/audition/deletePost.php",
        	type: "POST",
        	data: {postid:postid, userid:userid},
			success: function(response){
						//alert(response);
						$("#p"+postid).hide();
            },
			error:function(a,b,c){
				//alert(a.responseText);
			}
    	});
	}

}

function editAds(adsid){
	location.href="add_ads.php?nads=&&adsid="+adsid;
}
function deleteAds(adsid){
	var r= confirm("Do you really want to delete this Advertisement?");
	if(r== false){
	}
	else{
		$.ajax({
        	url: "deleteads.php",
        	type: "post",
        	data: {adsid:adsid},
			success: function(response){
						//alert(response);
						if(response==1)
							location.reload();
					
                	}
    	});
	}
}
function sharePost(postid){
	var r= confirm("Do you want to share this post?");
	if(r == false){
	}
	else{
		$.ajax({
        	url: "sharepost.php",
        	type: "post",
        	data: {postid:postid},
			success: function(response){
						alert(response);
					
                	}
    	});
	}
}
function deleteComment(cmntId, postid){
	$("span").css("pointer-events", "none");
	var userid=$("#c"+cmntId).val();
	//alert(userid);
	$.ajax({
        	url: "deletecomment.php",
        	type: "post",
        	data: {commentid:cmntId, userid:userid, postid:postid},
			success: function(response){
            		 	if(response!="0"){							
							$("#d"+cmntId).hide();
							
							var res = $.parseJSON(response);
							var likes=res.likes;
							var comments=res.comments;
							if($('#panel'+postid).children(':visible').length == 0) {
							  $('#panel'+postid).html('<div class="comment-username">No comments</div>')
							}
							//alert(data);
							//alert(comments);
							if(likes==1)
								$("#ajlk"+postid).text("( 1 Like ");
							else
								$("#ajlk"+postid).text("( " + likes + " Like ");
							if(comments==1)
								$("#ajcm"+postid).text(" 1 Comment )");
							else
								$("#ajcm"+postid).text(" " + comments + " Comments )");
							$("span").css("pointer-events", "auto");
						}
						else
							alert(response);
					 }
    	});
}
function editGame(id){
	$("#editid").val(id);
	$('.game-update-loading').fadeIn("slow");
	if(id!=''){
		$.ajax({
        	url: "editgame.php",
        	type: "post",
        	data: {gameid:id},
			success: function(data){
            			var res = $.parseJSON(data);
    					//alert(res.name+"  "+res.description);
						$("#gamename").val(res.name);
						$("#gamedescription").val(res.description);
						$('.game-update-loading').css("display", "none");
						$("#BackToTop").click();
					 }
    	});
	}
}
/*********** Handle Clicks ********/
function clickLike(id){
	$("#likebutton"+id).click();
}

function clickFile(){
	$("#file").click();
}
function alertfilename(){
	//alert($("#file").val());
	var string = $("#file").val();
	var result = string.substring(string.lastIndexOf("\\") + 1);
	//alert(result);
	$("#fileuploadname").html(result);
}

function dataDismisal(){
	$(".pop-content").html("");
	$(".pop-textarea").Attr("readonly","readonly");
	$(".btn-edit").css("display","none");
}
function editContent()
{
	$(".pop-textarea").removeAttr('readOnly');
	$(".pop-textarea").focus();
	$(".btn-edit").css("display","block");
}

function openPopup(postid){
	//alert("working");
	if (this.play == true) {
		this.pause();
		//alert('music paused');
	} 
	$(".btn-edit").css("display","none");
	$("#open-popup").click();
	$(".pop-content").html('<div class="popup-loading" style="text-align:center;"><img src="img/gameupdate.gif"></div>');
	if(postid!=''){
		$.ajax({
        	url: "popup.php",
        	type: "post",
        	data: {postid:postid},
			success: function(data){
						//alert(data);
            			$(".pop-content").html(data);
						$(".modal-backdrop").css("display", "none");
						$(".btn-edit").css("display","none");
					 }
    	});
	}
}

function likeThisPost(postid){
	$("span").css("pointer-events", "none");
	$.ajax({
        url: "like.php",
        type: "post",
        data: {postid:postid},
		success: function(data){
					var res = $.parseJSON(data);
					var likes=parseInt(res.like);
					if(likes==1)
						$("#lke"+postid).html("1 Like ");
					else if(likes==0)
						$("#lke"+postid).html("");
					else
						$("#lke"+postid).html(likes+" People like this");
					var label = res.op;
					$('#likebutton'+postid).html(label);
					$("span").css("pointer-events", "auto");
				},
		error: function(err){
			alert("Oops!! Something went wrong");
		}
    });
}

function showLikedPeople(postid){
	var siteUrl = $('#site_url').val();
	siteUrl = siteUrl + "others/ring.gif";
	$('.liked-users-list').html("<div align='center'><img src="+siteUrl+"><p style='font-weight:bold'>Loading</p></div>");
	$('#likeModalOpen').click();
	$.ajax({
        url: "likeList.php",
        type: "post",
        data: {postid:postid},
		success: function(data){
					$('.liked-users-list').html("");
					var res = $.parseJSON(data);
					for(var i =0; res[i]!=null; i++){
						var html = "";
						html = html+'<li style="padding:5px"><a href="'+res[i]["pUrl"]+'"><img src="'+res[i]["profile"]+'"style="display:inline-block"><p style="display:inline-block; font-size:15px; font-weight:600;padding-left: 20px;">'+res[i]["fullname"]+'</p></a></li>';
						$('.liked-users-list').append(html);
					}
				},
		error: function(err){
			alert("Oops!! Something went wrong");
		}
    });
}
