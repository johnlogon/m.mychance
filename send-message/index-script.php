<?php
	require_once('db_connect.php');
	require('init.php');
	session_start();
   	if(isset($_SESSION['username'])){
		if($_SESSION['username']=='invalidate'){
			session_destroy();
			header('Location:'.$ossn_site_url."action/logout");
		}
		else{
			$sql="SELECT * FROM ossn_users WHERE username='".$_SESSION['username']."'";
			$result=mysqli_query($db, $sql);
			if($result){
				$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
				$_SESSION['login_userid']=$row['guid'];
				//$_SESSION['userid']=$row['guid'];
				$_SESSION['login_user'] = $row['username'];
				$_SESSION['usertype'] = $row['type'];
				$_SESSION['login_session_type'] =$row['type'];
				$_SESSION['ERROR'] = '';
				$_SESSION['userfullname']=$row['first_name']." ".$row['last_name'];
				 //header("location: games.php?nads=f&&ngm=f");
			}
			else
				echo mysqli_error($db);
		}
	}
?>
<!DOCTYPE html>
<html>
	<head>
    	<link rel="stylesheet" href="css/message.css">
		<style type="text/css">
				body{ 
						font-family: 'lucida grande', tahoma, verdana, arial, sans-serif;
				}
				.contentArea{
						width:600px;
						margin:0 auto;
				}
				#inputSearch
				{
						width:350px;
						border:solid 1px #000;
						padding:3px;
				}
				#divResult
				{
						position:absolute;
						width:350px;
						display:none;
						margin-top:-1px;
						border:solid 1px #dedede;
						border-top:0px;
						overflow:hidden;
						border-bottom-right-radius: 6px;
						border-bottom-left-radius: 6px;
						-moz-border-bottom-right-radius: 6px;
						-moz-border-bottom-left-radius: 6px;
						box-shadow: 0px 0px 5px #999;
						border-width: 3px 1px 1px;
						border-style: solid;
						border-color: #333 #DEDEDE #DEDEDE;
						background-color: white;
				}
				.display_box
				{
						padding:4px; border-top:solid 1px #dedede; 
						font-size:12px; height:50px;
				}
				.display_box:hover
				{
						background:#3bb998;
						color:#FFFFFF;
						cursor:pointer;
				}
				.sender-name-outer:hover{
					background-color:#3bb998;
					cursor:pointer;
				}
		</style>
    	<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>
    	<script type="text/javascript" src="js/message.js"></script>
        <script type="text/javascript">
			/*$(document).ready(function() {
				$(".message-display-wrapper").scroll(function() {
					if ($(".message-display-wrapper").scrollTop() + $(".message-display-wrapper").height() > $(".message-display-wrapper").height() && !busy && again) {
						busy = true;
						offset = limit + offset;
						displayMoreMessages(limit, offset);
						console.log("called");
					}
				});
			});
	
			function displayMoreMessages( lim, off) {
			//alert(gameid);
        	$.ajax({
          		type: "GET",
          		async: false,
         		url: "getmoremessages.php",
          		data: "limit=" + lim + "&offset=" + off + "&from_id=" + fromid_g, 
          		cache: false,
          		beforeSend: function() {
           			$("#loader_message").html("").hide();
            		$('#loader_image').show();
          		},
          		success: function(html) {
					$(".message-display-wrapper").append(html);
					if (html == "") {
						again = false;
						$("#loader_message").html('<button data-atr="nodata" class="btn btn-default" type="button">No more records.</button>').show()
					} else {
					$("#loader_message").html('<button class="btn btn-default" type="button">Loading please wait...</button>').show();
					}
					window.busy = false;
					//showAds();
					
				}
        	});
		}*/
			$(document).ready(function(e) {
      			displaySenderList();
				
				var myVar = setInterval(myTimer, 3000);
				var t=0;
				var ajaxBusy=false;
				
				function myTimer(){
					//console.log(t++);
					$.each($('.message-outer'), function() {
						var from_id=(this.id).substring(7, (this.id).length);
						//console.log(from_id);
						var lastMessageId=$('#messagedisplay'+from_id+' div:last').attr('id');
						if (lastMessageId==undefined)
							lastMessageId=0;
						console.log(lastMessageId);
						if(ajaxBusy==false){
							ajaxBusy=true;
							$.ajax({
								type: "POST",
								async: false,
								url: "checknewmessage.php", 
								data:{from_id:from_id, lastmessageid:lastMessageId},
								cache: false,
								success: function(html) {
									//alert(html);
									console.log(html);
									if(html!=''){
										$('#messagedisplay'+from_id).append(html);
										$('.notification-sound').click();
										$('#messagedisplay'+from_id).scrollTop($('#message'+from_id).scrollTop(1000));
									}
								},
							});
							ajaxBusy=false;
						}
					});
				}
            });
			function displaySenderList() {
				//alert(gameid);
				$.ajax({
					type: "POST",
					async: false,
					url: "find-friends.php",
					data:{friendlist:1}, 
					cache: false,
					success: function(html) {
						$(".results-sender-list").append(html);
					},
				});
			}
			$(document).on('click', '.notification-sound', function(){
				this.play();
			})
        </script>
	</head>
	<body>
    	<div class='send-message-wrapper' style="text-align:center; border:groove">
        	<form id="send-message" action="uploadmessage.php" method="post" enctype="multipart/form-data">
            	<label>To</label>
                <div class="contentArea">
                	<input type="text" class="search" id="inputSearch" autocomplete="off" />
                    <div id="divResult">
                    </div>
                </div>
                <label>Select File</label>
                <input type="file" name="file" id="file"><br/><br><br>
                <input type="hidden" id="selectedId" name="touserid" value="">
                <input type="hidden" id="selectedName" name="tousername" value="">
                <button type="submit" id="send-btn" name="send-btn">Send</button>
            </form>
        </div>
        <div class="results-sender-list" style="float:right;border: groove;padding: 20px;">
        </div>
        <div class="message-display-wrapper" style="margin-top:10px;display:none; max-height:450px;">
        </div>
        <div id="message-notification" style="display:none">
        	<audio controls class='notification-sound' style='width:320px'>
                <source src='resources/audio/pling.mp3' type=audio/mp3>
             </audio>
        </div>
    </body>
</html>
    