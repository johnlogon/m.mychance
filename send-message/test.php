<?php
	session_start();
	include_once('../classes/OssnSession.php');
	include_once('../classes/OssnBase.php');
	include_once('../classes/OssnDatabase.php');
	include_once('../classes/OssnEntities.php');
	include_once('../classes/OssnUser.php');
	include_once('../components/OssnChat/classes/OssnChat.php');
	
	
	$chat = new OssnChat;
	$user = new OssnUser;
	$user ->guid = $_SESSION['login_userid'];
	$friends = $chat->getOnlineFriends($user, '10');
	if ($friends) {
		foreach ($friends as $friend) {
			$friend = arrayObject($friend, 'OssnUser');
			$friend->fullname = $friend->first_name . ' ' . $friend->last_name;
			$vars['entity'] = $friend;
			$vars['icon'] = $friend->iconURL()->smaller;
			$have = 1;
			echo ossn_plugin_view('chat/friends-item', $vars);
		}
	}
	if ($have !== 1) {
		echo '<div class="ossn-chat-none">'.ossn_print('ossn:chat:no:friend:online').'</div>';
	}
?>