<?php
	session_start();
	require_once('db_connect.php');
	require_once('constants.php');
	if(isset($_POST["send-btn"])){
		$touserid=$_POST['touserid'];
		$tousername=$_POST['tousername'];
		$fromuserid=$_SESSION['login_userid'];
		$fromuserfullname=$_SESSION['userfullname'];
			if (!($_FILES["file"]["error"] > 0)){
				/** IF PHOTO/VIDEO/AUDIO IS SUBMITTED **/
			 
				$allowedExts = array("webm", "mp4", "WEBM", "MP4", 
									 "gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF",
									 "mp3", "wav", "MP3", "WAV");
				$photoExts = array("gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
				$videoExts = array("mpeg", "avi", "mp4", "MPEG", "AVI", "MP4");
				$audioExts = array("mp3", "wav", "MP3", "WAV");
				$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

				if ((($_FILES["file"]["type"] == "video/mp4")
					|| ($_FILES["file"]["type"] == "video/webm")
					|| ($_FILES["file"]["type"] == "image/gif")
     	 			|| ($_FILES["file"]["type"] == "image/jpeg")
      	 			|| ($_FILES["file"]["type"] == "image/jpg")
      	 			|| ($_FILES["file"]["type"] == "image/pjpeg")
      	 			|| ($_FILES["file"]["type"] == "image/x-png")
      	 			|| ($_FILES["file"]["type"] == "image/png")
					|| ($_FILES["file"]["type"] == "audio/mp3")
					|| ($_FILES["file"]["type"] == "audio/mpeg")
					|| ($_FILES["file"]["type"] == "audio/wav"))
					&& in_array($extension, $allowedExts)){
						$dir;
						$type;
						if(in_array($extension, $photoExts)){
							$dir=$messagephoto;
							$type='photo';
						}
						else if(in_array($extension, $videoExts)){
							$dir=$messagevideo;
							$type='video';
						}
						else if(in_array($extension, $audioExts)){
							$dir=$messageaudio;
							$type='audio';
						}
						
  						if ($_FILES["file"]["error"] > 0){
    						echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    					}
  						else{
							$temp = explode(".", $_FILES["file"]["name"]);
							$newfilename = round(microtime(true)) . '.' . end($temp);
							//move_uploaded_file($_FILES["video"]["tmp_name"], $targetdir . $newfilename);
      						move_uploaded_file($_FILES["file"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].$dir .  $newfilename);
      						//echo "Stored in: " . $_SERVER['DOCUMENT_ROOT'].$dir. $_FILES["file"]["name"];
							$link=$dir .  $newfilename;
							$sql="INSERT INTO ossn_multimedia_messages(fromuserid, touserid, fromuserfullname, touserfullname, message_type, link,created)
								  VALUES (".$fromuserid.",".$touserid.",'".$fromuserfullname."','".$tousername."','".$type."','".$link."', '".time()."')";
							if(!mysqli_query($db,$sql)){
								echo "Error creating record:line 61 " . mysqli_error($db);
							}
							else{
								header('Location: '.$ossn_site_url.'/send-message');
							}
    					}
  				}
				else{
 					echo "Invalid file";
  				}
		 	}
		}
?>