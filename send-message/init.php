<?php
	require_once('db_connect.php');
	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$messageaudio)) {
    	mkdir($_SERVER['DOCUMENT_ROOT'].$messageaudio, 0777, true);
	} 
	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$messagevideo)) {
    	mkdir($_SERVER['DOCUMENT_ROOT'].$messagevideo, 0777, true);
	} 
	if (!file_exists($_SERVER['DOCUMENT_ROOT'].$messagephoto)) {
    	mkdir($_SERVER['DOCUMENT_ROOT'].$messagephoto, 0777, true);
	} 
	
	$sql= "ALTER TABLE ossn_messages
		ADD fromuserfullname VARCHAR(50),
		ADD touserfullname VARCHAR(50),
		ADD message_type VARCHAR(50) DEFAULT NULL,
		ADD link VARCHAR(500) DEFAULT NULL";
	if(!mysqli_query($db,$sql)){
		/*echo mysqli_error($db);
		die();*/
	}
	
	$sql= "CREATE TABLE IF NOT EXISTS ossn_chat_settings (
		id bigint(20) NOT NULL AUTO_INCREMENT,
  		userid INT(11) NOT NULL,
		chatSound INT(2) DEFAULT NULL,
		enterToSend INT(2) DEFAULT NULL,
		invisible INT(2) DEFAULT NULL,
  		PRIMARY KEY (id))ENGINE=MyISAM  DEFAULT CHARSET=utf8";
	if(!mysqli_query($db,$sql)){
		echo  mysqli_error($db);
		die();
	}
?>