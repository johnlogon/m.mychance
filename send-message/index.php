<?php
	require_once('db_connect.php');
	require('init.php');
	session_start();
   	if(isset($_SESSION['username'])){
		if($_SESSION['username']=='invalidate'){
			unset($_SESSION['username']);
			$_SESSION['redirect_to'] = "send-message";
			header('Location:'.$ossn_site_url."login");
		}
		else{
			$sql="SELECT * FROM ossn_users WHERE username='".$_SESSION['username']."'";
			$result=mysqli_query($db, $sql);
			if($result){
				$row=mysqli_fetch_array($result, MYSQLI_ASSOC);
				$_SESSION['login_userid']=$row['guid'];
				//$_SESSION['userid']=$row['guid'];
				$_SESSION['login_user'] = $row['username'];
				$_SESSION['usertype'] = $row['type'];
				$_SESSION['login_session_type'] =$row['type'];
				$_SESSION['ERROR'] = '';
				$_SESSION['userfullname']=$row['first_name']." ".$row['last_name'];
				 //header("location: games.php?nads=f&&ngm=f");
			}
			else
				echo mysqli_error($db);
		}
	}
	else
		header('location: $ossn_site_url');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<!-- Mirrored from themeandphoto.com/taplivedemos/2014/09/15/bootstrap-chat-example/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 May 2016 07:56:58 GMT -->
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <!--[if IE]>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <![endif]-->
    <title>MESSAGES</title>
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
    <link href="assets/css/bootstrap.css" rel="stylesheet" />
    <link href="css/message.css" rel="stylesheet"/>
  	
    <style>
		.sender-name-outer:hover{
			background-color:#CCC;
			cursor:pointer;
		}
		.upload-icon{
			float: right;
			margin-right: 72px;
			margin-top: -21px;
			position: relative;
			z-index: 2;
			color: #400080;
			font-size: 15px;
			cursor:pointer;
			font-size:1.5em;
		}
	</style>
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!--<script type="text/javascript" src="js/jquery-1.12.3.min.js"></script>-->
	<script type="text/javascript" src="../send-message/js/message.js"></script>
    <script type="text/javascript">
		$(document).ready(function(e) {
      			//displaySenderList();
				var getnewmessage = setInterval(newmessage, 5000);
				/*var newmessagenotification = setInterval(notification, 5000);
				var onlinecheck = setInterval(onlinecheck, 20000);
				var updatelastactivity=setInterval(updateactivity, 20000);*/
				var t=0;
				var ajaxBusy=false;
				
				function newmessage(){						
						if ($('#message-list li:last').attr('data-userid')!=undefined) {
							var lastMessageId=$('#message-list li:last').attr('id');
							var from_id=$('#'+lastMessageId).attr('data-userid');
							var temp=$('#message-list li:last').attr('data-userid');
							var userpic=$('#user-profile-pic'+temp).attr('src');
							//alert(userpic);
							//alert(from_id);
							if (lastMessageId==undefined)
								lastMessageId=0;
							//console.log(lastMessageId);
							if(ajaxBusy==false){
								ajaxBusy=true;
								//console.log("1 - newMessage");
								$.ajax({
									type: "POST",
									async: true,
									url: "checknewmessage.php", 
									data:{from_id:from_id, lastmessageid:lastMessageId, userpic:userpic},
									cache: false,
									success: function(html) {
										//alert(html);
										//console.log(html);
										if(html!=''){
											//console.log(html);
											$('#message-list').append(html);
											$('.notification-sound').click();
												//alert('1');
											$('#message-box').scrollTop(2000);
										}
									},
									complete: function(){
										ajaxBusy=false;
										notification();
									}
								});
								
							}
							
						}
						else{
							notification();
						}
						
				}
				function notification(){					
					if(ajaxBusy==false){
							ajaxBusy=true;
							//console.log("2 - notification")
							$.ajax({
									type: "GET",
									async: true,
									url: "notification.php", 
									cache: false,
									success: function(data) {
										if(data!=''){
											var result=$.parseJSON(data);
											//console.log(result);
											
											for(var i=0; result[i]!=undefined; i+=2){
												
												//if($('#messagecount'+result[i]).css('display')=='none'){
													//console.log(result[i+1]);
													if($('#messagecount'+result[i]).html()!=result[i+1]){
														$('.notification-sound').click();
														//alert('2')
													}
													$('#messagecount'+result[i]).html(result[i+1]).css('display', 'table-cell');
														
												//}
											}
										}
									},
									complete: function(){
										ajaxBusy=false;
										onlinecheck();
									}
								});
					}
				}
				function onlinecheck(){
					if(ajaxBusy==false){
						var userids= new Array();
						$("ul.userul li").each(function(index, element) {
							userids.push($(this).attr('data-useridli'));
                        });
						ajaxBusy=true;
						//console.log("3 - onlinecheck");
						var jsonData = JSON.stringify(userids);
						$.ajax({
							type: "POST",
							async: true,
							url: "checkonlineusers.php", 
							data:{data: jsonData},
							cache: false,
							success: function(html) {
								var data= $.parseJSON(html)
								//console.log("online: "+data);
								var notonline=new Array();
								$("ul.userul li").each(function(index, element) {
									notonline.push($(this).attr('data-useridli'));
								});
								for(var i=0;data[i]!=undefined;i++){
									$('#user-profile-pic'+data[i]).css('border', '3px solid green');
									var removeItem = data[i];
									notonline=jQuery.grep(notonline, function(value) {
											return value != data[i];
									});
								}
								//console.log("offline: "+notonline);
								for(var j=0; notonline[j]!=undefined; j++){
									$('#user-profile-pic'+notonline[j]).css('border', '');
								}
								//alert(notonline);
							},
							complete: function(){
										ajaxBusy=false;
										updateactivity();
							}
						});
						
					}
					
				}
				function updateactivity(){
					if(ajaxBusy==false){
						//console.log("4 - updateactivity")
						$.ajax({
								type: "POST",
								async: true,
								url: "updatelastactivity.php", 
								cache: false,
								success: function(html) {
									//console.log(html);
								},
								complete: function(){
										ajaxBusy=false;
								}
						});
					}
				}
            });
			
			/*$(document).on('keyup', '.message-text', function(){
				console.log('enter pressed');
			})*/;
			var friendlimit=11;
			var friendoffset=0;
			var friendagain=true;
			var friendajaxbusy=false;
			var friendagain=true;
			
			$(document).ready(function(e) {
                displaySenderList();
				friendoffset=friendoffset+friendlimit;
				//console.log($('#friendlist-wrapper').height());
				$('#friendlist-wrapper').bind('scroll', function(){
					if($(this).scrollTop() + $(this).innerHeight()>=$(this)[0].scrollHeight){
					  //alert('end reached');
					  displaySenderList();
					  friendoffset=friendoffset+friendlimit;
					}
				 });
            });
			/*function friendlistScrollfunction(){
				var pos=$('#friendlist-wrapper').scrollTop();
				console.log(pos);
				if(pos>=$('#friendlist-wrapper').height()){
					displaySenderList();
					friendoffset=friendoffset+friendlimit;
				}
			}*/
			
			function displaySenderList() {
				//alert(gameid);
				if(!friendajaxbusy && friendagain){
					ajaxbusy=true;
					console.log("ajaxing");
					$.ajax({
						type: "GET",
						async: false,
						url: "getfriends.php",
						data:{limit:friendlimit, offset:friendoffset}, 
						cache: false,
						success: function(html) {
							if(html!='')
								$("#user-list").append(html);
							else
								friendagain=false;
						},
					});
					ajaxbusy=false;
					 
				}
			}
			$(document).ready(function(e) {
                //alert("progi");
				$.ajax({
					type: "POST",
					async: false,
					data:{start:1},
					url: "userpic.php",
					dataType:"text",
					cache: false,
					success: function(data) {
						//alert(data);
						$('#login-user-p').val(data);
					},
				});
            });
			$(document).ready(function(e) {
                $.ajax({
					type: "POST",
					async: false,
					data:{op:"read"},
					url: "chatSettings.php",
					dataType:"text",
					cache: false,
					success: function(data) {
						var result=$.parseJSON(data);
						$.each(result, function(key,value) {
							if(value=='1'){
								$("#"+key).prop('checked', true);
							}
							else{
								$("#"+key).prop('checked', false);
							}
							
						});
					},
				});
            });
			$(document).on('click', '.notification-sound', function(){
				if($("#chatSound").is(':checked')){
					this.play();
				}
			})
			$(document).on("click", "video", function(e) {
				if (this.paused == false) {
					  this.pause();
					  console.log('video paused');
				} 
			});
			$(document).ready(function(e) {
				$.ajax({
					type: "POST",
					async: false,
					url: "checklastsender.php",
					dataType:"text",
					cache: false,
					success: function(data) {
						//alert(data);
						$('#chatBox'+data).click();
					},
				});
            });
			
			$(document).on('keydown', '.message-text', function(e){
				if($("#enterToSend").is(':checked')){
					if((e.keyCode || e.which) == 13) {
						e.preventDefault();
						$('#submit-btn').click();
					}
				}
			})
			
    </script>

</head>
<body style="font-family:Verdana">
  <div id="fixedheader">
  		<div  align="center" id="headertext">
                WWW.MYCHANCE.IN
            <a href="<?php echo $ossn_site_url?>">
                <img src="img/s.png" width="30px" height="30px" class="pull-right home-logo" style="cursor:pointer;">
            </a>
        </div>
  </div>
  <div class="container">
        <div class="row " style="padding-top:40px;">
            <!--<h3 class="text-center" style="padding: 25px;"> MESSAGES </h3>-->
            <br /><br />
            <div class="col-md-8 message-display-wrapper">
            </div>
            <div class="col-md-4">
                  <div class="panel panel-primary">
                    <div class="panel-heading">
                       <span style="display: table-cell;">ONLINE USERS</span>
                       <div class="dropdown" style="display: table-cell; padding-left:55%">
                              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">
                              	<span class="glyphicon glyphicon-cog"></span>
                              </button>
                              <ul class="dropdown-menu">
                                <li><a href="#">Enter to send&nbsp;<input class="pull-right" type="checkbox" id="enterToSend" 
                                				onClick="saveSettings('enterToSend')"></a></li>
                                <li><a href="#">Sound&nbsp;&nbsp;<input class="pull-right" type="checkbox" 
                                				onClick="saveSettings('chatSound')" id="chatSound"></a></li>
                              </ul>
                        </div>
                    </div>
                    <div class="panel-body"  id="friendlist-wrapper" style="min-height:500px; max-height: 500px;overflow-y: scroll;" >
                        <ul class="media-list userul" id="user-list">
        
                            <!-- USERS WILL BE APPENDED HERE -->
        
                        </ul>	
                     </div>
                   </div>
                
            </div>
        </div>
		<input type="hidden" id="login-user-p" value="">

	<!-- MESSAGE MEDIA POP-UP -->
<button type="button" class="btn btn-info btn-lg" id="message-pop" data-toggle="modal" data-target="#myModal" style="display:none"></button>

 
  <div class="modal fade" id="myModal" role="dialog" style="">
    <div class="modal-dialog" style="width:auto">
    
      
      <div class="modal-content" style="width:100%">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body" id="message-body" style="text-align:center;background-color:black; width:100%; height:100%">
         
        </div>
        <div class="modal-footer">
        </div>
      </div>
      
    </div>
  </div>
  
  <!-- END OF POP-UP -->
<div id="message-notification" style="display:none">
    <audio controls class='notification-sound' style='width:320px'>
        <source src='resources/audio/pling.mp3' type=audio/mp3>
     </audio>
</div>
  </div>
  <div class="footer">
      <hr>
      	<span class="footer-text pull-left">
        	<a href="<?php echo $ossn_site_url;?>">
        		 © COPYRIGHT www.mychance.in
             </a>
         </span>
         <span class="footer-text-company pull-right">
         	<a href="http://www.snogol.net" target="_blank">
        		Designed By Snogol Software Solutions LLP
             </a>
          </span>
      </div>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58126461-1', 'auto');
  ga('send', 'pageview');

</script>
</body>

<!-- Mirrored from themeandphoto.com/taplivedemos/2014/09/15/bootstrap-chat-example/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 30 May 2016 07:57:03 GMT -->
</html>
