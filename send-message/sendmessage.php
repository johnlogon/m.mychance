<?php
	session_start();
	require_once('db_connect.php');
	if(isset($_POST) && !isset($_POST['cat'])){
		$fromuserid=$_SESSION['login_userid'];
		$fromuserfullname=$_SESSION['userfullname'];
		$msg_type='';
		$link='';
		$tousername='';
		$dir='';
		$type='';
		$time;
		$msg = '';
		$touserid=$_POST['toid'];
		$tousername=$_POST['touserfullname'];
		$textcontent=$_POST['textcontent'];
			if (!($_FILES["file"]["error"] > 0)){
				/** IF PHOTO/VIDEO/AUDIO IS SUBMITTED **/
			 
				$allowedExts = array("webm", "mp4", "WEBM", "MP4", 
									 "gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF",
									 "mp3", "wav", "MP3", "WAV");
				$photoExts = array("gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
				$videoExts = array("mpeg", "avi", "mp4", "MPEG", "AVI", "MP4");
				$audioExts = array("mp3", "wav", "MP3", "WAV");
				$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

				if ((($_FILES["file"]["type"] == "video/mp4")
					|| ($_FILES["file"]["type"] == "video/webm")
					|| ($_FILES["file"]["type"] == "image/gif")
     	 			|| ($_FILES["file"]["type"] == "image/jpeg")
      	 			|| ($_FILES["file"]["type"] == "image/jpg")
      	 			|| ($_FILES["file"]["type"] == "image/pjpeg")
      	 			|| ($_FILES["file"]["type"] == "image/x-png")
      	 			|| ($_FILES["file"]["type"] == "image/png")
					|| ($_FILES["file"]["type"] == "audio/mp3")
					|| ($_FILES["file"]["type"] == "audio/mpeg")
					|| ($_FILES["file"]["type"] == "audio/wav"))
					&& in_array($extension, $allowedExts)){
						if(in_array($extension, $photoExts)){
							$dir=$messagephoto;
							$type='photo';
						}
						else if(in_array($extension, $videoExts)){
							$dir=$messagevideo;
							$type='video';
						}
						else if(in_array($extension, $audioExts)){
							$dir=$messageaudio;
							$type='audio';
						}
						
  						if ($_FILES["file"]["error"] > 0){
    						echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    					}
  						else{
							date_default_timezone_set('Asia/Kolkata');
							$time=time();
							$temp = explode(".", $_FILES["file"]["name"]);
							$newfilename = round(microtime(true)) . '.' . end($temp);
							//move_uploaded_file($_FILES["video"]["tmp_name"], $targetdir . $newfilename);
      						move_uploaded_file($_FILES["file"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].$dir .  $newfilename);
      						//echo "Stored in: " . $_SERVER['DOCUMENT_ROOT'].$dir. $_FILES["file"]["name"];
							$link="http://".$_SERVER['SERVER_NAME'] . $dir .  $newfilename;
							if($type=='photo'){
								$msg = "<img id=message-photo src=$link width=100px height=100px>";
							}
							else if($type=='video'){
								$msg = "<video class=message
												preload=none controls>
													<source type=video/mp4 src=".$link.">
													<source type=video/webm src=".$link.">
											 </video>";
							}
							else if($type=='audio'){
								$msg = "<audio controls class=message>
												<source src=".$link." type=audio/mp3>
												<source src=".$link." type=audio/ogg>
												<source src=".$link." type=audio/mpeg>
												<source src=".$link." type=audio/wav>
												Your browser does not support Audio Playback.
											 </audio>";
							}
							$sql="INSERT INTO ossn_messages(message_from, message_to, message, viewed, time, fromuserfullname, touserfullname, message_type, link)
								  VALUES (".$fromuserid.",".$touserid.", '".$msg."', '0', '".$time."' , '".$fromuserfullname."','".$tousername."','".$type."','".$link."')";
							if(!mysqli_query($db,$sql)){
								echo "Error creating record: " . mysqli_error($db);
							}
							else{
								$html='';
								//$html=$html."<div class='sent-date' style='font-size:14px;'>".date('m/d/Y H:i:s',$time)."</div><div class='message-display' id='".mysqli_insert_id($db)."' style='float:right'>";
								
								$html="<li class='media' id='".mysqli_insert_id($db)."' data-userid='".$touserid."'>
										  <div class='media-body sender_message' onclick='openMessagePopup(".mysqli_insert_id($db).")' style='cursor:pointer'>
											  <div class='media message_dec_sender'>
												 <a class='pull-left' href='#'>";
										 $html=$html."<img class='media-object img-circle' width='40px' height='40px' src='".$_POST['loginuserp']."' />
												 </a>
												 <div class='media-body'>";
								
								if($type=='photo'){
									$html=$html."<img src='http://".$_SERVER['SERVER_NAME'].$link."' style='width:328px'>";
								}
								else if($type=='video'){
									$source="http://".$_SERVER['SERVER_NAME'].$link;
									$html=$html."<video style='height:200px; width:328px; margin:0 auto; background:black' preload='none' 
													poster='img/play.png' controls style='min-height:150px'>
														<source type='video/mp4' src='$source'>
														<source type='video/webm' src='$source'>
												 </video>";
								}
								else if($type=='audio'){
									$source="http://".$_SERVER['SERVER_NAME'].$link;
									$html=$html."<img src='img/audio.jpg' class='csr' style='height:200px;width:328px;'>
												<audio controls class='audio-player' style='width:328px;'>
													<source src='$source' type=audio/mp3>
													<source src='$source' type='audio/ogg'>
													<source src='$source' type=audio/mpeg>
													<source src='$source' type=audio/wav>
													Your browser does not support Audio Playback.
												 </audio>";
								}
								
								$html=$html."<br />
									
               								<small class=''>".$fromuserfullname." | ".date("d F Y H:i:s", $time)."</small>
                						 
                             </div>
         				</div>

     				</div>
					<hr />
				</li>";
								echo $html;
							}
    					}
  				}
				else{
 					echo "Invalid file";
  				}
		 	}
			else{
				$type='';
				date_default_timezone_set('Asia/Kolkata');
				$time=time();
				$sql="INSERT INTO ossn_messages(message_from, message_to, message, viewed, time, fromuserfullname, touserfullname, message_type, link)
								  VALUES (".$fromuserid.",".$touserid.", '$textcontent', '0' , '$time', '".$fromuserfullname."','".$tousername."','".$type."','".$link."')";
			  if(!mysqli_query($db,$sql)){
			  	echo "Error creating record: " . mysqli_error($db);
			  }
			  else{
				    $html='';
				  	$html="<li class='media'  id='".mysqli_insert_id($db)."' data-userid='".$touserid."'>
										  <div class='media-body sender_message'>
											  <div class='media message_dec_sender'>
												 <a class='pull-left' href='#'>";
										 $html=$html."<img class='media-object img-circle' style='max-width:40px;max-height:40px;min-width:40px;min-height:40px;'  src='".$_POST['loginuserp']."' />
												 </a>
												 <div class='media-body'>";
					$html=$html.$textcontent;
					$html=$html."<br />
               								<small class=''>".$fromuserfullname." | ".date("d F Y h:i:s A", $time)."</small>
                						 
                             </div>
         				</div>

     				</div>
					<hr />
				</li>";
					echo $html;
			  }
			}
		}
		
		
		
		
		
		
		
		
		
if(isset($_POST['cat']) && $_POST['cat']=='insp'){
		$messagefrom=$_POST['message_from'];
		$fromuserfullname=$_POST['message_from_name'];
		$messageto=$_POST['message_to'];
		$touserfullname = $_POST['message_to_name'];
	if (!($_FILES["file"]["error"] > 0)){
				/** IF PHOTO/VIDEO/AUDIO IS SUBMITTED **/
			 
				$allowedExts = array("webm", "mp4", "WEBM", "MP4", 
									 "gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF",
									 "mp3", "wav", "MP3", "WAV");
				$photoExts = array("gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
				$videoExts = array("mpeg", "avi", "mp4", "MPEG", "AVI", "MP4");
				$audioExts = array("mp3", "wav", "MP3", "WAV");
				$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

				if ((($_FILES["file"]["type"] == "video/mp4")
					|| ($_FILES["file"]["type"] == "video/webm")
					|| ($_FILES["file"]["type"] == "image/gif")
     	 			|| ($_FILES["file"]["type"] == "image/jpeg")
      	 			|| ($_FILES["file"]["type"] == "image/jpg")
      	 			|| ($_FILES["file"]["type"] == "image/pjpeg")
      	 			|| ($_FILES["file"]["type"] == "image/x-png")
      	 			|| ($_FILES["file"]["type"] == "image/png")
					|| ($_FILES["file"]["type"] == "audio/mp3")
					|| ($_FILES["file"]["type"] == "audio/mpeg")
					|| ($_FILES["file"]["type"] == "audio/wav"))
					&& in_array($extension, $allowedExts)){
						if(in_array($extension, $photoExts)){
							$dir=$messagephoto;
							$type='photo';
						}
						else if(in_array($extension, $videoExts)){
							$dir=$messagevideo;
							$type='video';
						}
						else if(in_array($extension, $audioExts)){
							$dir=$messageaudio;
							$type='audio';
						}
						
  						if ($_FILES["file"]["error"] > 0){
    						echo "Return Code: " . $_FILES["file"]["error"] . "<br />";
    					}
  						else{
							$time=time();
							$temp = explode(".", $_FILES["file"]["name"]);
							$newfilename = round(microtime(true)) . '.' . end($temp);
      						move_uploaded_file($_FILES["file"]["tmp_name"],$_SERVER['DOCUMENT_ROOT'].$dir .  $newfilename);
							$link="http://".$_SERVER['SERVER_NAME'] . $dir .  $newfilename;
							$link1= $dir .  $newfilename;
							$message='';
							if($type=='photo'){
								$message = "<img id=message-photo src=$link width=100px height=100px>";
							}
							else if($type=='video'){
								$message = "<video class=message
												preload=none controls>
													<source type=video/mp4 src=".$link.">
													<source type=video/webm src=".$link.">
											 </video>";
							}
							else if($type=='audio'){
								$message = "<audio controls class=message>
												<source src=".$link." type=audio/mp3>
												<source src=".$link." type=audio/ogg>
												<source src=".$link." type=audio/mpeg>
												<source src=".$link." type=audio/wav>
												Your browser does not support Audio Playback.
											 </audio>";
								
							}
							$sql="INSERT INTO ossn_messages(message_from, message_to, message, viewed, time, fromuserfullname, touserfullname,
							 message_type, link) VALUES (".$messagefrom.",".$messageto.", '".$message."', '0', '".$time."', '".$fromuserfullname."',
							 '".$touserfullname."', '".$type."', '".$link1."')";
							 $html = '<div class="message-sender" id="ossn-message-item-'.mysqli_insert_id($db).'">
										<div class="ossn-chat-text-data-right">
											<div class="ossn-chat-triangle ossn-chat-triangle-blue"></div>
											<div class="text">
												<div class="inner">
													'.$message.'
												</div>
											</div>
										</div>
									</div>';
							if(!mysqli_query($db,$sql)){
								echo "Error creating record: " . mysqli_error($db);
							}
							else{
								echo $html;
							}
    					}
  				}
				else{
 					echo "Invalid file";
  				}
		 	}
	
}
?>