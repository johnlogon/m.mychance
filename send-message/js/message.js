function searchFriends(){
	var searchString=$("#to-user").val();
	//console.log(searchString);
	if(searchString!=''){
		$.ajax({
        	url: "find-friends.php",
        	type: "post",
        	data: {searchString:searchString},
			success: function(data){
            			//alert(data);
						$("#friends").html(data);
					 }
    	});
	}
}
 $(function(){
    $('#to-user').change(function(){
        console.log($("#friends option[value='" + $('#to-user').val() + "']").attr('id'));
    });
});

$(function(){
	$(".search").keyup(function() { 
		var inputSearch = $(this).val();
		var dataString = 'searchword='+ inputSearch;
		if(inputSearch!=''){
			  $.ajax({
				  type: "POST",
				  url: "find-friends.php",
				  data: dataString,
				  cache: false,
				  success: function(html){
					  //alert(html);
					  $("#divResult").html(html).show();
				  }
			  });
		}
		return false;    
	});

	jQuery("#divResult").on("click",function(e){ 
		  var $clicked = $(e.target);
		  var $name = $clicked.find('.name').html();
		  var decoded = $("<div/>").html($name).text();
		  $('#inputSearch').val(decoded);
	});
	jQuery(document).on("click", function(e) { 
		  var $clicked = $(e.target);
		  if (! $clicked.hasClass("search")){
		  jQuery("#divResult").fadeOut(); 
		  }
	});
	$('#inputSearch').click(function(){
		  jQuery("#divResult").fadeIn();
	});
});

function saveId(id, usernames){
	//alert();
 $('#selectedId').val(id);
 $('#selectedName').val(usernames);
}


fromid_g='';
var busy = false;
var limit = 5;
var offset = 0;
var again = true;

function fetchmessages(from_id){
	//alert(from_id);
	if(from_id!=''){
		busy=true;
		otheruserfname=$('#user-name'+from_id).val();
		otheruserpic=$('#user-profile-pic'+from_id).attr('src');
		//alert(otheruserpic);
		fromid_g=from_id;
		$.ajax({
			  type: "GET",
			  url: "getmessages.php",
			  data: {	fromid:from_id, 
			  			offset:offset, 
						limit:limit,
						loginuserp:$('#login-user-p').val(),
						otheruserp:otheruserpic,
						otheruserfname:otheruserfname
					},
			  cache: false,
			  success: function(html){
				  //alert(html);
				  if(html!=''){
				  	//$('#messagedisplay'+from_id).prepend(html).show();
					$('#message-list').prepend(html).show();
					$('#message-box').scrollTop($('#message'+from_id).scrollTop()+8);
				  }
				  else{
					  again=false;
				  	$('#message-list').prepend("<li class='media' id='0' data-userid='"+from_id+"'><h6 align='center'><b>No more messages</b></h6>").show();
					
				  }
			  }
		});
		busy=false;
	}
	else
		alert("Oops! Something Went Wrong!");
}
function openChatbox(id){
	//alert($("#message"+id).attr('id'));
	if($("#message"+id).attr('id')==undefined){
		var fromuserid=id;
		var profilepic=$('#user-profile-pic'+id).attr('src');
		var sendername=$('#sendername'+id).html();
		//alert(profilepic);
		//alert(profilepic+"   "+sendername);
		//var html="<div class='message-outer' onscroll='scrollFunction("+id+")'  id='message"+id+"' style='display:inline-block;border:groove;max-height:350px;min-height: 350px;max-width:342px;overflow-y:scroll'><div class='sender-profilepic' id='profilepic"+id+"' style='display:inline-block'><img id='pic"+id+"' src='"+profilepic+"' width='25px' height='25px'></div><div class='sender-name' id='sendername"+id+"' style='display:inline-block'>"+sendername+"</div><hr/><div id='messagedisplay"+id+"'></div><div id='inputmsg"+id+"'><form class='uploadimage' name='chatBoxForm'  id='form"+id+"' action='' method='post' enctype='multipart/form-data'><input type='text' style='width:320px' name='textcontent' id='text"+id+"' autocomplete='off'><input type='file' id='file"+id+"' name='file'><button type='submit' id='send"+id+"'>Send</button><br/><br><br></div></div>";
		var html="<div class='panel panel-info'><div class='panel-heading'><img src='"+profilepic+"' class='media-object img-circle' style='width:30px; height:30px; display:inline-block'> <div style='display:inline-block'> "+sendername+"</div></div><div class='panel-body' id='message-box' onscroll='scrollFunction("+id+")' style='max-height: 500px;overflow-y: scroll;'><ul class='media-list' id='message-list'><!-- MESSAGES WILL BE APPENDED HERE	--></ul></div><div class='panel-footer'><div class='input-group' style='width:100%'><div id='inputmsg"+id+"' style='width:100%'><form class='uploadimage' name='chatBoxForm'  id='form"+id+"' action='' method='post' enctype='multipart/form-data'><textarea class='form-control message-text' name='textcontent' id='text"+id+"' autocomplete='off' placeholder='Enter Message' rows='3'  style='width:90%; resize:none' /><img src='img/358.gif' class='errspan-upload' style=''><span><button class='btn btn-info' id='submit-btn' type='submit'>SEND</button></span><i class='fa fa-camera-retro upload-icon' onclick='clickfile("+id+")' aria-hidden='true'></i><span id='fileuploadname' style='float:right'></span><input type='file' id='file"+id+"' name='file' style='display:none' onchange='alertfilename("+id+")'></div></div></div>";
		$('.message-display-wrapper').html(html).show();
		fetchmessages(id);
		$('#message-box').animate({scrollTop: 1000});
		$('#messagecount'+id).css('display', 'none');
		$('#messagecount'+id).html("");
	}
}
function clickfile(id){
	$('#file'+id).click();
}
/*function sendThismessage(toId){
	var data = new FormData();
	jQuery.each(jQuery('#file')[0].files, function(i, file) {
		data.append('file-'+i, file);
	});
	if(toId!=''){
		senderName=$("#sendername"+toId).html();
		alert(senderName);
		$.ajax({
			type: "POST",
			url: "sendmessage.php",
			data: data,
			cache: false,
			contentType: false,
			processData: false,
		    success: function(html){
			  
		    }
		});
	}
}*/
var sendingMessage = false;
$(document).on('submit', 'form', function(e){
	e.preventDefault();
	if(this.id!='' && !sendingMessage){
		toId=(this.id).substring(4, (this.id).length);
		//alert(('text'+toId));
		if($('#file'+toId).val()!='' || $('#text'+toId).val()!='' ){	
			var formData= new FormData(this);
			toUserFullname=$('#sendername'+toId).html().trim();
			otheruserpic=$('#user-profile-pic'+toId).attr('src');
			formData.append('toid', toId);
			formData.append('touserfullname', toUserFullname);
			formData.append('loginuserp', $('#login-user-p').val());
			if($('#file'+toId).val()!='')
				$('.errspan-upload').css('display', 'block');
			$.ajax({
				url: "sendmessage.php", 
				type: "POST",            
				data:formData,
				contentType: false,       
				cache: false,            
				processData:false,
				beforeSend: function(){
					sendingMessage = true;
					$('#submit-btn').attr('disabled', true);
				},
				success: function(data){
				 //alert(data);
				 $('#message-list').append(data);
				 $('#text'+toId).val("");
				 $('#message-box').scrollTop(2000);
				 $('#file'+toId).val("");
				 $("#fileuploadname").html("");
				 $('.errspan-upload').css('display', 'none');
				},
				error:function(error){
					//alert(error.status)
				},
				complete:function(){
					sendingMessage = false;
					$('#submit-btn').attr('disabled', false);
				}
			});
		}
	}
})

function scrollFunction(id){
	var pos=$('#message-box').scrollTop();
	//console.log(pos);
	if(pos==0){
		offset = limit + offset;
		if(busy==false && again==true)
			fetchmessages(id);
		//console.log(pos);
	}
}
function openMessagePopup(postid){
	//alert(potid);
	$('#message-pop').click();
	if(postid!=''){
		$.ajax({
			url: "messagepopup.php", 
			type: "GET",            
			data:{postid:postid},
			cache: false,                  
			success: function(data){
				// alert(data);
				 $('#message-body').html(data);
			},
			error:function(error){
				//alert(error.status)
			}
		});
	}
}

function alertfilename(id){
	//alert($("#file").val());
	var string = $("#file"+id).val();
	var result = string.substring(string.lastIndexOf("\\") + 1);
	//alert(result);
	$("#fileuploadname").html(result);
}

function selectThisli(id){
	$('#user-list-li'+id).css('background', 'goldenrod');
	$("ul.userul li").each(function(index, element) {
		if($(this).attr('id')!=('user-list-li'+id)){
			var liid=$(this).attr('id');
			$('#'+liid).css('background', '');
		}
	});
}

function saveSettings(category){
	var cat = category;
	var flag;
	if($("#"+category).is(':checked')){
		flag=1;
	}
	else{
		flag=0;
	}
	$.ajax({
		url: "chatSettings.php", 
		type: "POST",            
		data:{op: "save", cat:cat, value:flag},
		cache: false,                  
		success: function(data){
			//alert(data);
		},
		error:function(error){
			alert(error.status)
		}
	});
}
