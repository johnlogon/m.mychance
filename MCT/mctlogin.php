<?php
	require_once("oc-load.php");
	require_once('../system/start.php');
	require_once('constants.php');
	

	if(isset($_GET['username']) && isset($_GET['password'])){
		$username = $_GET['username'];
		$password = $_GET['password'];
		
		$obj = new OssnUser;
		$user = ossn_user_by_username($username);		
		$salt = $user->salt;
		$password = $obj->generate_password($password . $salt);
		
		if($password==$user->password){
			Session::newInstance()->session_start();
			Session::newInstance()->_set('userId', $user->guid);
			Session::newInstance()->_set('userName', $user->username);
			Session::newInstance()->_set('userEmail', $user->email);
			session_name('osclass');
			header("location:". $ossn_site_url."mct/");
		}
	}
?>