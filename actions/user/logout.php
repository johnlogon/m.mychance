<?php
/**
 * Open Source Social Network
 *
 * @package   (Informatikon.com).ossn
 * @author    OSSN Core Team <info@opensource-socialnetwork.org>
 * @copyright 2014 iNFORMATIKON TECHNOLOGIES
 * @license   General Public Licence http://www.opensource-socialnetwork.org/licence
 * @link      http://www.opensource-socialnetwork.org/licence
 */
 require_once("../../restCalls/constants.php");
 require_once("../../restCalls/error_log.php");
 $db = mysqli_connect($dbserver, $dbusername, $dbpassword, $dbdatabase);
 
 if(isset($_SESSION['OSSN_USER'])){
 	$user = $_SESSION['OSSN_USER'];
 	$username = $user->username;
 }
 else{
 	error_write("OSSN_USER SESSION EMPTY");
	//die();
 }
 if(isset($_SESSION['DEVICE_ID'])){
 	$device_id = $_SESSION['DEVICE_ID'];
	$sql = "DELETE FROM ossn_deviceids_socialmedia WHERE device_id='".$device_id."' AND username='".$username."'";
 	if(!mysqli_query($db, $sql)){
		//echo mysqli_error($db);
		//die();
		error_write(mysqli_error($db));
		//die();
	}
 }
 else{
	 error_write("DEVICE ID SESSION EMPTY");
	 //die();
 }
 //ossn_logout();
 ossn_logout();
