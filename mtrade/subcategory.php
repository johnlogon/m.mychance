<?php
	require_once('loader.php');
	require_once('data/subCategoryData.php');
?>

<!DOCTYPE html>
<html lang="en-US">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="user-scalable = yes">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MyChance Trade</title>
      <?php include_once('head.php') ?>      
   </head>
   <body  class="home page page-id-3896 page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php single-author">
      
      <?php require_once('header.php')?>
      <div class="ad-title">
      	<h2><?php echo $data['subcategory']; ?>
          <span class="ad-page-price">
          	<a><?php echo $data['count']; ?> ads found</a>
          </span>
       </h2>
      </div>
      <section class="container m-t-60">
           <div class="row">
              <div class="col-md-9 col-sm-12 col-xs-12">
                 <section id="ads-homepage" class="category-page-ads clearfix">
                    <div class="row">
                       <div class="tab-content">
                          <div class="tab-pane active fade in latest-ads-holder" id="Tlatest">
                             <div class="latest-ads-grid-holder">
                                <?php echo $data['ads']; ?>
                             </div>
                          </div>
                      </div>
                    </div>
                 </section>
              </div>
              <div class="col-md-3 col-sm-12 col-xs-12 classi_sidebar">
                 <div class="cat-widget custom-widget category-widget">
                    <div class="cat-widget-title">
                       <h3>CATEGORIES</h3>
                    </div>
                    <div class="cat-widget-content">
                       <ul>
                          <?php echo $data['categories']; ?>
                       </ul>
                    </div>
                 </div>
              </div>
           </div>
        </section>
        <?php require_once('foot.php'); ?>
     </body>
</html>