<?php
	require_once('loader.php');
	require_once('data/viewAdData.php');
?>

<!DOCTYPE html>
<html lang="en-US">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="user-scalable = yes">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MyChance Trade</title>
      <?php include_once('head.php') ?>      
   </head>
   <body  class="home page page-id-3896 page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php single-author">
      <?php require_once('header.php')?>
      <div class="ad-title">
      	<h2><?php echo $ad['title']; ?>
       </h2>
      </div>
      <section class="container m-t-60">
           <div class="row">
                
           <div class="col-md-9 col-sm-9 col-xs-12">
           <div class="single-slider">
              <img style="margin-bottom:-7px;width:100%;" src="<?php echo $ad['image']; ?>">
              <div class="single-ad-price">
                <img src="images/rs.png" style="width: 14px;margin-top: -2px;margin-right: 5px;"><?php echo $ad['price']; ?>						
              </div>
              <div class="clearfix"></div>
           </div>
           <input type="hidden" id="posted_by" value="<?php echo $ad['posted_by']; ?>">
           <?php if(!empty($ad['video'])){ ?>
               <div id="ab-video-text"><span><i class="fa fa-youtube-play"></i>Video</span></div>
               <div id="ab-video">
                  <div class="fluid-width-video-wrapper" style="padding-top: 56.2069%;">
                    <iframe width="854" height="480" src="<?php echo $ad['video'];?>" frameborder="0" allowfullscreen></iframe>
                  </div>
               </div>
           <?php }?>
           <div class="post-detail">
              <div class="detail-cat clearfix">
                 <div class="category-icon">
                    <div class="category-icon-box"><i class="fa fa-book"></i></div>
                 </div>
                 <a href="#"><?php echo $ad['title']; ?></a> 								
              </div>
              <hr />
              <div class="single-description">
                 <div class="description-title">DESCRIPTION </div>
                 <p><?php echo $ad['description']; ?></p>
                 <?php if($ad['price']!=''){ ?>
                 <p>
                    <h4 style="display: inline-block;">Price:</h4>&nbsp;
                    <span style="display: inline-block;">
                        <img src="images/rs.png" style="width: 8px;margin-top: -3px;margin-right: 5px;display: inline-block;">
                        <h4 style="display: inline-block;"><?php echo $ad['price']; ?>
                        </h4>
                    </span>
                  </p>
                 <?php } ?>
              </div>
              <div class="clearfix"></div>
           </div>
           <div class="clearfix"></div>
           <div class="author-info clearfix post-single">
              <div class="author-avatar">
                <a href="<?php echo ossn_site_url('u/'.$posted->username); ?>">
                 <img src="<?php echo ossn_site_url('avatar/'.$posted->username.'/'); ?>" style="border-radius: 12%;">
                </a>
              </div>
              <div class="author-detail-right clearfix">
                <span class="author-meta">
                 <i class="fa fa-location-arrow"></i>Location:&nbsp; <?php echo $ad['city']; ?></span>
                 <span class="author-meta">
                 <i class="fa fa-map-marker"></i>Address:&nbsp; <?php echo $ad['address']; ?></span>
                  <?php if($data['settings']['show_email']==1){ ?>
                     <span class="author-meta">
                     <i class="fa fa-phone"></i>Phone:&nbsp; <?php echo $data['settings']['phone']; ?></span>
                  <?php } ?>
                  <?php if($data['settings']['show_phone']==1){ ?>
                     <span class="author-meta">
                     <i class="fa fa-envelope"></i>Email:&nbsp; <?php echo $posted->email; ?>
                     </span>
                 <?php	} ?>
              </div>
              <div class="author-btn">
                 <span class="author-profile-ad-details"><a href="<?php echo ossn_site_url('u/'.$posted->username); ?>" class="button-ag large green">VIEW <?php echo $posted->first_name." ".$posted->last_name; ?>'s PROFILE</a></span>
              </div>
           </div>
           <div class="ad-detail-content">	    			
           </div>
           <?php if($user->guid!=$posted->guid){ ?>
               <div class="full author-form full-single">
                  <div class="pos-relative">
                     <h3>LEAVE MESSAGE TO <?php echo strtoupper($posted->first_name." ".$posted->last_name); ?></h3>
                     <div class="h3-seprator"></div>
                  </div>
                  <div id="contact-ad-owner-v2">
                     
                        <div class="col-md-12 col-xs-12">
                           <textarea class="form-control" placeholder="Write your message here..." name="comments" id="commentsText" cols="8" rows="5"></textarea>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <input type="hidden" id="testA" value="<?php echo $data['random']['a']; ?>">
                            <input type="hidden" id="testB" value="<?php echo $data['random']['b']; ?>">
                            <input type="hidden" id="testSign" value="<?php echo $data['random']['sign']; ?>">
                           <p class="humantest">
                              Human Test: Find the X &nbsp; &nbsp; 
                              <span>
                                <?php echo $data['random']['a']; ?>
                              </span> 
                              <span>
                                <?php echo $data['random']['sign']; ?>
                              </span> 
                              <span>
                                <?php echo $data['random']['b']; ?>
                               </span> =
                           </p>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                           <input type="text" placeholder="Your answer" name="humanTest" id="humanTest" value="" class="form-control">
                           <span id="contact-ad-owner-error" style="margin-left:25px;display:none"></span>
                        </div>
                        <div class="col-md-12 col-xs-12">
                           <input name="submitted" type="submit" value="Send Message" class="input-submit" onClick="sendMessage()">	
                        </div>
                     
                  </div>
               </div>
           <?php }
                if($user->type=='admin' || $user->guid==$posted->guid){
           ?>
                <button type="button" class="btn btn-danger" style="float:right;margin-top:10px;padding: 2px 5px; margin-left:5px" onClick="deleteAd(<?php echo $_GET['id'] ?>)">
                    <i class="fa fa-trash" aria-hidden="true"></i>&nbsp;
                </button>
             <?php }
                    if($user->guid==$posted->guid){
              ?>
                <a href="<?php echo "newAd.php?id=".$_GET['id'] ?>">
                    <button type="button" class="btn btn-success" style="float:right;margin-top:10px;padding: 2px 5px;">
                        <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp;
                    </button>
                </a>
           <?php 	}?>
           <div id="ad-comments">
              <div id="ad-comments">
              </div>
           </div>
        </div>
            <div class="col-md-3 col-sm-12 col-xs-12 classi_sidebar">
                 <div class="cat-widget custom-widget category-widget">
                    <div class="cat-widget-title">
                       <h3>CATEGORIES</h3>
                    </div>
                    <div class="cat-widget-content">
                       <ul>
                          <?php echo $data['categories']; ?>
                       </ul>
                    </div>
                 </div>
              </div>
           </div>
        </section>
        <?php require_once('foot.php'); ?>
      </body>
</html>