function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
function chooseSubCat(){
	var value = $('.mainCategory option:selected').val();
	if(value=='-1')
		return false;
	$('#cateID').css('border', '1px solid #eee');
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'newad-subcat', 'value':value},
		success: function(data){
			var obj = $.parseJSON(data);
			var html = "<option value='-1'>--Select Subcategory--</option>";
			for(var i = 0; obj[i]!=null; i++){
				var subcat=obj[i];
				html+="<option value='"+subcat['id']+"'>"+subcat['subcategory']+"</option>";
			}
			$('.subCategory').html(html);
		},
		error:function(req, err,error){
			
		}
	});
}
function changeSubcat(){
	var value = $('.mainCategory option:selected').val();
	if(value=='-1')
		return false;
	$('#subcatID').css('border', '1px solid #eee');
}

function updateSettings(which){
	var show_phone;
	var show_email;
	var phone;
	if(which=='email'){
		if($('.author-cc.show-email>i').hasClass('fa-times'))
			show_email=1;
		else
			show_email=0;
		
		if($('.author-cc.show-phone>i').hasClass('fa-times'))
			show_phone=0;
		else
			show_phone=1;
	}
	else if(which=='phone'){
		if($('.author-cc.show-phone>i').hasClass('fa-times'))
			show_phone=1;
		else
			show_phone=0;
		if($('.author-cc.show-email>i').hasClass('fa-times'))
			show_email=0;
		else
			show_email=1;
	}
	else if(which=='phone-num'){
		phone = $('.edit-phone-box').val();
		if($('.author-cc.show-phone>i').hasClass('fa-times'))
			show_phone=0;
		else
			show_phone=1;
		if($('.author-cc.show-email>i').hasClass('fa-times'))
			show_email=0;
		else
			show_email=1;
	}
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'settings', 'show_phone':show_phone,'show_email':show_email, 'phone':phone},
		success: function(data){
			
		},
		error:function(req, err,error){
			alert('er')
		},
		complete:function(){
			location.reload();
		}
	});
}
function editPhone(){
	$('.phone-text').hide();
	$('.edit-phone-box').css('display', 'inline-block').focus();
}
function editCategory(id){
	$('#cat_span'+id).hide();
	$('#cat'+id).css('display', 'inline-block').focus();
}
function updateCategory(id){
	var cat_name = $('#cat'+id).val();
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'cat-update', 'category':cat_name,'id':id},
		success: function(data){
			
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			location.reload();
		}
	});
}
function deletetCategory(id){
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'cat-delete', 'id':id},
		success: function(data){
			alert(data);
			var obj = $.parseJSON(data);
			window.location.href=window.location.href.substring(0,window.location.href.indexOf('?'))+"?msg="+obj.Message;
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
function newCategory(){
	$('.new-category').css('display', 'inline-block').focus();
	$('.close-new-cat').css('display', 'inline-block');
}
function closeNewCat(){
	$('.new-category').css('display', 'none');
	$('.close-new-cat').css('display', 'none');
}
function saveCategory(category){
	if(category=='')
		return false;
	clearError();
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'cat-add', 'category':category},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code=='201'){
				$('.add-cat-error').html(obj.Message)
								   .fadeIn(200)
								   .delay(5000)
								   .fadeOut();
								   
			}
			else
				window.location.href=window.location.href.substring(0,window.location.href.indexOf('?'))+"?msg="+obj.Message;
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
function clearError(){
	$('.add-cat-error').html('').fadeOut(200);
	$('.add-subcat-error').html('').fadeOut(200);
}
var category_html='';
function viewSubCategory(id){
	if(id=='')
		return false;
	category_html=$('.admin-wrapper').html();
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-view', 'id':id},
		success: function(data){
			var obj = $.parseJSON(data);
			var cat_name = obj.cat['category'];
			var html = '<h3 style="margin-top:-5px">'+cat_name.toUpperCase()+' > SUBCATEGORIES</h3><div class="h3-seprator"></div><span class="back-btn" onclick="backToCat()"><i class="fa fa-arrow-circle-left"></i>Back</span><div class="account-overview clearfix"><div class="col-xs-12 first author-avatar-edit-post p-30" style="padding-bottom:15px !important">';
			for(var i =0; obj.subc[i]!=null;i++){
				var $this = obj.subc[i];
				html+='<span class="author-details phone-number" id="subcat-wrapper'+$this['id']+'"><i class="fa fa-cog" style="background: '+getRandomColor()+';"></i><span style="display:inline-block" class="sub-category-text"  id="subcat_span'+$this['id']+'">'+$this['subcategory']+'</span><input type="text" class="form-control edit-subcategory-box" id="subcat'+$this['id']+'" value="'+$this['subcategory']+'" onblur="updateSubCategory('+$this['id']+','+$this['category_id']+')" onfocus="this.value=this.value"><span style="float:right"><i class="fa fa-pencil icon-cat" area-hidden="true" onclick="editSubCategory('+$this['id']+')"></i><i class="fa fa-times icon-cat" area-hidden="true" onclick="deletetSubCategory('+$this['id']+')"></i></span></span>';
			}
               	
           	html+= '<input type="text" class="form-control new-subcategory" placeholder="Subcategory Title |" onblur=saveSubCategory(this.value,'+obj.cat['id']+')><i class="fa fa-times close-new-subcat" onClick="closeNewSubCat()"></i><span class="add-subcat-error"></span><span style="float:right"><button type="button" id="add-subcategory" class="btn btn-primary add-subcategory" onClick="newSubCategory()"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add New</button></span></div></div>';
			
			$('.admin-wrapper').html(html);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
function editSubCategory(id){
	$('#subcat_span'+id).hide();
	$('#subcat'+id).css('display', 'inline-block').focus();
}
function updateSubCategory(id,cat_id){
	var subcat_name = $('#subcat'+id).val();
	if($('#subcat_span'+id).html()==subcat_name){
		clearEdit(id);
		return false;
	}
	else if(subcat_name==''){
		clearEdit(id);
		return false;
	}
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-update', 'subcategory':subcat_name,'id':id, 'category_id':cat_id},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code==200){
				$('#subcat_span'+id).html(subcat_name);
			}
			clearEdit(id);
			alert(obj.Message);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
function backToCat(){
	$('.admin-wrapper').html(category_html);
}
function clearEdit(id){
	$('#subcat_span'+id).show();
	$('#subcat'+id).hide();
}
function deletetSubCategory(id){
	if(id=='')
		return false;
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-delete', 'id':id},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code==200){
				$('#subcat-wrapper'+id).remove();
			}
			alert(obj.Message);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
function newSubCategory(){
	$('.new-subcategory').css('display', 'inline-block').focus();
	$('.close-new-subcat').css('display', 'inline-block');
}
function closeNewSubCat(){
	$('.new-subcategory').css('display', 'none');
	$('.close-new-subcat').css('display', 'none');
}
function saveSubCategory(subcategory, category_id){
	if(subcategory=='')
		return false;
	clearError();
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-add', 'subcategory':subcategory, 'category_id':category_id},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code==201){
				$('.add-subcat-error').html(obj.Message)
								   	  .fadeIn(500)
								      .delay(5000)
								      .fadeOut();
								   
			}
			else if(obj.code==200){
				$this=obj.data;
				if($this.length==0)
					location.reload();
				var html='<span class="author-details phone-number" id="subcat-wrapper'+$this['id']+'"><i class="fa fa-cog" style="background: '+getRandomColor()+';"></i><span style="display:inline-block" class="sub-category-text"  id="subcat_span'+$this['id']+'">'+$this['subcategory']+'</span><input type="text" class="form-control edit-subcategory-box" id="subcat'+$this['id']+'" value="'+$this['subcategory']+'" onblur="updateSubCategory('+$this['id']+','+$this['category_id']+')" onfocus="this.value=this.value"><span style="float:right"><i class="fa fa-pencil icon-cat" area-hidden="true" onclick="editSubCategory('+$this['id']+')"></i><i class="fa fa-times icon-cat" area-hidden="true" onclick="deletetSubCategory('+$this['id']+')"></i></span></span>';
				var ele = $('.col-xs-12.first.author-avatar-edit-post.p-30').find('.author-details.phone-number:last');
				if(ele.length)
					$('.col-xs-12.first.author-avatar-edit-post.p-30').find('.author-details.phone-number:last').after(html);
				else
					$('.col-xs-12.first.author-avatar-edit-post.p-30').find('.new-subcategory').before(html);
				$('.new-subcategory').val('');
				closeNewSubCat();
				
			}
			alert(obj.Message)
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
function deleteAd(id){
	if(id=='')
		return false;
	$.ajax({
		url: "../mtrade/xhr/xhr.php",
		type: "post",
		data: {'action':'ad-delete', 'id':id},
		success: function(data){
			var response = $.parseJSON(data);
			var url = window.location.href;
			if(response.code==200){
				if(url.indexOf('subcategory')>-1 || url.indexOf('category')>-1 )
					window.location.href+="&msg=Ad delete successfully"
				else
					window.location.href=response.url+"?Ad delete successfully";
			}
			else{
				if(url.indexOf('viewAd')>-1 )
					window.location.href+="&msg=Oops! Something went wrong."
				else
					window.location.href=response.url+"msg=?Ad delete successfully";
			}
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
	
}
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}

function sendMessage(){
	$('#contact-ad-owner-error').html('').hide();
	if(validateTest()){
		var message_to = $('#posted_by').val();
		var message = $('#commentsText').val();
		if(message_to=='')
			return false;
		if(message==''){
			$('#commentsText').focus();
			return false;
		}
		$.ajax({
			url: "../mtrade/xhr/xhr.php",
			type: "post",
			data: {'action':'contact-poster', 'message_to':message_to, 'message':message},
			success: function(data){
				alert(data);
				if(data==1){
					$('#contact-ad-owner-error').html('Message successfully sent')
												.css('color', 'green')
												.fadeIn(500)
												.delay(5000)
												.fadeOut()
												.css('color','red !important');
				}
				else{
					$('#contact-ad-owner-error').html('Message could not be sent!')
											.fadeIn(500)
											.delay(5000)
											.fadeOut();
				}
			},
			error:function(req, err,error){
				console.log('er');
				$('#contact-ad-owner-error').html('Message could not be sent!')
											.css('color', 'red')
											.fadeIn(500)
											.delay(5000)
											.fadeOut();
			},
			complete:function(){
				
			}
		});	
	}
	else{
		$('#contact-ad-owner-error').html('Your answer is wrong')
									.css('color', 'red')
									.fadeIn(500)
									.delay(5000)
									.fadeOut();
	}
}

function validateTest(){
	var a = parseInt($('#testA').val());
	var b = parseInt($('#testB').val());
	var ans = parseInt($('#humanTest').val());
	var sign = $('#testSign').val();
	switch (sign){
		case '+':
			if((a+b)==ans)
				return true;
			else
				return false;
		break;
		case '-':
			if((a-b)==ans)
				return true;
			else
				return false;
		break;
		case '*':
			if((a*b)==ans)
				return true;
			else
				return false;
		break;
		default:
			return false;
	}
}

/*
	validation
*/
$(document).on('submit', '#primaryPostForm', function(e){
	if($('#postTitle').val()==''){
		$($('#postTitle').focus())
		return false;
	}
	if($('#cateID option:selected').val()=='-1'){
		$('#cateID').css('border', '1px solid rgba(0, 115, 168, 0.45)');
		$('html, body').animate({
			scrollTop: ($("#cateID").offset().top) - 15
		}, 200);
		return false;
	}
	else{
		$('#cateID').css('border', '1px solid #eee');
	}
	if($('#subcatID option:selected').val()=='-1'){
		$('#subcatID').css('border', '1px solid rgba(0, 115, 168, 0.45)');
		$('html, body').animate({
			scrollTop: ($("#subcatID").offset().top) - 15
		}, 200);
		return false;
	}
	return true;
})
