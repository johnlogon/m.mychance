<section id="top-menu-block" class="header-v1">
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <section id="register-login-block-top">
                     <ul class="ajax-register-links inline">
                        <li class="last login">
                        <?php 
							if(ossn_isLoggedin())
                          		echo '<a href="'.ossn_add_tokens_to_url(ossn_site_url('action/user/logout')).'" class="" title="Logout"><i class="fa fa-sign-in"></i>Logout</a>';
							else
								echo '<a href="#" class="" title="Logint"><i class="fa fa-sign-in"></i>Login</a>';
						?>
                        </li>	
                     </ul>
                  </section>
               </div>
            </div>
         </div>
      </section>
<header id="navbar" class="header-v1">
   <div class="container">
      <div class="row">
         <div class="col-xs-12">
            <a class="logo pull-left" href="<?php echo ossn_site_url(); ?>" title="Home">
            <img src="<?php echo ossn_site_url('others/s.png'); ?>" alt="Logo" />
            </a>
            <div class="make-new-ad">
               <a href="newAd.php" ><i class="fa fa-plus-circle"></i>Make new ad</a>
            </div>
            <div class="menutrigger">
               <span class="icons-bar"></span>
               <span class="icons-bar"></span>
               <span class="icons-bar"></span>
            </div>
            <div id="version-two-menu" class="main_menu responsive-menu">
               <ul id="menu-main" class="menu">
                  <li id="menu-item-1322" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-1322">
                     <a href="<?php echo ossn_site_url('mtrade'); ?>">HOME</a>
                  </li>
                  <li id="menu-item-1321" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1321">
                  	<a href="<?php echo ossn_site_url('mtrade'); ?>">CATEGORIES</a></li>
               	  <li id="menu-item-1321" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1321">
                    <a href="<?php echo ossn_site_url('mtrade/profile.php'); ?>">PROFILE</a></li>
                  <?php if(ossn_isAdminLoggedin()){ ?>
                  	<li id="menu-item-1321" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1321">
                    <a href="<?php echo ossn_site_url('mtrade/admin.php'); ?>">Admin</a></li>
                  <?php } ?>
               </ul>
            </div>
         </div>
      </div>
   </div>
</header>