<?php
	require_once('loader.php');
	require_once('data/profileData.php');
?>

<!DOCTYPE html>
<html lang="en-US">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="user-scalable = yes">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MyChance Trade</title>
      <?php include_once('head.php') ?>      
   </head>
   <body  class="home page page-id-3896 page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php single-author">
      
      <?php require_once('header.php')?>
      <div class="ad-title">
      	<h2>PROFILE</h2>
      </div>
      <section class="container m-t-60">
   <div class="row">
      <div class="col-md-9 col-sm-12 col-xs-12">
        <div class="full">
          <div class="account-overview clearfix">
             <div class="col-sm-4 col-xs-12 first author-avatar-edit-post p-30">
                <img class="author-avatar" src="<?php echo ossn_site_url('avatar/'.$user_logged->username) ?>">
             </div>
             <div class="col-sm-8 col-xs-12 b-l-1 p-30">
                <span class="author-cc show-email">
                    <?php 
                        if($data['settings']['show_email']==1){
                            echo '<i class="fa fa-check" onClick=updateSettings("email"); title="Click to change settings"></i>';
                        }
                        else
                            echo '<i class="fa fa-times" style="background:red" onClick=updateSettings("email"); title="Click to change settings"></i>';
                    ?>
                    <span class="author-cc-detail">Show Phone in My Ads</span>
                </span>
                <span class="author-cc show-phone">
                    <?php 
                        if($data['settings']['show_phone']==1){
                            echo '<i class="fa fa-check" onClick=updateSettings("phone"); title="Click to change settings"></i>';
                        }
                        else
                            echo '<i class="fa fa-times" style="background:red" onClick=updateSettings("phone"); title="Click to change settings"></i>';
                    ?>
                    <span class="author-cc-detail">Show Email in My Ads</span>
                </span>
             </div>
          </div>
          <div class="clearfix">
             <h3>AUTHOR PROFILE</h3>
             <div class="h3-seprator"></div>
             <div class="full profile-content">
                <div class="col-xs-12 profile-list">
                   <span class="author-details phone-number"><i class="fa fa-phone"></i>Phone: 
                        <span style="display:inline-block" class="phone-text"><?php echo $data['settings']['phone'] ?></span>
                        <input type="text" class="form-control edit-phone-box" onfocus="this.value = this.value;" 
                            value="<?php echo $data['settings']['phone']; ?>" style="width:180px;display:none" onBlur="updateSettings('phone-num')">
                        <i class="fa fa-pencil edit-phone" area-hidden=true onClick="editPhone()"></i>
                   </span>
                   <span class="author-details">
                    <i class="fa fa-envelope"></i>Email: <?php echo $data['settings']['email'] ?></a></span>
                </div>
                
             </div>
          </div>
        </div>
        <div class="full">
          <h3>MY ADS</h3>
          <div class="h3-seprator"></div>
          <div class="full" style="margin-left: 0px; padding-top: 20px;">
              <?php echo $data['ads']; ?>
          </div>
        </div>
     </div>
     <div class="col-md-3 col-sm-12 col-xs-12 classi_sidebar">
         <div class="cat-widget custom-widget category-widget">
            <div class="cat-widget-title">
               <h3>CATEGORIES</h3>
            </div>
            <div class="cat-widget-content">
               <ul>
                  <?php echo $data['categories']; ?>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<?php require_once('foot.php'); ?>
  </body
</html>