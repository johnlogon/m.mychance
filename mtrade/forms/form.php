<?php
	require_once('../loader.php');
	if(isset($_POST)){
		try{
			$save = new MCTDAOSaveUpdate;
			if(isset($_POST['ads_id'])){
				$response = $save->updateAds($_POST);
			}
			else{
				$response=$save->saveAds($_POST);
			}
			if($response['error']!=1)
				header('location:'.ossn_site_url('mtrade/viewAd.php?id='.$response['ads_id']));
			else
				header('location:'.ossn_site_url('mtrade/newAd.php?msg=Oops! Something went wrong.&err='.$response['Message']));
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
?>