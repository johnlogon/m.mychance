<?php
	ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
	require_once('log.php');
	require_once('loader.php');
	require_once('data/homeData.php');
?>

<!DOCTYPE html>
<html lang="en-US">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="user-scalable = yes">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MyChance Trade</title>
      <?php require_once('head.php') ?>      
   </head>
   <body  class="home page page-id-3896 page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php single-author">
      <?php require_once('header.php')?>
      <section id="categories-homepage" class="cat-box-contain"  style="background-color:#ffffff;"  >
         <div class="container">
            <div class="section-header-style-1">
               <div class="row">
                  <div class="col-xs-12">
                     <h2 class="main-title">SERVICE CATEGORIES</h2>
                     <div class="main-sub-title">
                       					
                     </div>
                     <div class="h2-seprator"></div>
                  </div>
               </div>
            </div>
            <div class="row cat-wrapper" data-call="11">
               	<?php echo $data['data'] ?>
            </div>
            
         </div>
      </section>
      <?php require_once('foot.php'); ?>
   </body>
</html>