<?php
	require_once('loader.php');
	require_once('data/adminData.php');
?>

<!DOCTYPE html>
<html lang="en-US">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="user-scalable = yes">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MyChance Trade</title>
      <?php include_once('head.php') ?>      
   </head>
   <body  class="home page page-id-3896 page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php single-author">
      <?php require_once('header.php')?>
      <div class="ad-title">
      	<h2>ADMIN</h2>
      </div>
      <section class="container m-t-60">
       <div class="row">
         <div class="col-md-9 col-sm-12 col-xs-12 admin-wrapper" style="width:100% !important;">
            <h3 style="margin-top:-5px">CATEGORIES</h3>
                <div class="h3-seprator"></div>
            <div class="account-overview clearfix">
                <div class="col-xs-12 first author-avatar-edit-post p-30" style="padding-bottom:15px !important">
                    <?php echo $data['categories']; ?>
                </div>
            </div>
        </div>
       </div>
     </section>
     <?php require_once('foot.php'); ?>
</body>
</html>