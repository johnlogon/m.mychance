<?php
	require_once('loader.php');
	require_once('data/newAdData.php');
?>

<!DOCTYPE html>
<html lang="en-US">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
   <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="user-scalable = yes">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MyChance Trade</title>
      <?php include_once('head.php') ?>      
   </head>
   <body  class="home page page-id-3896 page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php single-author">
      
      <?php require_once('header.php')?>
      <div class="ad-title">
      	<h2>New Ad
       </h2>
      </div>
      <section class="container m-t-60">
   <div class="row">
      <div class="col-md-9 col-sm-12 col-xs-12">
         
         <div class="col-sm-9 col-xs-12 first ad-post-main">
   
   <div id="upload-ad" class="ad-detail-content">
      <form class="form-item row" action="forms/form.php" id="primaryPostForm" method="POST" enctype="multipart/form-data">
         <div class="col-xs-12">
            <h3 style="margin-top:-6px;">
            	<?php if(isset($_GET['id']) && $_GET['id']!=''){
							echo 'EDIT AD';
					  }
					  else 
					  		echo 'MAKE NEW AD';
				?>
            </h3>
            <div class="h3-seprator"></div>
         </div>
         <?php if(isset($_GET['id']) && $_GET['id']!='')
		 	echo '<input type="hidden" id="hidden_id" name="ads_id" value="'.$_GET['id'].'">';
			echo '<input type="hidden" id="hidden_adsid" name="id" value="'.$edit['id'].'">';
		 ?>
         <div class="col-xs-12">
         </div>
         <div class="col-sm-6 col-xs-12" style="width:100% !important">
            <input type="text" id="postTitle" name="title" placeholder="Ad Title Goes here |" 
            	size="60" maxlength="255" class="form-control" required value="<?php echo $edit['title'] ?>">
         </div>
         <div class="col-sm-6 col-xs-12 form-s-select" style="width:100% !important">
            <select  name="cat" id="cateID" class="mainCategory" 
            	style="display: block; width:100%; margin-bottom:10px; border-radius:60px;height:44px;border:1px solid #eee" onChange="chooseSubCat()">
               <option value="-1">--Select Category--</option>
               <?php
			   		foreach($categories as $category){
						if($category['id']==$edit['category_id']){
							echo '<option class="level-0" value="'.$category['id'].'" selected="selected">'.$category['category'].'</option>';
						}
						else{
							echo '<option class="level-0" value="'.$category['id'].'">'.$category['category'].'</option>';
						}
					}
			   ?>
               
            </select>
            <?php if(isset($_GET) && !empty($subcategories)){?>
                <select  name="subcat_id" id="subcatID" class="subCategory" 
                        style="display: block; width:100%; margin-bottom:10px; border-radius:60px;height:44px;border:1px solid #eee" onChange="changeSubcat()">
                   <option value="-1">--Select Subcategory--</option>
                   <?php
                   		foreach($subcategories as $sub){
							if($sub['id']==$edit['subcat_id']){
								echo '<option class="level-0" value="'.$sub['id'].'" selected="selected">'.$sub['subcategory'].'</option>';
							}
							else{
								echo '<option class="level-0" value="'.$sub['id'].'">'.$sub['subcategory'].'</option>';
							}
					}
				   ?>
                </select>
            <?php }
				  else{
				  		?>
							<select  name="subcat_id" id="subcatID" class="subCategory" 
                                    style="display: block; width:100%; margin-bottom:10px; border-radius:60px;height:44px;border:1px solid #eee" onChange="changeSubcat()">
                               <option value="-1">--Select Subcategory--</option>
                            </select>
						<?php
				  }
			?>
            
         </div>
         
         
         <div class="extra-fields-container">
         </div>
         <div class="col-sm-6 col-xs-12">
            <input type="number" id="post_price" name="price" onkeypress="return isNumber(event)" placeholder="Price" size="30" class="form-control" value="<?php echo $edit['price'] ?>">
            
         </div>
         <div class="col-sm-6 col-xs-12 form-s-select">
            <input type="text" id="post_location" name="city" placeholder="Location" size="30" class="form-control" value="<?php echo $edit['city'] ?>">
         </div>
          <div class="col-xs-12 form-s-select">
            <input type="text" id="post_location" name="address" placeholder="Address" size="30" class="form-control" value="<?php echo $edit['address'] ?>">
         </div>
         <div class="col-xs-12">
            <fieldset class="input-title" style="margin-bottom:0px;">
               <textarea style="height: 150px !important;;border:1px solid #cecdcd" name="description" id="video" cols="8" rows="5" placeholder="Ad Description Goes here |"><?php echo $edit['description'] ?></textarea>
            </fieldset>
         </div>
         <?php if(!empty($edit['image'])){?>
         	<div class="col-xs-12">
         		<img style="width:100%" src="<?php echo $edit['image'];?>">
                <input type="hidden" name="image" value="<?php echo $edit['image'];?>"
            </div>
         <?php }?>
         <div class="col-xs-12" align="center">
           <div class="jfiler-holder">
              <div class="jFiler jFiler-theme-dragdropbox">
                 <input type="file" name="file" id="filer_input"  multiple style="position: absolute; left: -9999px; top: -9999px; z-index: -9999;">
                 <div class="jFiler-input-dragDrop" onClick="openFileChoose()">
                    <div class="jFiler-input-inner">
                       <div class="jFiler-input-text">
                          <h4>Click here to upload files</h4>
                       </div>
                       <a class="jFiler-input-choose-btn" onClick="">Browse Images</a>
                    </div>
                 </div>
              </div>
           </div>
        </div>
         <div class="col-xs-12">
            <fieldset class="input-title" style="margin-bottom:0px;">
               <textarea name="video" id="video" cols="8" rows="2" placeholder="Put video url here."><?php echo $edit['video'] ?></textarea>
               <p class="help-block">Add video embedding URL here (youtube only)</p>
            </fieldset>
         </div>
         
         <div class="col-xs-12">
            <div class="publish-ad-button">
               <button class="btn form-submit full-btn" id="edit-submit" name="op" value="" type="submit">
				<?php 
					if(isset($_GET['id']))
						echo "Save Ad";
					else
						echo "Publish Ad";
				?>               
               </button>
            </div>
         </div>
      </form>
   </div>
</div>
    </div>
      <div class="col-md-3 col-sm-12 col-xs-12 classi_sidebar">
         <div class="cat-widget custom-widget category-widget">
            <div class="cat-widget-title">
               <h3>CATEGORIES</h3>
            </div>
            <div class="cat-widget-content">
               <ul>
                  <?php echo $data['categories']; ?>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<?php require_once('foot.php'); ?>
      </body>
</html>