	
	<link rel="profile" href="http://gmpg.org/xfn/11">
      <link rel="pingback" href="xmlrpc.php">
      <link rel="shortcut icon" href="wp-content/uploads/2014/08/favicon.png" type="image/x-icon" />
      <script type="text/javascript">
         var _gaq = _gaq || [];
         _gaq.push(['_setAccount', '']);
         _gaq.push(['_setDomainName', 'none']);
         _gaq.push(['_setAllowLinker', true]);
         _gaq.push(['_trackPageview']);
         
         (function() {
         	var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
         	ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
         	var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
         })();
         
      </script>
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.8-fix/jquery.nicescroll.min.js"></script>
      <link rel='dns-prefetch' href='http://ajax.googleapis.com/' />
      <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
      <link rel='dns-prefetch' href='http://netdna.bootstrapcdn.com/' />
      <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' />
      <link rel="alternate" type="application/rss+xml" title="MyChance Trade - classified ads wordpress theme &raquo; Feed" href="feed/index.html" />
      <link rel="alternate" type="application/rss+xml" title="MyChance Trade - classified ads wordpress theme &raquo; Comments Feed" href="comments/feed/index.html" />
      <link rel="alternate" type="application/rss+xml" title="MyChance Trade - classified ads wordpress theme &raquo; HOME V1 – LayerSlider 2 Comments Feed" href="home-v1-layerslider-2/feed/index.html" />
     
      
      <link rel='stylesheet' id='dashicons-css'  href='wp-includes/css/dashicons.min1c9b.css?ver=4.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='thickbox-css'  href='wp-includes/js/thickbox/thickbox1c9b.css?ver=4.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='layerslider-css'  href='wp-content/plugins/LayerSlider/static/css/layerslider9dff.css?ver=5.3.2' type='text/css' media='all' />
      <link rel='stylesheet' id='ls-google-fonts-css'  href='http://fonts.googleapis.com/css?family=Lato:100,300,regular,700,900%7COpen+Sans:300%7CIndie+Flower:regular%7COswald:300,regular,700&amp;subset=latin%2Clatin-ext' type='text/css' media='all' />
      <link rel='stylesheet' id='contact-form-7-css'  href='wp-content/plugins/contact-form-7/includes/css/styles8686.css?ver=4.5.1' type='text/css' media='all' />
      <link rel='stylesheet' id='nextend_fb_connect_stylesheet-css'  href='wp-content/plugins/nextend-facebook-connect/buttons/facebook-btn1c9b.css?ver=4.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='nextend_twitter_connect_stylesheet-css'  href='wp-content/plugins/nextend-twitter-connect/buttons/twitter-btn1c9b.css?ver=4.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-postratings-css'  href='wp-content/plugins/wp-postratings/postratings-cssc936.css?ver=1.83' type='text/css' media='all' />
      <link rel='stylesheet' id='buttons-css'  href='wp-includes/css/buttons.min1c9b.css?ver=4.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='mediaelement-css'  href='wp-includes/js/mediaelement/mediaelementplayer.min51cd.css?ver=2.22.0' type='text/css' media='all' />
      <link rel='stylesheet' id='wp-mediaelement-css'  href='wp-includes/js/mediaelement/wp-mediaelement.min1c9b.css?ver=4.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='media-views-css'  href='wp-includes/css/media-views.min1c9b.css?ver=4.6.1' type='text/css' media='all' />
      <link rel='stylesheet' id='imgareaselect-css'  href='wp-includes/js/imgareaselect/imgareaselect3bf4.css?ver=0.9.8' type='text/css' media='all' />
      <link rel='stylesheet' id='classiads-fonts-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A400%2C700%2C400italic%2C700italic%257CLato%3A400%2C700%2C900italic&amp;subset=latin%2Clatin-ext' type='text/css' media='all' />
      <link rel='stylesheet' id='genericons-css'  href='wp-content/themes/classiads/fonts/genericons0778.html?ver=2.09' type='text/css' media='all' />
      <link rel='stylesheet' id='classiads-style-css'  href='wp-content/themes/classiads/stylee6e4.css?ver=2014-07-18' type='text/css' media='all' />
      
      <link rel='stylesheet' id='boostrat-style-css'  href='wp-content/themes/classiads/css/bootstrap95b8.css?ver=2.3.2' type='text/css' media='all' />
      <link rel='stylesheet' href='css/font-awesomefa0c.css'/>
      <link rel="stylesheet" href="css/style.css" />
      <link rel="stylesheet" href="css/scroll.css" />
      <link rel='stylesheet' id='boostrat-chosen-css'  href='wp-content/themes/classiads/css/chosen.min68b3.css?ver=1' type='text/css' media='all' />
      <link rel='stylesheet' id='bxslider-style-css'  href='wp-content/themes/classiads/css/jquery.bxslider68b3.css?ver=1' type='text/css' media='all' />
      <link rel='stylesheet' id='owl-carousel-css'  href='wp-content/themes/classiads/css/owl.carousel68b3.css?ver=1' type='text/css' media='all' />
      <link rel='stylesheet' id='main-style-custom-css'  href='wp-content/themes/classiads/css/custom68b3.css?ver=1' type='text/css' media='all' />
      <link rel='stylesheet' id='main-fancybox-css'  href='wp-content/themes/classiads/css/fancybox68b3.css?ver=1' type='text/css' media='all' />
      <link rel='stylesheet' id='boostrat-style-responsive-css'  href='wp-content/themes/classiads/css/bootstrap-responsive95b8.css?ver=2.3.2' type='text/css' media='all' />
      <link rel='stylesheet'  href='wp-content/themes/classiads/css/jFiller.css' type='text/css' />
      <link rel='stylesheet' id='redux-google-fonts-redux_demo-css'  href='http://fonts.googleapis.com/css?family=Roboto+Slab%3A700%7CMontserrat%3A700%7CRaleway%3A700%2CNormal%7CRoboto%3A300%7CLato%3ANormal&amp;ver=1479991719' type='text/css' media='all' />
      <script type='text/javascript' src='wp-includes/js/jquery/jqueryb8ff.js?ver=1.12.4'></script>
      <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min330a.js?ver=1.4.1'></script>
      <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/greensockcd11.js?ver=1.11.8'></script>
      <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/layerslider.kreaturamedia.jquery9dff.js?ver=5.3.2'></script>
      <script type='text/javascript' src='wp-content/plugins/LayerSlider/static/js/layerslider.transitions9dff.js?ver=5.3.2'></script>
      <script type='text/javascript'>
        
         var userSettings = {"url":"\/classiads-new\/","uid":"0","time":"1480063943","secure":""};
        
      </script>
      <script type='text/javascript' src='wp-includes/js/utils.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/plupload/plupload.full.mincc91.js?ver=2.1.8'></script>
   	  <script type="text/javascript" src="js/script.js"></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/horizental/slye6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/fancybox.packe6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/fitvidse6e4.js?ver=2014-07-18'></script>
      
      <script type="text/javascript" src="js/script.js"></script>
      
      <link rel='https://api.w.org/' href='wp-json/index.html' />
      <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
      <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
      <meta name="generator" content="WordPress 4.6.1" />
      <link rel="canonical" href="index.html" />
      <link rel='shortlink' href='index.html' />