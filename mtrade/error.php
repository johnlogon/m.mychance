<?php
	require_once('loader.php')
?>

<!DOCTYPE html>
<html lang="en-US">
   <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="user-scalable = yes">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>MyChance Trade</title>
      <?php require_once('head.php') ?>      
   </head>
   <body  class="home page page-id-3896 page-template page-template-template-homepage-v1 page-template-template-homepage-v1-php single-author">
      <?php require_once('header.php')?>
      <section id="categories-homepage" class="cat-box-contain"  style="background-color:#ffffff;"  >
         <div class="container">
            <div class="section-header-style-1">
               <div class="row">
                  <div class="col-xs-12">
                     <h2 class="main-title">ERROR</h2>
                     <div class="main-sub-title">
                     </div>
                     <div class="h2-seprator"></div>
                     	<img src="images/error.jpg" style="width:100%;margin-top:-40px;margin-bottom: 10px;">
                        <div class="view-more-btn" style="margin-bottom: 15px;">
                        	<div class="more-btn-inner" style="background: #e96969;">
                        		<a href="<?php echo ossn_site_url('mtrade');?>" style="color:white">
                        			<span>Go Back To Home</span>
                        		</a>
                        	</div>
                        </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php require_once('foot.php'); ?>
   </body>
</html>