<?php
	/*
		Author  : Suhail;
		Created : 26-Nov-2016;
	*/
	
	class MCTDAOSaveUpdate{
		public function connect(){
			$conn = new mysqli(MY_HOST,MY_USER, MY_PWD, MY_DB);
			return $conn;
		}
		private function logger($log){
			$file = $_SERVER['DOCUMENT_ROOT']."/mtrade/log.txt";
			$logFile = fopen($file, 'a');
			$time= $this->getTimeStamp();
			$log.="time:".date('d-m-Y h:i:s A', $time)."\n";
			fwrite($logFile, $log);
			fclose($logFile);
		}
		private function setTimeZone(){
			date_default_timezone_set('Asia/Kolkata');
		}
		private function getTimeStamp(){
			$this->setTimeZone();
			$time = time();
			return $time;
		}
		public function saveCategory($name){
			$mysqli = $this->connect();
			$sql = "INSERT INTO mct_categories (category) VALUES (?)";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('s', $name);
			if($stmt->execute())
				return true;
			return false;
		}
		public function updateCategory($params){
			$mysqli = $this->connect();
			$sql = "UPDATE mct_categories SET category=? WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('si', $params['category'], $params['id']);
			if($stmt->execute())
				return true;
			return false;
		}
		public function saveSubCategory($params){
			$mysqli = $this->connect();
			$sql = "INSERT INTO mct_subcategories (subcategory, category_id) VALUES (?,?)";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('si', $params['subcategory'], $params['category_id']);
			if($stmt->execute())
				return true;
			return false;
		}
		public function updateSubCategory($params){
			$mysqli = $this->connect();
			$sql = "UPDATE mct_subcategories SET subcategory=?,category_id=? WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('sii', $params['subcategory'], $params['category_id'], $params['id']);
			if($stmt->execute())
				return true;
			return false;
		}
		public function saveAds($params){
			$mysqli = $this->connect();
			$response = $this->saveImage();
			if($response['error']==1)
				return $response;
			if($response['error']!=2)
				$params['image'] = $response['filename'];
			$sql = "INSERT INTO mct_ads (subcat_id, created) VALUES (?,?)";
			$stmt = $mysqli->prepare($sql);
			if(!$stmt)
				$this->logger($mysqli->error);
			$time = $this->getTimeStamp();
			$rc = $stmt->bind_param('is', $params['subcat_id'], $time);
			if(!$rc)
				$this->logger($stmt->error);
			if($stmt->execute()){
				$ads_id = $stmt->insert_id;
				$params['ads_id'] = $ads_id;
				if($this->saveAdsDetails($params)){
					$response['error']=0;
					$response['ads_id']=$ads_id;
					return $response;
				}
			}
			return false;
		}
		private function saveImage(){
			if (empty($_FILES['file']['name'])) {
				$response['error']=2;
				return $response;
			}
			$response = array();
			if (!($_FILES["file"]["error"] > 0)){
				$photoExts = array("gif", "jpeg", "jpg", "png", "JPG", "PNG" , "JPEG" , "GIF");
				$extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				if ((($_FILES["file"]["type"] == "image/gif")
     	 			|| ($_FILES["file"]["type"] == "image/jpeg")
      	 			|| ($_FILES["file"]["type"] == "image/jpg")
      	 			|| ($_FILES["file"]["type"] == "image/pjpeg")
      	 			|| ($_FILES["file"]["type"] == "image/x-png")
      	 			|| ($_FILES["file"]["type"] == "image/png"))
					&& in_array($extension, $photoExts)){
						$time=time();
						$temp = explode(".", $_FILES["file"]["name"]);
						$newfilename = round(microtime(true)) . '.' . end($temp);
						move_uploaded_file($_FILES["file"]["tmp_name"], $_SERVER['DOCUMENT_ROOT']."/mtrade/wp-content/uploads/".  $newfilename);
						$response['error'] = 0;
						$newfilename=ossn_site_url("mtrade/wp-content/uploads/".$newfilename);
						$response['filename']=$newfilename;
						return $response;
				}
				else{
					$response['error'] = 1;
					$response['Message'] = "Invalide file";
					return $response;
				}
			}
			else{
				$response['error'] = 1;
				$response['Message'] = "Invalide file";
				return $response;
			}
		}
		private function saveAdsDetails($params){
			$mysqli = $this->connect();
			$sql = "INSERT INTO mct_ads_details 
					(ads_id,title,description,country,city,address,image,video,price,posted_by) 
					 VALUES (?,?,?,?,?,?,?,?,?,?)";
			$stmt = $mysqli->prepare($sql);
			$posted_by = ossn_loggedin_user()->guid;
			$stmt->bind_param('issssssssi', $params['ads_id'],$params['title'],
										  $params['description'],$params['country'],
										  $params['city'],$params['address'],
										  $params['image'],$params['video'], $params['price'], $posted_by);
			if($stmt->execute()){
				return true;
			}
			else
				$this->logger($stmt->error);
			return false;
		}
		public function updateAds($params){
			$mysqli = $this->connect();
			$this->logger(json_encode($params));
			if(isset($_FILES["file"])){
				$this->removeImage($params['image']);
				$response=$this->saveImage();
				if($response['error']==1){
					return $response;
				}
				if($response['error']!=2)
					$params['image'] = $response['filename'];
			}
			$sql = "UPDATE mct_ads SET subcat_id=?, updated=? WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$time = $this->getTimeStamp();
			$stmt->bind_param('isi', $params['subcat_id'], $time, $params['id']);
			if($stmt->execute()){
				$flag = $this->updateAdsDetails($params);
				if($flag){
					$response['error']=0;
					$response['ads_id']=$params['ads_id'];
					return $response;
				}
				else{
					$response['error'] =1;
					$response['Message']='Oops! Somthing went wrong.';
					return $response;
				}
			}
			return false;
		}
		public function updateAdsDetails($params){
			$mysqli = $this->connect();
			$sql = "UPDATE mct_ads_details SET ads_id=?,title=?,description=?,country=?,city=?,address=?,image=?,video=?, price=? WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('issssssssi', $params['ads_id'],$params['title'],
										  $params['description'],$params['country'],
										  $params['city'],$params['address'],
										  $params['image'],$params['video'],$params['price'], $params['id']);
			if($stmt->execute()){
				return true;
			}
			return false;
		}
		public function deleteCategoryById($id){
			$mysqli = $this->connect();
			$obj = new MCTDAOFetchData;
			$subcategories = $obj->getSubCategoryByCategoryId($id);
			if(!empty($subcategories)){
				$ids = array();
				foreach($subcategories as $sub)
					$ids[] = $sub['id'];
				if(!$this->deleteSubCategories($ids))
					return false;
			}
			$sql = "DELETE FROM mct_categories WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $id);
			if($stmt->execute())
				return true;
			
			return false;
		}
		public function deleteSubCategories($ids){
			$mysqli = $this->connect();
			$ids_ = '';
			foreach($ids as $id){
				$ids_.=$id.",";
			}
			$ids_=rtrim($ids_, ",");
			$sql = "DELETE FROM mct_subcategories WHERE id IN(".$ids_.")";
			$stmt = $mysqli->prepare($sql);
			$this->logger($sql);
			if($stmt->execute()){
				return true;
			}
			return false;
		}
		public function deleteAd($id){
			$mysqli = $this->connect();
			$fetch = new MCTDAOFetchData;
			$ad = $fetch->getAdsById($id);
			$user = ossn_loggedin_user();
			if(!($user->type=='admin' || $user->guid==$ad['posted_by']))
				return false;
			if(!$this->removeImage($ad['image']))
				return false;
			if(!$this->deleteAdsDetails($ad['ads_id']))
				return false;
			if($this->deleteAdsById($ad['ads_id']))
				return true;
			return false;
		}
		private function removeImage($url){
			$uri = str_replace(ossn_site_url(), $_SERVER['DOCUMENT_ROOT']."/", $url);
			if(unlink($uri))
				return true;
			return false;
		}
		private function deleteAdsDetails($ads_id){
			$mysqli = $this->connect();
			$sql = "DELETE FROM mct_ads_details WHERE ads_id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $ads_id);
			if($stmt->execute())
				return true;
			return false;
		}
		private function deleteAdsById($id){
			$mysqli = $this->connect();
			$sql = "DELETE FROM mct_ads WHERE id=?";
			$stmt = $mysqli->prepare($sql);
			$stmt->bind_param('i', $id);
			if($stmt->execute())
				return true;
			return false;
		}
	}
?>