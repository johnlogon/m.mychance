<section class="callout-section callout-section-version-1" style="background:url(images/callout-bg1.jpg) center center fixed">
         <div class="container ">
            <div class="row ">
               <div class="col-xs-12 ">
                  <div class="callout clearfix">
                     <div class="callout-inner">
                        <div class="callout-title">
                           <h4>ARE YOU READY</h4>
                        </div>
                        <div class="callout-desc">
                           <p>FOR THE POSTING YOUR ADS WITH "<span>MyChance</span>".</p>
                        </div>
                     </div>
                     <div class="view-more-btn">
                        <div class="more-btn-inner">
                           <a href="newAd.php">
                           <span>Get Started </span>
                           </a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      
      <section class="socket"  style="background-color:#ffffff;">
         <div class="container">
            <div class="site-info"  style="color:#333333;">
              Copyright © 2015 snogol software solutions. All right reserved.
            </div>
            <!-- .site-info -->
            <div class="bottom-social-icons">
               <a class="target-blank" href="https://www.facebook.com/"><i class="fa fa-facebook"></i></a>
               <a class="target-blank" href="https://www.twitter.com/"><i class="fa fa-twitter"></i></a>
               <a class="target-blank" href="https://www.flickr.com/"><i class="fa fa-flickr"></i></a>
               <a class="target-blank" href="https://www.pinterest.com/"><i class="fa fa-pinterest"></i></a>
               <a class="target-blank" href="https://www.google.com/"><i class="fa fa-google-plus"></i></a>
               <a class="target-blank" href="https://www.vimeo.com/"><i class="fa fa-vimeo-square"></i></a>
            </div>
            
         </div>
      </section>
      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/jquery.form.mind03d.js?ver=3.51.0-2014.06.20'></script>
      <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts8686.js?ver=4.5.1'></script>
      <script type='text/javascript' src='wp-includes/js/underscore.min4511.js?ver=1.8.3'></script>
      <script type='text/javascript' src='wp-includes/js/shortcode.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/backbone.min9632.js?ver=1.2.3'></script>
      <script type='text/javascript' src='wp-includes/js/wp-util.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/wp-backbone.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/media-models.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/plupload/wp-plupload.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/jquery/ui/core.mine899.js?ver=1.11.4'></script>
      <script type='text/javascript' src='wp-includes/js/jquery/ui/widget.mine899.js?ver=1.11.4'></script>
      <script type='text/javascript' src='wp-includes/js/jquery/ui/mouse.mine899.js?ver=1.11.4'></script>
      <script type='text/javascript' src='wp-includes/js/jquery/ui/sortable.mine899.js?ver=1.11.4'></script>
      <script type='text/javascript' src='wp-includes/js/mediaelement/mediaelement-and-player.min51cd.js?ver=2.22.0'></script>
      <script type='text/javascript' src='wp-includes/js/mediaelement/wp-mediaelement.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/media-views.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/media-editor.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-includes/js/media-audiovideo.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/planse6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-includes/js/comment-reply.min1c9b.js?ver=4.6.1'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/functionse6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/jquery.formef16.js?ver=2016-02-8'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/menue6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/bootstrap.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/chosen.jquery.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/jquery.isotope.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/owl.carousel.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='https://maps.googleapis.com/maps/api/js?ver=2014-07-18&key=AIzaSyC-ofPLIfBnJGxPiSR6tey8HPI5NjNt5js'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/gmap3.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/gmap3.infoboxe6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='js/jquery-ui.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/jquery.tools.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/modernizr.touche6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/jquery.ui.touch-punch.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/jquery.bxslider.mine6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-content/themes/classiads/js/custome6e4.js?ver=2014-07-18'></script>
      <script type='text/javascript' src='wp-includes/js/wp-embed.min1c9b.js?ver=4.6.1'></script>