<?php
	try{
		$user = new UserDao;
		$obj = new MCTDAOFetchData;
		$user_logged = ossn_loggedin_user();
		$data['settings'] = $user->getUserSettings($user_logged->guid);
		$data['settings']['email'] = $user_logged->email;
		$categories = $obj->getAllCategories();
		foreach($categories as $category){
			$count = $obj->getAdsCountByCategory($category['id']);
			$data['categories'].='<li>
									 <div class="category-icon-box"><i class="fa fa-book"></i></div>
									 <a href="category.php?id='.$category['id'].'" title="View posts in '.$category['category'].';">
									 	'.$category['category'].'
									 </a>
									 <span class="category-counter">'.$count.'</span>
								  </li>';
		}
		
		$data['ads']="";
		$ads = $obj->getAdsByUser($user_logged->guid);
		$obj->logger(json_encode($ads));
		foreach($ads as $ad){
			$data['ads'].='
							<div class="my-ad-box clearfix">
								<a href="viewAd.php?id='.$ad['ads_id'].'">
								<div class="my-ad-image">
									<img src="'.$ad['image'].'" style="max-width: 100%;">
								</div>
								</a>
								<div class="my-ad-details">
								<div class="post-title-my-ad clearfix">
								   <div class="post-title">
								   	<a href="viewAd.php?id='.$ad['ads_id'].'">
									  <span class="title-span"><i class="fa fa-cog" style="color:#fff;line-height: 33px;margin-left: 5px;"></i></span>
									  <span style="font-size:16px">'.$ad['title'].'</span>
									</a>
									  <span class="my-ad-a">
									    <a href="newAd.php?id='.$ad['ads_id'].'" style="margin-right:0px !important"><i class="fa fa-pencil edit-ad"></i> </a>
									  	<i class="fa fa-trash-o delete-ad"></i>
									  </span>
									  <span><i class="fa fa-clock-o"></i> '.$ad['created'].'</span>
									  <div class="my-ad-description">
									  	'.$ad['description'].'
									  </div>
								   </div>
								</div>
								</div>
						   </div>
						   ';
		}
	}
	catch(Exception $e){
		$obj->logger($e->getMessage());
		header(ossn_site_url('mtrade/error.php'));
	}
?>