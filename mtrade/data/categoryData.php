<?php
	try{
		$obj = new MCTDAOFetchData;
		if(!isset($_GET['id']))
			throw new Exception('Id not present');
		else if(!ctype_digit($_GET['id']))
			throw new Exception('Invalid ID');
		$userlogged = ossn_loggedin_user();
		$category = $obj->getCategoryById($_GET['id']);
		$data['category'] = $category['category'];
		$data['ads_count'] = $obj->getAdsCountByCategory($_GET['id']);
		$ads = $obj->getAdsByCategory($_GET['id']);
		if(count($ads)==0){
			$data['ads'] = '<h1>No ads found</h1>';
		}
		else{
			$data['ads']="";
			foreach($ads as $ad){
				$data['ads'].='<div class="col-md-4 col-sm-6 col-xs-12">
							    <div class="ad-box random-posts-grid ">
								  <a class="ad-image" href="viewAd.php?id='.$ad['ads_id'].'" title="'.$ads['title'].'">
									<img width="270" height="220" src="'.$ad['image'].'" 
										class="attachment-270x220 size-270x220 wp-post-image"  sizes="(max-width: 270px) 100vw, 270px">
								  </a>				
								  <div class="add-price"><span>
								  	<img src="images/rs.png" style="width: 8px;margin-top: -1px;">
									 '.$ad['price'].'	
									 </span>
								  </div>
								  <div class="post-title-cat">
									 <div class="post-title" style="display: inline-block;">
										<div class="post-title-icon" style="background-color:#'.random_color().'"><i class="fa fa-book"></i></div>
										<a href="viewAd.php?id='.$ad['ads_id'].'">'.$ad['title'].'</a>
									 </div>';
									if($userlogged->type=='admin' || $ad['posted_by']==$userlogged->guid){
								$data['ads'].='<div class="delete-ad-grid" style="display: inline-block;" onclick="deleteAd('.$ad['ads_id'].')">
									 				<i class="fa fa-trash"></i>
											   </div>';
									}
								 $data['ads'].='</div>
							   </div>
							</div>';
			}
		}
		
		$subcategories = $obj->getSubCategoryByCategoryId($_GET['id']);
		$data['subcategories']="";
		foreach($subcategories as $sub){
			$count = $obj->getAdsCountBySubCategory($sub['id']);
			$data['subcategories'].='<li>
										 <div class="category-icon-box"><i class="fa fa-book"></i></div>
										 <a href="subcategory.php?id='.$sub['id'].'" title="View posts in '.$sub['subcategory'].';">'.$sub['subcategory'].'</a>
										 <span class="category-counter">'.$count.'</span>
									  </li>';
		}
	}
	catch(Exception $e){
		$obj->logger($e->getMessage());
		header('location:'.ossn_site_url('mtrade/error.php'));
	}
?>