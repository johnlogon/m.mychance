<?php
	try{
		if(!ossn_isAdminLoggedin()){
			header("HTTP/1.1 401 Unauthorized");
			echo "<h1>Forbidden</h1>
					<p>You don't have permission to access /admin
					on this server.</p>
					<p>Additionally, a 404 Not Found
					error was encountered while trying to use an ErrorDocument to handle the request.</p>";
    		exit;
		}
		$obj = new MCTDAOFetchData;
		$categories = $obj->getAllCategories();
		$data['categories']="";
		foreach($categories as $category){
			$data['categories'].='<span class="author-details phone-number">
										<i class="fa fa-cog" style="background: #'.random_color().';"></i>
										<span style="display:inline-block" class="category-text" title="Click to see more" 
											id="cat_span'.$category['id'].'" onClick="viewSubCategory('.$category['id'].')">'.$category['category'].'</span>
											<input type="text" class="form-control edit-category-box" 
												id="cat'.$category['id'].'" value="'.$category['category'].'" 
												onblur=updateCategory('.$category['id'].') onFocus="this.value=this.value">
										<span style="float:right">
											<i class="fa fa-pencil icon-cat" area-hidden="true" onclick="editCategory('.$category['id'].')"></i>
											<i class="fa fa-times icon-cat" area-hidden="true" onclick="deletetCategory('.$category['id'].')"></i>
										</span>
								   </span>
								  ';
		}
		$data['categories'].='
								<input type="text" class="form-control new-category" placeholder="Category Title |" 
												onblur=saveCategory(this.value)>
								<i class="fa fa-times close-new-cat" onClick="closeNewCat()"></i>
								<span class="add-cat-error"></span>
							  <span style="float:right">
								<button type="button" id="add-category" class="btn btn-primary add-category" onClick="newCategory()">
									<i class="fa fa-plus-circle"></i>&nbsp;&nbsp;
									Add New
								</button>
							  </span>';
	}
	catch(Exception $e){
		$obj->logger($e->getMessage());
		header(ossn_site_url('mtrade/error.php'));
	}
?>