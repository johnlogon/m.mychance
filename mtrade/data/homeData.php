<?php
	try{
		$obj = new MCTDAOFetchData;
		$categories = $obj->getAllCategories();
		$data = array();
		$data['data']="";
		foreach($categories as $category){
			$data['data'].='<div class="col-md-3 col-sm-6 col-xs-12 category-box-outter" id="cat-'.$category['id'].'">
							  <div class="category-box">
								 <div class="category-header">
									<div class="category-icon">
									   <div style="background-color:#'.random_color().'" class="category-icon-box"><i class="fa fa-cog"></i></div>
									</div>
									<div class="cat-title">
									   <a href="category.php?id='.$category['id'].'">
										  <h4>'.$category['category'].'</h4>
									   </a>
									</div>
								 </div>
								 <div class="category-content clearfix">
								 	 <ul>';
			$subcategories = $obj->getSubCategoryByCategoryId($category['id']);
			foreach($subcategories as $subcategory){
				$count = $obj->getAdsCountBySubCategory($subcategory['id']);
				$data['data'].='<li>
								   <a href="subcategory.php?id='.$subcategory['id'].'" title="View posts in '.$subcategory['subcategory'].'">
								      '.$subcategory['subcategory'].'
								   </a>
								   <span class="category-counter">'.$count.'</span>
							    </li>';
			}
				$data['data'].='</ul>
								 </div>
						 <div class="clearfix"></div>
						 <div class="category-btn">
							<a href="category.php?id='.$category['id'].'">VIEW ALL</a>
						 </div>
					  </div>
					</div>';
		}
	}
	catch(Exception $e){
		$obj->logger($e->getMessage());
		header(ossn_site_url('mtrade/error.php'));
	}
?>