<?php
	try{
		$obj = new MCTDAOFetchData;
		$user = ossn_loggedin_user();
		$categories = $obj->getAllCategories();
		if(isset($_GET['id']) && $_GET['id']!=''){
			if(!ctype_digit($_GET['id']))
				throw new Exception('Invalid Id');
			$edit = $obj->getAdsById($_GET['id']);
			if($edit['posted_by']!=$user->guid)
				throw new Exception('Unauthorized');
			if(empty($edit))
				throw new Exception('Ad not found');
			$subcategories = $obj->getSubCategoryByCategoryId($edit['category_id']);
		}
		foreach($categories as $category){
			$count = $obj->getAdsCountByCategory($category['id']);
			$data['categories'].='<li>
									 <div class="category-icon-box"><i class="fa fa-book"></i></div>
									 <a href="category.php?id='.$category['id'].'" title="View posts in '.$category['category'].';">
									 	'.$category['category'].'
									 </a>
									 <span class="category-counter">'.$count.'</span>
								  </li>';
		}
	}
	catch(Exception $e){
		$obj->logger($e->getMessage());
		header(ossn_site_url('mtrade/error.php'));
	}
?>