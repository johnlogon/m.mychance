<?php
	try{
		$obj = new MCTDAOFetchData;
		$settings = new UserDao;
		if(!isset($_GET['id']))
			throw new Exception('Id not present');
		else if(!ctype_digit($_GET['id']))
			throw new Exception('Invalid ID');
		else if($_GET['id']=='')
			throw new Exception('Invalid ID');
		$ad = $obj->getAdsById($_GET['id']);
		if(empty($ad))
			throw new Exception('Ad not found');
		$categories = $obj->getAllCategories();
		foreach($categories as $category){
			$count = $obj->getAdsCountByCategory($category['id']);
			$data['categories'].='<li>
									 <div class="category-icon-box"><i class="fa fa-book"></i></div>
									 <a href="category.php?id='.$category['id'].'" title="View posts in '.$category['category'].';">
									 	'.$category['category'].'
									 </a>
									 <span class="category-counter">'.$count.'</span>
								  </li>';
		}
		$user =ossn_loggedin_user();
		$posted = ossn_user_by_guid($ad['posted_by']);
		$data['settings'] = $settings->getUserSettings($posted->guid);
		
		$data['random']['a'] = rand(1,10);
		$data['random']['b'] = rand(1,10);
		$sign = array('-', '+', '*');
		$data['random']['sign'] = $sign[rand(0,2)];
	}
	catch(Exception $e){
		$obj->logger($e->getMessage());
		header(ossn_site_url('mtrade/error.php'));
		exit;
	}
?>